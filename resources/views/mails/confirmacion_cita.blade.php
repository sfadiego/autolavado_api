@extends('mails.template')

@section('content')
<tr style="border-collapse:collapse;">
    <td style="Margin:0;padding-top:10px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:left top;background-color:#FAFAFA;" bgcolor="#fafafa" align="left">
      <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
        <tr style="border-collapse:collapse;">
          <td width="540" valign="top" align="center" style="padding:0;Margin:0;">
            <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
              <tr style="border-collapse:collapse;">
                <td align="left" style="padding:0;Margin:0;padding-bottom:10px;">
                  <h2 style="Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:24px;font-style:normal;font-weight:bold;color:#212121;">Confirmación de cita</h2>
                </td>
              </tr>
              <tr style="border-collapse:collapse;">
                <td align="left" style="padding:0;Margin:0;padding-bottom:5px;">
                  <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
                    Estimado, <strong>{{$demo[0]->nombre.' '.$demo[0]->apellido_paterno.' '.$demo[0]->apellido_materno}}</strong>
                  </p>
                  <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
                    Los datos de la cita con el folio #{{$demo[0]->id}} son los 	siguientes: 
                  </p>
                  <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
                    <strong>Fecha y hora: </strong> {{$demo[0]->fecha_cita.' '.$demo[0]->horario}} <br>
                    <strong>Asesor: </strong> {{$demo[0]->nombre_usuario_asesor.' '.$demo[0]->apellido_paterno_asesor.' '.$demo[0]->apellido_materno_asesor}} <br>
                  </p>                  
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
@endsection