<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| MODULO POLIZAS CONTABILIDAD Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'polizas-contabilidad'], function () {
    Route::get('/', 'Contabilidad\PolizasContabilidadController@index');
    Route::get('/{id}', 'Contabilidad\PolizasContabilidadController@show');
    Route::post('/', 'Contabilidad\PolizasContabilidadController@store');
    Route::put('/{id}', 'Contabilidad\PolizasContabilidadController@update');
    Route::delete('/{id}', 'Contabilidad\PolizasContabilidadController@destroy');
});

Route::group(['prefix' => 'polizas-contabilidad-detalle'], function () {
    Route::get('/', 'Contabilidad\PolizaContabilidadDetalleController@index');
    Route::get('/{id}', 'Contabilidad\PolizaContabilidadDetalleController@show');
    Route::post('/', 'Contabilidad\PolizaContabilidadDetalleController@store');
    Route::put('/{id}', 'Contabilidad\PolizaContabilidadDetalleController@update');
    Route::delete('/{id}', 'Contabilidad\PolizaContabilidadDetalleController@destroy');
});

Route::group(['prefix' => 'subcuenta'], function () {
    Route::get('/', 'Contabilidad\SubcuentaController@index');
    Route::get('/{id}', 'Contabilidad\SubcuentaController@show');
    Route::post('/', 'Contabilidad\SubcuentaController@store');
    Route::put('/{id}', 'Contabilidad\SubcuentaController@update');
    Route::delete('/{id}', 'Contabilidad\SubcuentaController@destroy');
     Route::get('cuenta/{id}', 'Contabilidad\SubcuentaController@subcuentaIdCuenta');
});