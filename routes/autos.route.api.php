<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| MODULO AUTOS Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'unidades'], function () {
    Route::get('/resumen-contabilidad', 'Autos\UnidadesController@getTotalUnidades');
    Route::get('/', 'Autos\UnidadesController@index');
    Route::get('/{id}', 'Autos\UnidadesController@show');
    Route::post('/', 'Autos\UnidadesController@store');
    Route::put('/{id}', 'Autos\UnidadesController@update');
    Route::delete('/{id}', 'Autos\UnidadesController@destroy');
});

Route::group(['prefix' => 'salida-unidades'], function () {
    Route::get('/', 'Autos\SalidaUnidades\SalidaUnidadesController@index');
    Route::get('/{id}', 'Autos\SalidaUnidades\SalidaUnidadesController@show');
    Route::post('/', 'Autos\SalidaUnidades\SalidaUnidadesController@store');
    Route::put('/{id}', 'Autos\SalidaUnidades\SalidaUnidadesController@update');
    Route::delete('/{id}', 'Autos\SalidaUnidades\SalidaUnidadesController@destroy');
});

Route::group(['prefix' => 'producto-servicio'], function () {
    Route::get('/', 'Autos\HistoricoProductosController@index');
    Route::get('/{id}', 'Autos\HistoricoProductosController@show');
    Route::post('/', 'Autos\HistoricoProductosController@store');
    Route::put('/{id}', 'Autos\HistoricoProductosController@update');
    Route::delete('/{id}', 'Autos\HistoricoProductosController@destroy');
});

Route::group(['prefix' => 'seminuevos'], function () {
    Route::get('/', 'Seminuevos\SeminuevosController@index');
    Route::get('/{id}', 'Seminuevos\SeminuevosController@show');
    Route::post('/', 'Seminuevos\SeminuevosController@store');
    Route::put('/{id}', 'Seminuevos\SeminuevosController@update');
    Route::delete('/{id}', 'Seminuevos\SeminuevosController@destroy');
});

Route::group(['prefix' => 'seminuevos/control-documentos/documentacion'], function () {
    Route::get('/', 'Seminuevos\DocumentacionController@index');
    Route::get('/{id}', 'Seminuevos\DocumentacionController@show');
    Route::post('/', 'Seminuevos\DocumentacionController@store');
    Route::put('/{id}', 'Seminuevos\DocumentacionController@update');
    Route::delete('/{id}', 'Seminuevos\DocumentacionController@destroy');
});

Route::group(['prefix' => 'catalogo-modelos'], function () {
    Route::get('/', 'Autos\CatalogoModelosController@index');
    Route::get('/{id}', 'Autos\CatalogoModelosController@show');
    Route::post('/', 'Autos\CatalogoModelosController@store');
    Route::put('/{id}', 'Autos\CatalogoModelosController@update');
    Route::delete('/{id}', 'Autos\CatalogoModelosController@destroy');
    Route::post('/buscar-id', 'Refacciones\CatalogoModelosController@buscar_id');
});

Route::group(['prefix' => 'venta-unidades'], function () {
    // Route::get('/', 'Autos\VentaAutosController@index');
    Route::get('/{id}', 'Autos\VentaAutosController@show');
    Route::post('/', 'Autos\VentaAutosController@store');
    Route::put('/{id}', 'Autos\VentaAutosController@update');
    Route::delete('/{id}', 'Autos\VentaAutosController@destroy');
});

Route::group(['prefix' => 'autos/pre-pedidos'], function () {
    Route::get('/preventa-equipo-opcional', 'Autos\EquipoOpcionalController@getVentaAutosEquipoOpcional');//unidades pre pedidas con equipo adicional
    Route::get('/', 'Autos\VentaAutosController@getPrepedidos');
    Route::get('/{prepedido_id}', 'Autos\VentaAutosController@getPrepedidosById'); //show by id pre pedido
});

Route::group(['prefix' => 'autos/equipo-opcional'], function () {
    Route::get('/', 'Autos\EquipoOpcionalController@index');
    Route::get('/{id}', 'Autos\EquipoOpcionalController@show'); //regresa carrito por id pre venta 
    Route::post('/', 'Autos\EquipoOpcionalController@store');
    Route::put('/{id}', 'Autos\EquipoOpcionalController@update');
    Route::delete('/{id}', 'Autos\EquipoOpcionalController@destroy');
});

Route::group(['prefix' => 'catalogo-autos'], function () {
    Route::get('/', 'Autos\CatalogoAutosController@index');
    Route::get('/{id}', 'Autos\CatalogoAutosController@show');
    Route::post('/', 'Autos\CatalogoAutosController@store');
    Route::put('/{id}', 'Autos\CatalogoAutosController@update');
    Route::delete('/{id}', 'Autos\CatalogoAutosController@destroy');
    Route::post('/buscar-id', 'Autos\CatalogoAutosController@buscar_id');
    Route::get('/buscar-catalogo/{id_catalogo}', 'Autos\CatalogoAutosController@buscar_elementos');
});

Route::group(['prefix' => 'catalogo-vestiduras'], function () {
    Route::get('/', 'Autos\CatalogoVestidurasController@index');
    Route::get('/{id}', 'Autos\CatalogoVestidurasController@show');
    Route::post('/', 'Autos\CatalogoVestidurasController@store');
    Route::put('/{id}', 'Autos\CatalogoVestidurasController@update');
    Route::delete('/{id}', 'Autos\CatalogoVestidurasController@destroy');
    Route::get('buscar-id/{nombre}', 'Autos\CatalogoVestidurasController@buscar_id');
});

Route::group(['prefix' => 'categoria-autos'], function () {
    Route::get('/', 'Autos\CategoriaAutosController@index');
    Route::get('/{id}', 'Autos\CategoriaAutosController@show');
    Route::post('/', 'Autos\CategoriaAutosController@store');
    Route::put('/{id}', 'Autos\CategoriaAutosController@update');
    Route::delete('/{id}', 'Autos\CategoriaAutosController@destroy');
});