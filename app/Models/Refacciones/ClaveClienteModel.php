<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ClaveClienteModel extends Modelo
{
    protected $table = 'clave_cliente';
    const ID = "id";
    const CLIENTE_ID = "id_cliente";
    const CLAVE_ID = "id_clave";
    
    protected $fillable = [
        self::CLIENTE_ID,
        self::CLAVE_ID
    ];
}