<?php

namespace App\Models\Refacciones;

use App\Models\Contabilidad\CatalogoCuentasModel;
use App\Models\Core\Modelo;
use App\Models\Refacciones\OrdenCompraModel;
use App\Models\Refacciones\VentasRealizadasModel;
use VentasRealizadas;

class FoliosModel extends Modelo
{
    protected $table = 'folios';
    const ID = "id";
    const FOLIO = "folio";
    const TIPO_CUENTA_ID = 'tipo_cuenta_id';
    const STATUS = "status";

    protected $fillable = [
        self::FOLIO,
        self::STATUS,
        self::TIPO_CUENTA_ID
    ];

    public function factura()
    {
        $this->belongsTo(Factura::class);
    }

    public function tipo_cuenta()
    {
        $this->hasOne(CatalogoCuentasModel::class, CatalogoCuentasModel::ID, self::TIPO_CUENTA_ID);
    }
}
