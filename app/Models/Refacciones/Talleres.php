<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class Talleres extends Modelo
{
    protected $table = 'taller';
    const ID = "id";
    const NOMBRE = "nombre";
    const UBICACION = "ubicacion";

    const DEFAUL_TALLER = 1;

    protected $fillable = [
        self::NOMBRE,
        self::UBICACION
    ];
}
