<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class CatTipoPagoModel extends Modelo
{
    protected $table = 'cat_tipo_pago';
    const ID = "id";
    const NOMBRE = "nombre";
    const CLAVE = "clave";
    const ACTIVO = "activo";

    protected $fillable = [
        self::NOMBRE,
        self::CLAVE,
        self::ACTIVO
    ];
}
