<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ProveedorRefacciones extends Modelo
{
    protected $table = 'proveedor_refacciones';
    const ID = "id";
    const PROVEEDOR_NOMBRE = "proveedor_nombre";
    const PROVEEDOR_RFC = "proveedor_rfc";
    const PROVEEDOR_CALLE = "proveedor_calle";
    const PROVEEDOR_NUMERO = "proveedor_numero";
    const PROVEEDOR_COLONIA = "proveedor_colonia";
    const PROVEEDOR_ESTADO = "proveedor_estado";
    const DIRECCION = "direccion";
    const CLAVE_IDENTIFICADOR = "clave_identificador";
    const RAZON_SOCIAL = "razon_social";
    const APELLIDO_MATERNO = "apellido_materno";
    const APELLIDO_PATERNO = "apellido_paterno";
    const PROVEEDOR_PAIS = "proveedor_pais";
    const CORREO = "correo";
    const CORREO_SECUNDARIO = "correo_secundario";
    const TELEFONO_SECUNDARIO = "telefono_secundario";
    const TELEFONO = "telefono";
    const NOTAS = "notas";
    const LIMITE_CREDITO = 'limite_credito';
    const SALDO = 'saldo';
    const CODIGO_POSTAL = 'codigo_postal';
    const FORMA_PAGO = 'forma_pago';
    const TIPO_REGISTRO = 'tipo_registro';

    const CFDI_ID = 'cfdi_id';
    const METODO_PAGO_ID = 'metodo_pago_id';


    protected $fillable = [
        self::PROVEEDOR_NOMBRE,
        self::PROVEEDOR_RFC,
        self::PROVEEDOR_CALLE,
        self::PROVEEDOR_NUMERO,
        self::PROVEEDOR_COLONIA,
        self::PROVEEDOR_ESTADO,
        self::DIRECCION,
        self::CLAVE_IDENTIFICADOR,
        self::RAZON_SOCIAL,
        self::APELLIDO_MATERNO,
        self::APELLIDO_PATERNO,
        self::PROVEEDOR_PAIS,
        self::CORREO,
        self::CORREO_SECUNDARIO,
        self::TELEFONO_SECUNDARIO,
        self::TELEFONO,
        self::NOTAS,
        self::CFDI_ID,
        self::METODO_PAGO_ID,
        self::LIMITE_CREDITO,
        self::SALDO,
        self::CODIGO_POSTAL,
        self::FORMA_PAGO,
        self::TIPO_REGISTRO
    ];
}
