<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ReComprasEstatusModel extends Modelo
{
    protected $table = 're_compras_estatus';
    const ID = "id";
    const COMPRA_ID = "orden_compra_id";
    const ESTATUS_COMPRA_ID = "estatus_compra_id";
    const ACTIVO = "activo";
    const USER_ID = "user_id";

    const REL_ESTATUS = "estatus_compra";

    
    protected $fillable = [
        self::COMPRA_ID,
        self::ESTATUS_COMPRA_ID,
        self::ACTIVO,
        self::USER_ID,
    ];

    public function estatus_compra()
    {
        return $this->hasOne(EstatusCompra::class, EstatusCompra::ID, self::ESTATUS_COMPRA_ID);
    }
}
