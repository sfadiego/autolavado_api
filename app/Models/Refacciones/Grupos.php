<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class Grupos extends Modelo
{
    protected $table = 'grupos';
    const ID = "id";
    const NOMBRE_GRUPO = "nombre_grupo";

    protected $fillable = [
        self::NOMBRE_GRUPO
    ];
}
