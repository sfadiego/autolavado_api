<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ProductoAlmacenModel extends Modelo
{
    protected $table = 'producto_almacen';
    const ID = "id";
    const ALMACEN_ID = "almacen_id";
    const PRODUCTO_ID = "producto_id";
    const NO_IDENTIFICACION = "no_identificacion";
    const CANTIDAD = "cantidad";

    const REL_ALMACEN = 'almacenes';
    
    protected $fillable = [
        self::ALMACEN_ID,
        self::PRODUCTO_ID,
        self::CANTIDAD,
        self::NO_IDENTIFICACION
    ];

    public function almacenes()
    {
        return $this->hasOne(Almacenes::class, Almacenes::ID, self::ALMACEN_ID);
    }
}
