<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\Precios;
use App\Models\Refacciones\ProductosModel;

class VentaProductoModel extends Modelo
{
    protected $table = 'venta_producto';
    const ID = "id";
    const PRODUCTO_ID = "producto_id";
    const VENTA_ID = "venta_id";
    const PRECIO_ID = "precio_id";
    const CLIENTE_ID = "cliente_id";
    const FOLIO_ID = "folio_id";
    const CANTIDAD = "cantidad";
    const TOTAL_VENTA = "venta_total";
    const VALOR_UNITARIO = "valor_unitario";
    const DESCONTADO_ALMACEN = "descontado_almacen";

    const REL_CLIENTE = 'cliente';

    protected $fillable = [
        self::PRODUCTO_ID,
        self::CLIENTE_ID,
        self::TOTAL_VENTA,
        self::VALOR_UNITARIO,
        self::PRECIO_ID,
        self::FOLIO_ID,
        self::CANTIDAD,
        self::VENTA_ID,
        self::DESCONTADO_ALMACEN
    ];

    public function producto()
    {
        return $this->belongsTo(ProductosModel::class);
    }

    public function precio()
    {
        return $this->belongsTo(Precios::class);
    }

    public function cliente()
    {
        return $this->belongsTo(ClientesModel::class);
    }

    public function rel_folio()
    {
        return $this->hasOne(FoliosModel::class, FoliosModel::ID);
    }
}
