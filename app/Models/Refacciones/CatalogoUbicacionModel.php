<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class CatalogoUbicacionModel extends Modelo
{
    protected $table = 'catalogo_ubicacion';
    const ID = "id";
    const NOMBRE = "nombre";

    protected $fillable = [
        self::NOMBRE
    ];
}
