<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class CatTipoPrecioModel extends Modelo
{
    protected $table = 'cat_tipo_precio';
    const ID = "id";
    const NOMBRE = "nombre";
    const ACTIVO = "activo";

    protected $fillable = [
        self::NOMBRE,
        self::ACTIVO
    ];
}
