<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ReProductoAlmacen extends Modelo
{
    protected $table = 're_producto_almacenes';
    const ID = "id";
    const ID_PRODUCTO = "id_producto";
    const ID_PRODUCTO_ALMACEN = "id_producto_almacen";
    const CANTAIDAD = "cantidad";


    protected $fillable = [
        self::ID_PRODUCTO,
        self::ID_PRODUCTO_ALMACEN, 
        self::CANTAIDAD
    ];
}
