<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ComprasRefacciones extends Modelo
{
    protected $table = 'compras_refacciones';
    const ID = "id";
    const PROVEEDOR_ID = "proveedor_id";
    const FECHA_FACTURA = "fecha_factura";
    const FECHA_CREACION = "fecha_creacion";
    const NUM_FACTURA = "num_factura";
    const OBSERVACIONES = "observaciones";
    const ESTADO = "estado";
    const TIPO_COMPRA = "tipo_compra";
    const ORDER_ENTRADA = "orden_entrada";
    const UID = "uid";
    const ESTATUS_COMPRA_ID = "estatus_compra_id";
    const NUMERO_PEDIDO = "numero_pedido";

    protected $fillable = [
        self::PROVEEDOR_ID,
        self::FECHA_FACTURA,
        self::FECHA_CREACION,
        self::NUM_FACTURA,
        self::OBSERVACIONES,
        self::ESTADO,
        self::TIPO_COMPRA,
        self::ORDER_ENTRADA,
        self::UID,
        self::ESTATUS_COMPRA_ID,
        self::NUMERO_PEDIDO
    ];
}
