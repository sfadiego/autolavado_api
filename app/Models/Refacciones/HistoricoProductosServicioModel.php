<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class HistoricoProductosServicioModel extends Modelo
{
    protected $table = 'historico_producto_servicio';
    const ID = "id";
    const PRODUCTO_ID = "producto_id";
    const PRODUCTO_SERVICIO_ID = "producto_servicio_id";
    const NO_IDENTIFICACION = "no_identificacion";
    const CANTIDAD = "cantidad";
    
    protected $fillable = [
        self::PRODUCTO_ID,
        self::PRODUCTO_SERVICIO_ID,
        self::NO_IDENTIFICACION,
        self::CANTIDAD
    ];
}
