<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class CatContactos extends Modelo
{
    protected $table = 'cat_contacto';
    const ID = "id";
    const NOMBRE = "nombre";
    const APELLIDO_PATERNO = "apellido_paterno";
    const APELLIDO_MATERNO = "apellido_materno";
    const TELEFONO = "telefono";
    const TELEFONO_SECUNDARIO = "telefono_secundario";
    const CORREO = "correo";
    const CORREO_SECUNDARIO = "correo_secundario";
    const NOTAS = "notas";
    const CLIENTE_ID = 'id_cliente';

    protected $fillable = [
        self::NOMBRE,
        self::APELLIDO_PATERNO,
        self::APELLIDO_MATERNO,
        self::TELEFONO,
        self::TELEFONO_SECUNDARIO,
        self::CORREO,
        self::CORREO_SECUNDARIO,
        self::NOTAS,
        self::CLIENTE_ID,
    ];
}
