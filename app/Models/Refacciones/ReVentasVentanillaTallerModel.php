<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ReVentasVentanillaTallerModel extends Modelo
{
    protected $table = 're_ventas_ventanilla_taller';
    const ID = "id";
    const VENTA_ID = "venta_id";
    const NUMERO_ORDEN = "numero_orden";

    protected $fillable = [
        self::VENTA_ID,
        self::NUMERO_ORDEN
    ];

}
