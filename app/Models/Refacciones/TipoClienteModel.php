<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class TipoClienteModel extends Modelo
{
    protected $table = 'tipo_cliente';
    const ID = "id";
    const NOMBRE = "nombre";
    

    const CLIENTE_ESPORADICO = 1;
    const CLIENTE_REGISTRADO = 2;

    protected $fillable = [
        self::NOMBRE,
    ];
}