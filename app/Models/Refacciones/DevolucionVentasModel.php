<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class DevolucionVentasModel extends Modelo
{
    protected $table = 'devolucion_venta';
    const ID = "id";
    const CLIENTE_ID = "cliente_id";
    const FOLIO_ID = "folio_id";
    const OBSERVACIONES = "observaciones";
    const VENDEDOR_ID = "vendedor_id";
    const VENTA_ID = "venta_id";
    const TOTAL_DEVOLUCION = "total_devolucion";
    const CANTIDAD = "cantidad";

    const REL_CLIENTE = 'cliente';

    protected $fillable = [
        self::CLIENTE_ID,
        self::FOLIO_ID,
        self::VENTA_ID,
        self::VENDEDOR_ID,
        self::OBSERVACIONES,
        self::TOTAL_DEVOLUCION
    ];

    public function producto()
    {
        return $this->belongsTo(ProductosModel::class);
    }

    public function precio()
    {
        return $this->belongsTo(Precios::class);
    }

    public function cliente()
    {
        return $this->belongsTo(ClientesModel::class);
    }
}
