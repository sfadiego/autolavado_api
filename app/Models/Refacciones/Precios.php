<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class Precios extends Modelo
{
    protected $table = 'precio';
    const ID = "id";
    const PRECIO_PUBLICO = "precio_publico";
    const PRECIO_MAYOREO = "precio_mayoreo";
    const PRECIO_INTERNO = "precio_interno";
    const PRECIO_TALLER = "precio_taller";
    const PRECIO_OTRAS_DISTRIBUIDORAS = "precio_otras_distribuidoras";
    const IMPUESTO = 'impuesto';
    const DESCRIPCION = 'descripcion';
    const DEFAULT_PRECIO = 1;

    protected $fillable = [
        self::PRECIO_PUBLICO,
        self::PRECIO_MAYOREO,
        self::PRECIO_INTERNO,
        self::PRECIO_TALLER,
        self::PRECIO_OTRAS_DISTRIBUIDORAS,
        self::IMPUESTO,
        self::DESCRIPCION
    ];
}
