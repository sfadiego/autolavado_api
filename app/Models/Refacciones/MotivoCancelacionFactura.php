<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class MotivoCancelacionFactura extends Modelo
{
    protected $table = 'motivo_cancelacion_factura';
    const ID = "id";
    const FACTURA_ID = "factura_id";
    const MOTIVO_ID = "motivo_id";
    
    protected $fillable = [
        self::FACTURA_ID,
        self::MOTIVO_ID
    ];
}
