<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ImagenProductoModel extends Modelo
{
    protected $table = 'imagen_producto';
    const ID = "id";
    const NOMBRE_ARCHIVO = "nombre_archivo";
    const RUTA_ARCHIVO = "ruta_archivo";
    const PRODUCTO_ID = "producto_id";
    const IMAGEN_PRODUCTO = 'imagen_producto';
    
    const CATEGORIA_ID = "categoria_id";
    
    const CAT_MAS_VENDIDO = 1;
    const CAT_GALERIA = 2;
    const CAT_PRODUCTO = 3;
    const CAT_PRODUCTO_DESTACADO = 4;

    const PATH_IMAGEN = 'productos';
    const IMAGEN_ID = 'imagen_id';
    const PRINCIPAL = 'principal';
    
    protected $fillable = [
        self::NOMBRE_ARCHIVO,
        self::RUTA_ARCHIVO,
        self::PRODUCTO_ID,
        self::CATEGORIA_ID
    ];
}
