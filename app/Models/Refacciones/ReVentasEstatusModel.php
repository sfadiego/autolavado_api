<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ReVentasEstatusModel extends Modelo
{
    protected $table = 're_ventas_estatus';
    const ID = "id";
    const VENTA_ID = "ventas_id";
    const ESTATUS_VENTA_ID = "estatus_ventas_id";
    const ACTIVO = "activo";
    const USER_ID = "user_id";

    const REL_ESTATUS = "estatus_venta";
    
    protected $fillable = [
        self::VENTA_ID,
        self::ESTATUS_VENTA_ID,
        self::ACTIVO,
        self::USER_ID,
    ];

    public function estatus_venta()
    {
        return $this->hasOne(EstatusVentaModel::class, EstatusVentaModel::ID, self::ESTATUS_VENTA_ID);
    }
}
