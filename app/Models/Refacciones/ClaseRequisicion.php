<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ClaseRequisicion extends Modelo
{
    protected $table = 'clase_requisicion';
    const ID = "id";
    const NOMBRE = "nombre";

    protected $fillable = [
        self::NOMBRE
    ];
}
