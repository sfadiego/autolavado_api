<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class Areas extends Modelo
{
    protected $table = 'areas';
    const ID = "id";
    const NOMBRE = "nombre";

    protected $fillable = [
        self::NOMBRE
    ];
}
