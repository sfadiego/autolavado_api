<?php

namespace App\Models\Seminuevos;

use App\Models\Core\Modelo;

class DocumentacionModel extends Modelo
{
    protected $table = 'seminuevos_documentos';
    const ID = "id";
    const USUARIO_RECIBE ="usuario_recibe";
    const FECHA_RECEPCION = "fecha_recepcion";
    const ID_UNIDAD = "id_unidad";
    const NUMERO_ECONOMICO = "numero_economico";
    const NUMERO_CLIENTE = "numero_cliente";
    const REGIMEN_FISCAL = "regimen_fiscal";
    const NOMBRE = "nombre";
    const APELLIDO_PATERNO = "apellido_paterno";
    const APELLIDO_MATERNO = "apellido_materno";
    const NOMBRE_EMPRESA = "nombre_empresa";
    const COPIA_FACTURA_ORIGEN = "copia_factura_origen";
    const REFACTURACION_MYLSA = "refacturacion_mylsa";
    const FACTURA_ORIGINAL_ENDOSADA = "factura_original_endosada";
    const TENENCIAS_PAGADAS = "tenencias_pagadas";
    const COPIA_TARJETA_CIRCULACION = "copia_tarjeta_circulacion";
    const CERTIFICADO_VERIFICACION_VIGENTE = "certificado_verificacion_vigente";
    const MANUAL_PROPIETARIO = "manual_propietario";
    const POLIZA_GARANTIA = "poliza_garantia";
    const CANCELACION_POLIZA_VIGENTE = "cancelacion_poliza_vigente";
    const DUPLICADO_LLAVES = "duplicado_llaves";
    const CODIGO_RADIO = "codigo_radio";
    const BRILLO_SEGURIDAD = "brillo_seguridad";
    const CONTROL ="control";
    const AUDIFONOS = "audifonos";
    const INE_FRONTAL = "ine_frontal";
    const INE_TRASERA = "ine_trasera";
    const CURP = "curp";
    const RFC_SAT = "rfc_sat";
    const COMPROBANTE_DOMICILIO = "comprobante_domicilio";
    const CONSTANCIA_SITUACION_FISCAL = "constancia_situacion_fiscal";
    const ACTA_CONSTITUTIVA = "acta_constitutiva";

    protected $fillable = [
        self::USUARIO_RECIBE,
        self::FECHA_RECEPCION,
        self::ID_UNIDAD,
        self::NUMERO_ECONOMICO,
        self::NUMERO_CLIENTE,
        self::REGIMEN_FISCAL,
        self::NOMBRE,
        self::APELLIDO_PATERNO,
        self::APELLIDO_MATERNO,
        self::NOMBRE_EMPRESA,
        self::COPIA_FACTURA_ORIGEN,
        self::REFACTURACION_MYLSA,
        self::FACTURA_ORIGINAL_ENDOSADA,
        self::TENENCIAS_PAGADAS,
        self::COPIA_TARJETA_CIRCULACION,
        self::CERTIFICADO_VERIFICACION_VIGENTE,
        self::MANUAL_PROPIETARIO,
        self::POLIZA_GARANTIA,
        self::CANCELACION_POLIZA_VIGENTE,
        self::DUPLICADO_LLAVES,
        self::CODIGO_RADIO,
        self::BRILLO_SEGURIDAD,
        self::CONTROL,
        self::AUDIFONOS,
        self::INE_FRONTAL,
        self::INE_TRASERA,
        self::CURP,
        self::RFC_SAT,
        self::COMPROBANTE_DOMICILIO,
        self::CONSTANCIA_SITUACION_FISCAL,
        self::ACTA_CONSTITUTIVA
    ];
}
