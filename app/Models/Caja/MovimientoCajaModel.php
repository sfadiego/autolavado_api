<?php

namespace App\Models\Caja;

use App\Models\Core\Modelo;

class MovimientoCajaModel extends Modelo
{
    protected $table = 'movimientos_caja';
    
    const ID = "id";
    const NOMBRE = 'nombre';

    const ENTRADA = 1;
    const SALIDA = 2;

    protected $fillable = [
        self::NOMBRE
    ];
}
