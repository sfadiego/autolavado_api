<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class TipoDocumentoCreditoModel extends Modelo
{
    protected $table = 'tipo_documento_credito';
    const ID = "id";
    const DESCRIPCION = "descripcion";

    protected $fillable = [
        self::DESCRIPCION
    ];
}
