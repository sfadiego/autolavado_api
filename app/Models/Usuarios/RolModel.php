<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;

class RolModel extends Modelo
{
    protected $table = 'roles';
    const ID = "id";
    const ROL = "rol";
    
    protected $fillable = [
        self::ROL
    ];
}
