<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;

class MenuVistasModel extends Modelo
{
    protected $table = 'menu_vistas';
    const ID = "id";
    const NOMBRE = "nombre";
    const SUBMENU_ID = "submenu_id";
    const LINK = "link";
    const CONTROLADOR = "controlador";
    const MODULO = "modulo";
    const ES_EXTERNA = "es_externa";
    
    protected $fillable = [
        self::ID,
        self::NOMBRE,
        self::SUBMENU_ID,
        self::LINK,
        self::CONTROLADOR,
        self::MODULO,
        self::ES_EXTERNA
    ];

}
