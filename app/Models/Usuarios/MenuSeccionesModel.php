<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class MenuSeccionesModel extends Modelo
{
    protected $table = 'menu_secciones';
    const ID = "id";
    const NOMBRE = "nombre";
    const MODULO_ID = "modulo_id";
    
    protected $fillable = [
        self::ID,
        self::MODULO_ID,
        self::NOMBRE
    ];
}
