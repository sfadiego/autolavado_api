<?php
namespace App\Models\Ecommerce;
use App\Models\Core\Modelo;

class CarritoModel extends Modelo
{
    protected $table = 'carrito';
    const ID = 'ID';
    const ID_USUARIO = 'id_usuario';
    const ID_PRODUCTO = 'id_producto';
    const CANTIDAD = 'cantidad';
    const VENDIDO = 'vendido';

    protected $fillable = [
        self::ID,
        self::ID_USUARIO,
        self::ID_PRODUCTO,
        self::CANTIDAD,
        self::VENDIDO
    ];
}
