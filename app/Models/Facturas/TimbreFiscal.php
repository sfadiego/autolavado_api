<?php

namespace App\Models\Facturas;

use App\Models\Core\Modelo;

class TimbreFiscal extends Modelo
{
    protected $table = 'timbre_fiscal';
    const ID = "id";
    const VERSION = 'version';
    const UUID = 'uuid';
    const FECHA_TIMBRADO = 'fecha_timbrado';
    const RFC_PROV_CERTIF = 'rfc_prov_certif';
    const SELLO_CFD = 'sello_cfd';
    const NO_CERTIFICADO_SAT = 'no_certificado_sat';
    const SELLO_SAT = 'sello_sat';
    const FACTURA_ID = "factura_id";

    protected $fillable = [
        self::UUID,
        self::FECHA_TIMBRADO,
        self::RFC_PROV_CERTIF,
        self::SELLO_CFD,
        self::NO_CERTIFICADO_SAT,
        self::SELLO_SAT,
        self::FACTURA_ID
    ];
}
