<?php

namespace App\Http\Controllers\Polizas;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Polizas\ServicioCatTipoPolizas;
use Illuminate\Http\Request;

class CatTipoPolizaController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatTipoPolizas();
    }
}
