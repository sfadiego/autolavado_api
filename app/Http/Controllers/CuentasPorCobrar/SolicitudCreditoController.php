<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioManejoArchivos;
use App\Servicios\CuentasPorCobrar\ServicioSolicitudCredito;
use App\Models\CuentasPorCobrar\DocumentosSolicitudModel;
use Illuminate\Http\Request;

class SolicitudCreditoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioSolicitudCredito();
        $this->servicioArchivos = new ServicioManejoArchivos();
    }

    public function index()
    {
        return Respuesta::json($this->servicio->getAll(), 200);
    }
    
    public function show($id)
    {
        $modelo = $this->servicio->getOne($id);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $solicitud = $this->servicio->store($request->all());
            $request->merge(['solicitud_id' => $solicitud->id]);
            $this->handleFiles($request);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($solicitud, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function handleFiles(Request $request)
    {
        $this->fileUploadSolicitudCredito($request);
        $this->fileUploadComprobanteDomicilio($request);
        $this->fileUploadReciboNomina($request);
        $this->fileUploadTipoIdentificacion($request);
    }

    public function fileUploadSolicitudCredito($request)
    {
        
        $file_identificacion = $request->file(DocumentosSolicitudModel::SOLICITUD_CREDITO);
        $newFileName = $this->servicioArchivos->setFileName($file_identificacion);
        $directorio = $this->servicio->setDirectory($request->get('solicitud_id'));
        $this->servicioArchivos->upload($file_identificacion, $directorio, $newFileName);

        return $this->servicio->guardarArchivos([
            DocumentosSolicitudModel::ID_SOLICITUD => $request->get('solicitud_id'),
            DocumentosSolicitudModel::RUTA_ARCHIVO => $directorio,
            DocumentosSolicitudModel::NOMBRE_ARCHIVO => $newFileName,
            DocumentosSolicitudModel::ID_TIPO_DOCUMENTO => DocumentosSolicitudModel::ID_TIPO_SOLICITUD_CREDITO
        ]);
    }

    public function fileUploadComprobanteDomicilio($request)
    {
        $file_identificacion = $request->file(DocumentosSolicitudModel::COMPROBANTE_DOMICILIO);
        $newFileName = $this->servicioArchivos->setFileName($file_identificacion);
        $directorio = $this->servicio->setDirectory($request->get('solicitud_id'));
        $this->servicioArchivos->upload($file_identificacion, $directorio, $newFileName);

        return $this->servicio->guardarArchivos([
            DocumentosSolicitudModel::ID_SOLICITUD => $request->get('solicitud_id'),
            DocumentosSolicitudModel::RUTA_ARCHIVO => $directorio,
            DocumentosSolicitudModel::NOMBRE_ARCHIVO => $newFileName,
            DocumentosSolicitudModel::ID_TIPO_DOCUMENTO => DocumentosSolicitudModel::ID_TIPO_COMPROBANTE_DOMICILIO
        ]);
    }

    public function fileUploadReciboNomina($request)
    {
        $file_identificacion = $request->file(DocumentosSolicitudModel::RECIBO_NOMINA);
        $newFileName = $this->servicioArchivos->setFileName($file_identificacion);
        $directorio = $this->servicio->setDirectory($request->get('solicitud_id'));
        $this->servicioArchivos->upload($file_identificacion, $directorio, $newFileName);

        return $this->servicio->guardarArchivos([
            DocumentosSolicitudModel::ID_SOLICITUD => $request->get('solicitud_id'),
            DocumentosSolicitudModel::RUTA_ARCHIVO => $directorio,
            DocumentosSolicitudModel::NOMBRE_ARCHIVO => $newFileName,
            DocumentosSolicitudModel::ID_TIPO_DOCUMENTO => DocumentosSolicitudModel::ID_TIPO_RECIBO_NOMINA
        ]);
    }

    public function fileUploadTipoIdentificacion($request)
    {
        $file_identificacion = $request->file(DocumentosSolicitudModel::IDENTIFICACION);
        $newFileName = $this->servicioArchivos->setFileName($file_identificacion);
        $directorio = $this->servicio->setDirectory($request->get('solicitud_id'));
        $this->servicioArchivos->upload($file_identificacion, $directorio, $newFileName);
        
        return $this->servicio->guardarArchivos([
            DocumentosSolicitudModel::ID_SOLICITUD => $request->get('solicitud_id'),
            DocumentosSolicitudModel::RUTA_ARCHIVO => $directorio,
            DocumentosSolicitudModel::NOMBRE_ARCHIVO => $newFileName,
            DocumentosSolicitudModel::ID_TIPO_DOCUMENTO => DocumentosSolicitudModel::ID_TIPO_IDENTIFICACION
        ]);
    }

    public function downloadArchivosSolicitudCredito($id, $file_name)
    {
        return response()->download(storage_path("app/solicitud_credito/uploads/{$id}/{$file_name}"));
    }
}
