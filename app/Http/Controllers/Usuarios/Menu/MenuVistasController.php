<?php

namespace App\Http\Controllers\Usuarios\Menu;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Usuarios\Menu\ServiciomenuRolesVistas;
use App\Servicios\Usuarios\Menu\ServicioMenuUsuariosVistas;
use App\Servicios\Usuarios\Menu\ServiciomenuVistas;
use Illuminate\Http\Request;

class MenuVistasController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServiciomenuVistas();
        $this->serviciomenuRolesVistas = new ServiciomenuRolesVistas();
        $this->servicioMenuUsuariosVistas = new ServicioMenuUsuariosVistas();
    }

    public function getVistasBySubmenu(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasMenuVista());
            $menu = $this->servicio->vistasBySubmenuId($request->all());
            return Respuesta::json($menu, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function saveVistasRoles(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->serviciomenuRolesVistas->getReglasMenuVistaRoles());
            $menu = $this->serviciomenuRolesVistas->storeVistasRoles($request->all());
            return Respuesta::json($menu, 201);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function saveVistasUsuarios(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicioMenuUsuariosVistas->getReglasMenuVistaUsuarios());
            $menu = $this->servicioMenuUsuariosVistas->storeVistasUsuarios($request->all());
            return Respuesta::json($menu, 201);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }


    public function getUsuariosVistasByUserId(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicioMenuUsuariosVistas->getReglasVistaByUsuario());
            return Respuesta::json($this->servicioMenuUsuariosVistas->vistasByUserId($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getRolesVistasByRolId(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->serviciomenuRolesVistas->getReglasVistaByRol());
            return Respuesta::json($this->serviciomenuRolesVistas->vistasByRoles($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
