<?php

namespace App\Http\Controllers\Usuarios;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Usuarios\Menu\ServicioRoles;
use Illuminate\Http\Request;

class RolesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioRoles();
    }

    public function storerolesModulos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasStoreModulos());
            $modelo = $this->servicio->storeRolesModulos($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function rolesModulos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasByRol());
            return Respuesta::json($this->servicio->rolesAndModulos($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e); 
        }
    }
}
