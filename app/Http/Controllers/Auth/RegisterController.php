<?php

namespace App\Http\Controllers\Auth;

// use App\Http\Core\Controllers\Controller;
use App\Http\Controllers\Core\Controller;
use App\Models\Usuarios\User as Usuarios;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            Usuarios::NOMBRE => ['required', 'string', 'max:255'],
            Usuarios::EMAIL => ['required', 'string', 'email', 'max:255', 'unique:usuarios'],
            Usuarios::RFC => ['required', 'string', 'unique:usuarios'],
            Usuarios::USUARIO => ['required', 'string', 'unique:usuarios'],
            Usuarios::PASSWORD => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return App\Models\Usuarios\User;
     */
    protected function create(array $data)
    {
        return Usuarios::create([
            Usuarios::NOMBRE => $data['nombre'],
            Usuarios::EMAIL => $data['email'],
            Usuarios::USUARIO => $data['usuario'],
            Usuarios::RFC => $data['rfc'],
            Usuarios::ULTIMO_ACCESO => date('yy-m-d'),
            Usuarios::PASSWORD => Hash::make($data['password']),
        ]);
    }
}
