<?php

namespace App\Http\Controllers\CuentasPorPagar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorPagar\ServicioCatTipoAbono;

class CatTipoAbonoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatTipoAbono();
    }

}
