<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioRequisiciones;
use Illuminate\Http\Request;

class RequisicionesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioRequisiciones();
    }

    public function getDetalleRequisicion($id)
    {
        try {
            $data = $this->servicio->getByIdRequisicion($id);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
