<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioEstatusInventario;

class CatalogoEstatusInventarioController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioEstatusInventario();
    }
}
