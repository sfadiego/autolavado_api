<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Models\Refacciones\ServicioCatalogoUbicacion;
use Illuminate\Http\Request;

class UbicacionController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatalogoUbicacion();
    }
}
