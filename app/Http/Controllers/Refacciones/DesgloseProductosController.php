<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioDesgloseProductos;
use Illuminate\Http\Request;
use App\Servicios\Core\Respuestas\Respuesta;


class DesgloseProductosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioDesgloseProductos();
    }

    public function actualizaStockByProducto(Request $request)
    {
        try {
            $data = $this->servicio->actualizaStockByProducto($request->all());
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getStockByProducto(Request $request)
    {
        try {
            $data = $this->servicio->getStockByProducto($request->all());
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
