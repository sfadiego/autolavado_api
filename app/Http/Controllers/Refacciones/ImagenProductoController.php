<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioImagenProducto;
use Illuminate\Http\Request;

class ImagenProductoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioImagenProducto();
    }

    public function imagenByProductos($producto_id)
    {
        return ['data' => $this->servicio->imagenByProducto($producto_id)];
    }
}
