<?php

namespace App\Http\Controllers\Refacciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioClaveCliente;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;

class ClaveClienteController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioClaveCliente();
    }

    public function searchClienteId($id)
    {
        try {
            $modelo = $this->servicio->searchClientesId($id);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

}
