<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioProductoAlmacen;
use Illuminate\Http\Request;

class ProductoAlmacenController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioProductoAlmacen();
    }

    public function getCantidadAlmacenProducto(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasCantidadProducto());
            $modelo = $this->servicio->getCantidadAlmByProducto($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function descontar(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, [
                'no_identificacion'=>'required',
                'cantidad'=>'required'
            ]);
            $modelo = $this->servicio->descontarAlmacen($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
