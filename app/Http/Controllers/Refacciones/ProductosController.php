<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioManejoArchivos;
use App\Servicios\Facturas\ServicioFacturacion;
use App\Servicios\Refacciones\ServicioProductos;
use App\Servicios\Refacciones\ServicioVentaProducto;
use App\Servicios\Refacciones\ServicioListaProductosOrdenCompra;
use App\Models\Facturas\Factura;
use App\Models\Refacciones\ImagenProductoModel;
use App\Servicios\Refacciones\ServicioImagenProducto;
use Illuminate\Http\Request;
use Throwable;

class ProductosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioProductos();
        $this->servicioFacturas = new ServicioFacturacion();
        $this->servicioArchivos = new ServicioManejoArchivos();
        $this->servicioVentaProducto = new ServicioVentaProducto();
        $this->servicioProductoOrdenCompra = new ServicioListaProductosOrdenCompra();
        $this->servicioImagenProducto = new ServicioImagenProducto();
    }

    public function storeproductos(Request $request)
    {
        try {
            $producto = $this->servicio->existeProducto($request);
            if (count($producto) > 0) {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdate());
                $modelo = $this->servicio->updateproductos($request, $producto[0]->id);
                $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
                return Respuesta::json($modelo, 200, $mensaje);
            } else {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
                $modelo = $this->servicio->storeproductos($request);
                if (!empty($request->file(ImagenProductoModel::IMAGEN_PRODUCTO))) {
                    $request->merge([ImagenProductoModel::PRODUCTO_ID => $modelo->id]);
                    $this->handleFoto($request);
                }

                $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
                return Respuesta::json($modelo, 201, $mensaje);
            }
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function handleFoto(Request $request)
    {
        $file = $request->file(ImagenProductoModel::IMAGEN_PRODUCTO);
        $newFileName = $this->servicioArchivos->setFileName($file);
        $directorio = $this->servicioArchivos->setDirectory(ImagenProductoModel::PATH_IMAGEN);
        $this->servicioArchivos->upload($file, $directorio, $newFileName);
        $path =  DIRECTORY_SEPARATOR . $directorio . DIRECTORY_SEPARATOR;
        $this->servicioImagenProducto->crear([
            ImagenProductoModel::NOMBRE_ARCHIVO => $newFileName,
            ImagenProductoModel::RUTA_ARCHIVO => $path,
            ImagenProductoModel::PRODUCTO_ID => $request->get(ImagenProductoModel::PRODUCTO_ID),
        ]);
    }

    public function updateImagenProducto(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasAgregaImagenes());
            $file = $request->file(ImagenProductoModel::IMAGEN_PRODUCTO);
            $newFileName = $this->servicioArchivos->setFileName($file);
            $directorio = $this->servicioArchivos->setDirectory(ImagenProductoModel::PATH_IMAGEN);
            if (!empty($request->get(ImagenProductoModel::PRODUCTO_ID)) && empty($request->get(ImagenProductoModel::IMAGEN_ID))) {
                $path =  DIRECTORY_SEPARATOR . $directorio . DIRECTORY_SEPARATOR;
                $this->servicioArchivos->upload($file, $directorio, $newFileName);
                $this->servicioImagenProducto->crear([
                    ImagenProductoModel::NOMBRE_ARCHIVO => $newFileName,
                    ImagenProductoModel::RUTA_ARCHIVO => $path,
                    ImagenProductoModel::PRODUCTO_ID => $request->get(ImagenProductoModel::PRODUCTO_ID),
                ]);
            }

            if (!empty($request->get(ImagenProductoModel::PRODUCTO_ID)) && !empty($request->get(ImagenProductoModel::IMAGEN_ID))) {
                $this->servicioArchivos->upload($file, $directorio, $newFileName);
                $this->massUpdateWhereId(ImagenProductoModel::ID, $request->get(ImagenProductoModel::IMAGEN_ID), [
                    ImagenProductoModel::NOMBRE_ARCHIVO => $newFileName,
                    ImagenProductoModel::RUTA_ARCHIVO => $path,
                    ImagenProductoModel::PRODUCTO_ID => $request->get(ImagenProductoModel::PRODUCTO_ID),
                ]);
            }
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function uploadFacturaProductos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicioFacturas->getReglasUploadxml());
            $file = $request->file(Factura::ARCHIVO_FACTURA);
            $newFileName = $this->servicioArchivos->setFileName($file);
            $directorio = $this->servicioArchivos->setDirectory(ServicioFacturacion::DIRECTORIO_FACTURAS);
            $this->servicioArchivos->upload($file, $directorio, $newFileName);
            $path =  DIRECTORY_SEPARATOR . $directorio  . $newFileName;
            $factura = $this->servicioFacturas->handleDataXmlFactura($path);
            $this->servicio->setFacturaId($factura);
            $modelo = $this->servicio->guardarDataFacturaProducto(['xml_path' => $path]);
            return Respuesta::json($modelo, 200);
        } catch (Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getProductoById($id)
    {
        try {
            $modelo = $this->servicio->getproductodataByid($id);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getAllProductos(Request $request)
    {
        try {
            $productos = $this->servicio->getAllProductos($request);
            return Respuesta::json($productos, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getAllStockProducto(Request $request)
    {
        try {
            return Respuesta::json($this->servicio->stockProductos($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getStockByProductoId(int $producto_id)
    {
        try {
            return Respuesta::json($this->servicio->stockByProductoId($producto_id), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getCantidadActualByProducto(Request $request)
    {
        try {
            return Respuesta::json($this->servicio->getCantidadActualByProducto($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function listadoStockProductos(Request $request)
    {
        try {

            return ['data' => $this->servicio->stockProductos($request->all())];
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByNumeroPiezaDescripcion(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaPiezas());
            $modelo = $this->servicio->searchProductos($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function descontarProductos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasDescontarAumentar());
            $modelo = $this->servicio->descontarProductos($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function aumentarProductos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasDescontarAumentar());
            $modelo = $this->servicio->aumentarProductos($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function validarCantidadProducto(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasValidarCantidad());
            $modelo = $this->servicio->validarRestarCantidadProducto($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function updateProductoUbicacion(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdateUbicacionProducto());
            $producto_ubicacion = $this->servicio->customUpdate($request, $id, $this->servicio->getReglasUpdateUbicacionProducto());
            return Respuesta::json($producto_ubicacion);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
