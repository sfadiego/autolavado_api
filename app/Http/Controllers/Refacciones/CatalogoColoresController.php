<?php

namespace App\Http\Controllers\Refacciones;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Refacciones\ServicioCatalogoColores;

class CatalogoColoresController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatalogoColores();
    }

    public function buscar_id(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaColor());
            $modelo = $this->servicio->searchIdColor($request);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
