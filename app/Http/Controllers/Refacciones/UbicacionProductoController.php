<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioUbicacionProducto;

class UbicacionProductoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioUbicacionProducto();
    }
}
