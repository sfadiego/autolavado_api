<?php

namespace App\Servicios\Contabilidad;

use App\Servicios\Core\ServicioDB;
use App\Models\Contabilidad\CatalogoCuentasModel;

class ServicioCuentas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catalogo cuentas';
        $this->modelo = new CatalogoCuentasModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoCuentasModel::NO_CUENTA => 'required',
            CatalogoCuentasModel::NOMBRE_CUENTA => 'required',
            CatalogoCuentasModel::CVE_AUXILIAR => 'required',
            CatalogoCuentasModel::TIPO_CUENTA => 'required',
            CatalogoCuentasModel::CODIGO => 'required',
            CatalogoCuentasModel::NATURALEZA => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatalogoCuentasModel::NO_CUENTA => 'nullable',
            CatalogoCuentasModel::NOMBRE_CUENTA => 'nullable',
            CatalogoCuentasModel::CVE_AUXILIAR => 'nullable',
            CatalogoCuentasModel::TIPO_CUENTA => 'nullable',
            CatalogoCuentasModel::CODIGO => 'nullable',
            CatalogoCuentasModel::NATURALEZA => 'nullable'
        ];
    }
}
