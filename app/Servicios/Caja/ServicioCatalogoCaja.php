<?php

namespace App\Servicios\Caja;

use App\Servicios\Core\ServicioDB;
use App\Models\Caja\CatalogoCajaModel;

class ServicioCatalogoCaja extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'cajas';
        $this->modelo = new CatalogoCajaModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoCajaModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatalogoCajaModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre'
        ];
    }
}
