<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\MenuSeccionesModel;

class ServicioSeccionesMenu extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new MenuSeccionesModel();
        $this->recurso = 'secciones';
    }

    public function getReglasGuardar()
    {
        return [
            MenuSeccionesModel::NOMBRE => 'required|unique:modulo,nombre',
            MenuSeccionesModel::MODULO_ID => 'required|exists:modulo,id'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            MenuSeccionesModel::NOMBRE => 'required|unique:modulo,nombre',
            MenuSeccionesModel::MODULO_ID => 'required|exists:modulo,id'
        ];
    }

    public function getReglasSecciones()
    {
        return [
            MenuSeccionesModel::MODULO_ID => 'required|exists:modulo,id'
        ];
    }

    public function getSecciones($parametros)
    {
        return $this->modelo
            ->where(MenuSeccionesModel::MODULO_ID, '=', $parametros[MenuSeccionesModel::MODULO_ID])
            ->get();
    }

    public function seccionesByModulos($parametros)
    {
        $query = $this->modelo->select(
            'menu_secciones.id as seccion_id',
            'menu_secciones.nombre as seccion_nombre',
            'modulo.nombre as modulo_nombre',
            'modulo.id as modulo_id'
        );
        $query->join('modulo', 'modulo.id', '=', 'menu_secciones.modulo_id');

        if (isset($parametros[MenuSeccionesModel::MODULO_ID])) {
            $query->where(MenuSeccionesModel::MODULO_ID, '=', $parametros[MenuSeccionesModel::MODULO_ID]);
        }

        return  $query->get();
    }

    public function seccionesByModulosPermisos($parametros)
    {
        $query = $this->modelo->select(
            'menu_secciones.id as seccion_id',
            'menu_secciones.nombre as seccion_nombre',
            'menu_secciones.modulo_id'
        );
        $query->join('menu_usuario_vista', 'menu_usuario_vista.seccion_id', '=', 'menu_secciones.id');
        $query->join('menu_submenu', 'menu_submenu.seccion_id', '=', 'menu_secciones.id');

        if (isset($parametros[MenuSeccionesModel::MODULO_ID])) {
            $query->where(MenuSeccionesModel::MODULO_ID, '=', $parametros[MenuSeccionesModel::MODULO_ID]);
        }

        return  $query->groupBy(
            'menu_secciones.id',
            'menu_secciones.nombre',
            'menu_secciones.modulo_id'
        )->get();
    }
}
