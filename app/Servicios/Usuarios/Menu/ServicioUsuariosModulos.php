<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\User;
use App\Models\Usuarios\UsuariosModulosModel;

class ServicioUsuariosModulos extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new UsuariosModulosModel();
        $this->modeloUsuarios = new User();
        $this->recurso = 'usuarios_modulos';
    }

    public function getReglasGuardar()
    {
        return [
            UsuariosModulosModel::USUARIO_ID => 'required|exists:usuarios,id',
            UsuariosModulosModel::MODULO_ID => 'required|exists:modulo,id'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            UsuariosModulosModel::USUARIO_ID => 'required|exists:usuarios,id',
            UsuariosModulosModel::MODULO_ID => 'required|exists:modulo,id'
        ];
    }

    public function reglasStoreUsuariosModulos()
    {
        return [
            UsuariosModulosModel::MODULO_ID => 'required|array',
            UsuariosModulosModel::USUARIO_ID => 'required|exists:usuarios,id',
        ];
    }

    public function storeUsuariosModulos($parametros)
    {
        $modulos = $parametros[UsuariosModulosModel::MODULO_ID];
        $this->modelo
            ->where(UsuariosModulosModel::USUARIO_ID, '=',  $parametros[UsuariosModulosModel::USUARIO_ID])
            ->delete();

        foreach ($modulos as $key => $item) {
            $this->crear([
                UsuariosModulosModel::USUARIO_ID => $parametros[UsuariosModulosModel::USUARIO_ID],
                UsuariosModulosModel::MODULO_ID => $item,
            ]);
        }
    }

    public function getReglasGetModulosByUsuarios()
    {
        return [
            UsuariosModulosModel::USUARIO_ID => 'required|exists:usuarios,id'
        ];
    }

    public function usuariosAndModulos($parametros)
    {
        $query = $this->modeloUsuarios->select(
            'usuarios.id as usuario_id',
            'modulo.nombre as nombre_modulo',
            'modulo.icono',
            'modulo.orden',
            'modulo.id as modulo_id'
        );
        $query->join('det_usuarios_modulos', 'det_usuarios_modulos.usuario_id', '=','usuarios.id');
        $query->join('modulo', 'det_usuarios_modulos.modulo_id', '=','modulo.id');
        
        if (isset($parametros[UsuariosModulosModel::USUARIO_ID])) {
            $query->where(UsuariosModulosModel::USUARIO_ID, '=', $parametros[UsuariosModulosModel::USUARIO_ID]);
        }

        return  $query->orderBy('modulo.orden','ASC')->get();
    }
}
