<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\RolesModulosModel;

class ServicioRolesModulos extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new RolesModulosModel();
        $this->recurso = 'roles_modulos';
    }

    public function getReglasGuardar()
    {
        return [
            RolesModulosModel::ROL_ID => 'required',
            RolesModulosModel::MODULO_ID => 'required'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            RolesModulosModel::ROL_ID => 'required',
            RolesModulosModel::MODULO_ID => 'required'
        ];
    }

}
