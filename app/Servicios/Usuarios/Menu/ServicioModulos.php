<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\Menu\ModulosModel;

class ServicioModulos extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new ModulosModel();
        $this->recurso = 'modulos';
    }

    public function getReglasGuardar()
    {
        return [
            ModulosModel::NOMBRE => 'required|unique:modulo,nombre',
            ModulosModel::ORDEN => 'required|numeric',
            ModulosModel::ICONO => 'required',
        ];
    }

    public function getReglasUpdate()
    {
        return [
            ModulosModel::NOMBRE => 'required',
            ModulosModel::ORDEN => 'required|numeric',
            ModulosModel::ICONO => 'required'
        ];
    }

    public function buscarTodos()
    {
        return $this->modelo->select(
            ModulosModel::ID,
            ModulosModel::NOMBRE,
            ModulosModel::ORDEN,
            ModulosModel::ICONO,
            ModulosModel::DEFAULT
        )->limit(200)
            ->orderBy(ModulosModel::ORDEN, 'asc')->get();
    }
}
