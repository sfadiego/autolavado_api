<?php

namespace App\Servicios\Core\ParametrosHttp;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Core\MensajesConstantes;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ParametrosHttpValidador
{
    use MensajesConstantes;

    public static function validar(Request $request, array $reglas)
    {
        $parametros_enviados = $request->all();
        $parametros_esperados = array_keys($reglas);
        foreach ($parametros_enviados as $key => $value) {
            if (!in_array($key, $parametros_esperados)) {
                throw new ParametroHttpInvalidoException([
                    $key => __(self::$E0031_ERROR_UNEXPECTED_PARAMETER, ["parametro" => $key])
                ]);
            }
        }

        $validador = Validator::make($parametros_enviados, $reglas);

        if ($validador->fails()) {
            throw new ParametroHttpInvalidoException($validador->errors()->messages());
        }
    }

    public static function validar_array($valor, array $reglas)
    {
        $validador = Validator::make($valor, $reglas);

        if ($validador->fails()) {
            throw new ParametroHttpInvalidoException($validador->errors()->messages());
        }
    }
}
