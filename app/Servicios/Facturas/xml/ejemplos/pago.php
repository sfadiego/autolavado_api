<?php
error_reporting(1);
ini_set('display_errors', 1);

require dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'autoload.php';

// Especificar la ruta hacia OpenSSL si es necesario
// UtilCertificado::establecerRutaOpenSSL('E:\OpenSSL-Win64\bin\openssl.exe');

$cfdi = new Comprobante();

// Requerido para pago10
$cfdi->customAttrs['xmlns:pago10'] = 'http://www.sat.gob.mx/Pagos';
$cfdi->customAttrs['xsi:schemaLocation'] .= ' http://www.sat.gob.mx/Pagos http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd';

// Preparar valores
$moneda = 'MXN';
$totalPago = 123.45;
$fecha  = time();
$tipoCambio = 1.0;
$folio = '21';
$serie = 'A';
$lugarExpedicion = '12345';
$fechaPago = time();
$formaPago = 'PUE';

// Establecer valores generales
$cfdi->TipoDeComprobante = 'P';
$cfdi->LugarExpedicion   = $lugarExpedicion;
$cfdi->Folio             = $folio;
$cfdi->Serie             = $serie;
$cfdi->TipoCambio        = $tipoCambio;
$cfdi->Moneda            = 'XXX';
$cfdi->setSubTotal(0);
$cfdi->setTotal(0);
$cfdi->setFecha($fecha);

// Agregar emisor
$cfdi->Emisor = Emisor::init(
    'LAN7008173R5',                    // RFC
    '622',                             // Régimen Fiscal
    'Emisor Ejemplo'                   // Nombre (opcional)
);

// Agregar receptor
$cfdi->Receptor = Receptor::init(
    'XAXX010101000',                   // RFC
    'P01',                             // Uso del CFDI
    'Receptor Ejemplo'                 // Nombre (opcional)
);

// Agregar concepto genérico
$cfdi->agregarConcepto(Concepto::init('84111506', 1, 'ACT', 'Pago', 0, 0));

// Agregar CFDIs relacionados (si aplica)
$tipoRelacion = '02';
$cfdi->CfdiRelacionados = CfdiRelacionados::init($tipoRelacion);
$cfdi->CfdiRelacionados->agregarUUID('670B9562-B30D-52D5-B827-655787665500');
$cfdi->CfdiRelacionados->agregarUUID('550E8400-E29B-41D4-A716-446655440000');

// Preparar datos del Complemento de Pago
$complemento = new ComplementoPago();
$pagoEl = $complemento->addPago(
    $fechaPago,
    $formaPago,
    $moneda,
    $tipoCambio,
    $totalPago,
    null, // pago num operacion (opcional)
    null, // pago rfc ordenante (opcional)
    null, // pago banco ordenante (opcional)
    null, // pago cta ordenante (opcional)
    null, // pago rfc beneficiario (opcional)
    null // pago cta beneficiari (opcional)
);

// Agregar datos del CFDI relacionado (factura a pagar)
$complemento->addDoctoRelacionado($pagoEl,
    '440E8400-E29B-41D4-A716-446655440011', // IdDocumento (UUID)
    'B', // Serie
    '34', // Folio
    'MXN', // MonedaDR (moneda)
    1.0, // TipoCambioDR (tipo de cambio)
    'PPD', // MetodoDePagoDR (método de pago)
    1, // NumParcialidad (número de abono)
    0.0, // ImpSaldoAnt (deuda por pagar)
    $totalPago, // ImpPagado (importe de este abono)
    0.0  // ImpSaldoInsoluto (nuevo saldo a pagar)
);

// Agregar más CFDIs relacionados
// $complemento->addDoctoRelacionado($pagoEl,
//     // ...
// );

// Agregar complemento de pago al CFDI
$cfdi->Complemento[] = $complemento;

// Mostrar XML del CFDI generado hasta el momento (antes de sellar)
// header('Content-type: application/xml; charset=UTF-8');
// echo $cfdi->obtenerXml();
// die;

// Cargar certificado que se utilizará para generar el sello del CFDI
$cert = new UtilCertificado();
$ok = $cert->loadFiles(
    dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.cer',
    dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.key',
    '12345678a'
);
if(!$ok) {
    die('Ha ocurrido un error al cargar el certificado.');
}

$ok = $cfdi->sellar($cert);
if(!$ok) {
    die('Ha ocurrido un error al sellar el CFDI.');
}

// Mostrar XML del CFDI con el sello
header('Content-type: application/xml; charset=UTF-8');
header('Content-Disposition: attachment; filename="xml/tu_archivo.xml');
echo $cfdi->obtenerXml();
die;

// Mostrar objeto que contiene los datos del CFDI
// print_r($cfdi);
