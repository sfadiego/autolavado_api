<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\ClientesModel;
use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\CatalogoClaveClienteModel;
use App\Models\Refacciones\ClaveClienteModel;
use App\Models\Refacciones\ClienteVehiculosModel;
use CatalogoClaveCliente;

class ServicioClientes extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'clientes';
        $this->modelo = new ClientesModel();
        $this->modeloClave = new ClaveClienteModel();
        $this->modeloVehiculo = new ClienteVehiculosModel();

        $this->servicioClave = new ServicioClaveCliente();
        $this->servicioCatalogoClave = new ServicioCatalogoClaveCliente();

        $this->servicioCatContactos = new ServicioContacto();
        $this->servicioCatTipoPago = new ServicioCatTipoPago();
        $this->servicioCatalogoCfdi = new ServicioCatalogoCfdi();
        $this->servicioCatVehiculo = new ServicioClienteVehiculos();
    }

    public function getReglasGuardar()
    {
        return [
            ClientesModel::NUMERO_CLIENTE => 'required',
            ClientesModel::TIPO_REGISTRO => 'required',
            ClientesModel::NOMBRE => 'required',
            ClientesModel::APELLIDO_MATERNO => 'required',
            ClientesModel::APELLIDO_PATERNO => 'required',
            ClientesModel::REGIMEN_FISCAL => 'required',
            ClientesModel::NOMBRE_EMPRESA => 'required',
            ClientesModel::RFC => 'required',
            ClientesModel::DIRECCION => 'required',
            ClientesModel::NUMERO_INT => 'required',
            ClientesModel::NUMERO_EXT => 'required',
            ClientesModel::COLONIA => 'required',
            ClientesModel::MUNICIPIO => 'required',
            ClientesModel::ESTADO => 'required',
            ClientesModel::PAIS => 'required',
            ClientesModel::CODIGO_POSTAL => 'required',
            ClientesModel::TELEFONO => 'required',
            ClientesModel::TELEFONO_2 => 'nullable',
            ClientesModel::TELEFONO_3 => 'nullable',
            ClientesModel::FLOTILLERO => 'nullable|numeric',
            ClientesModel::CORREO_ELECTRONICO => 'required',
            ClientesModel::CORREO_ELECTRONICO_2 => 'nullable',
            ClientesModel::FECHA_NACIMIENTO => 'required|date',
            ClientesModel::CFDI_ID => 'nullable|exists:catalogo_cfdi,id',
            ClientesModel::METODO_PAGO_ID => 'nullable|exists:cat_tipo_pago,id',
            ClientesModel::FORMA_PAGO => 'nullable|string',
            ClientesModel::SALDO => 'nullable',
            ClientesModel::LIMITE_CREDITO => 'nullable',
            ClientesModel::NOTAS => 'nullable',
            ClientesModel::ES_CLIENTE => 'nullable'

        ];
    }

    public function getReglasUpdate()
    {
        return [
            ClientesModel::NUMERO_CLIENTE => 'required',
            ClientesModel::TIPO_REGISTRO => 'required',
            ClientesModel::NOMBRE => 'required',
            ClientesModel::APELLIDO_MATERNO => 'required',
            ClientesModel::APELLIDO_PATERNO => 'required',
            ClientesModel::REGIMEN_FISCAL => 'required',
            ClientesModel::NOMBRE_EMPRESA => 'required',
            ClientesModel::RFC => 'required',
            ClientesModel::DIRECCION => 'required',
            ClientesModel::NUMERO_INT => 'required',
            ClientesModel::NUMERO_EXT => 'required',
            ClientesModel::COLONIA => 'required',
            ClientesModel::MUNICIPIO => 'required',
            ClientesModel::ESTADO => 'required',
            ClientesModel::PAIS => 'required',
            ClientesModel::CODIGO_POSTAL => 'required',
            ClientesModel::TELEFONO => 'required',
            ClientesModel::TELEFONO_2 => 'nullable',
            ClientesModel::TELEFONO_3 => 'nullable',
            ClientesModel::FLOTILLERO => 'nullable|numeric',
            ClientesModel::CORREO_ELECTRONICO => 'required',
            ClientesModel::CORREO_ELECTRONICO_2 => 'nullable',
            ClientesModel::FECHA_NACIMIENTO => 'required|date',
            ClientesModel::CFDI_ID => 'nullable|exists:catalogo_cfdi,id',
            ClientesModel::METODO_PAGO_ID => 'nullable|exists:cat_tipo_pago,id',
            ClientesModel::FORMA_PAGO => 'nullable|string',
            ClientesModel::SALDO => 'nullable',
            ClientesModel::LIMITE_CREDITO => 'nullable',
            ClientesModel::NOTAS => 'nullable'
        ];
    }

    public function getAll()
    {
        $data = $this->buscarTodos();
        foreach ($data as $key => $item) {
            $data[$key]['cfdi'] = isset($item[ClientesModel::CFDI_ID]) ? $this->servicioCatalogoCfdi->getById($item[ClientesModel::CFDI_ID]) : [];
            $data[$key]['metodo_pago'] = isset($item[ClientesModel::METODO_PAGO_ID]) ?  $this->servicioCatTipoPago->getById($item[ClientesModel::METODO_PAGO_ID]) : [];
        }
        return $data;
    }

    public function getOneCliente($cliente_id)
    {
        $data = $this->modelo->where(ClientesModel::ID, $cliente_id)->get();
        foreach ($data as $key => $item) {
            $data[$key]['cfdi'] = isset($item[ClientesModel::CFDI_ID]) ? $this->servicioCatalogoCfdi->getById($item[ClientesModel::CFDI_ID]) : [];
            $data[$key]['metodo_pago'] = isset($item[ClientesModel::METODO_PAGO_ID]) ?  $this->servicioCatTipoPago->getById($item[ClientesModel::METODO_PAGO_ID]) : [];
        }
        return $data;
    }

    public function lastRecord()
    {
        return $this->modelo
            ->orderBy(ClientesModel::ID, 'desc')
            ->first();
    }

    public function getReglasBusquedaNombre()
    {
        return [
            ClientesModel::NOMBRE => 'required'
        ];
    }

    public function searchNombreCliente($parametros)
    {
        //Buscamos coincidencias
        $data = $this->modelo->where(ClientesModel::NOMBRE, 'iLIKE', '%' . $parametros[ClientesModel::NOMBRE] . '%')
            ->orWhere(ClientesModel::APELLIDO_MATERNO, 'iLIKE', '%' . $parametros[ClientesModel::NOMBRE] . '%')
            ->orWhere(ClientesModel::APELLIDO_PATERNO, 'iLIKE', '%' . $parametros[ClientesModel::NOMBRE] . '%')
            ->orWhere(ClientesModel::NOMBRE_EMPRESA, 'iLIKE', '%' . $parametros[ClientesModel::NOMBRE] . '%')
            ->orWhere(ClientesModel::NUMERO_CLIENTE, 'iLIKE', '%' . $parametros[ClientesModel::NOMBRE] . '%')
            ->get();

        foreach ($data as $key => $item) {
            $data[$key]['contactos'] = isset($item[ClientesModel::ID]) ? $this->servicioCatContactos->contactosByClienteId($item[ClientesModel::ID]) : [];

            if (count($data[$key]['contactos']) == 0) {
                $data[$key]['contactos'] = array(
                    'contacto_nombre' => $item[ClientesModel::NOMBRE],
                    'contacto_apellido_paterno' => $item[ClientesModel::APELLIDO_MATERNO],
                    'contacto_apellido_materno' => $item[ClientesModel::APELLIDO_PATERNO],
                    'contacto_telefono' => $item[ClientesModel::TELEFONO],
                    'contacto_correo' => $item[ClientesModel::CORREO_ELECTRONICO]
                );
            }

            $data[$key]["vehiculo"] = isset($item[ClientesModel::ID]) ?  $this->servicioCatVehiculo->getVehiculosCliente($item[ClientesModel::ID]) : [];
        }

        return $data;
    }

    public function getReglasBusquedaCliente()
    {
        return [
            ClientesModel::NOMBRE_EMPRESA => 'required',
            ClientesModel::CORREO_ELECTRONICO => 'nullable',
            ClientesModel::CORREO_ELECTRONICO_2 => 'nullable'
        ];
    }

    public function searchCliente($request)
    {
        $data = $this->modelo->where(ClientesModel::NOMBRE_EMPRESA, 'iLIKE', $request->get(ClientesModel::NOMBRE_EMPRESA))
            ->orWhere(ClientesModel::CORREO_ELECTRONICO, 'iLIKE', $request->get(ClientesModel::CORREO_ELECTRONICO))
            ->orWhere(ClientesModel::CORREO_ELECTRONICO, 'iLIKE', $request->get(ClientesModel::CORREO_ELECTRONICO_2))

            ->orWhere(ClientesModel::CORREO_ELECTRONICO_2, 'iLIKE', $request->get(ClientesModel::CORREO_ELECTRONICO))
            ->orWhere(ClientesModel::CORREO_ELECTRONICO_2, 'iLIKE', $request->get(ClientesModel::CORREO_ELECTRONICO_2))

            ->select(ClientesModel::ID, ClientesModel::NUMERO_CLIENTE, ClientesModel::NOMBRE, ClientesModel::APELLIDO_MATERNO, ClientesModel::APELLIDO_PATERNO, ClientesModel::NOMBRE_EMPRESA)
            ->orderBy(ClientesModel::ID, 'desc')
            ->limit(1)
            ->get();
        //dd($data);
        return $data;
    }

    public function getReglasCliente()
    {
        return [
            ClientesModel::NUMERO_CLIENTE => 'required'
        ];
    }

    public function searchNumeroCliente($parametros)
    {
        $data = $this->modelo->where(ClientesModel::NUMERO_CLIENTE, '=', $parametros[ClientesModel::NUMERO_CLIENTE])
            ->select(ClientesModel::ID)
            ->orderBy(ClientesModel::ID, 'desc')
            ->limit(1)
            ->get();
        return $data;
    }

    public function clientePorClave($parametros)
    {
        return $this->modelo->select(ClientesModel::getTableName() . '.*')
            ->join(
                ClaveClienteModel::getTableName(),
                ClaveClienteModel::getTableName() . '.' . ClaveClienteModel::CLIENTE_ID,
                '=',
                ClientesModel::getTableName() . '.' . ClientesModel::ID
            )
            ->join(
                CatalogoClaveClienteModel::getTableName(),
                ClaveClienteModel::getTableName() . '.' . ClaveClienteModel::CLAVE_ID,
                '=',
                CatalogoClaveClienteModel::getTableName() . '.' . CatalogoClaveClienteModel::ID
            )
            ->where(CatalogoClaveClienteModel::CLAVE, $parametros[CatalogoClaveClienteModel::CLAVE])
            ->get();
    }

    public function getReglasClaveCliente()
    {
        return [
            CatalogoClaveClienteModel::CLAVE => 'required|exists:'.CatalogoClaveClienteModel::getTableName().',clave'
        ];
    }
}
