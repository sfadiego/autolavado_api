<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\ClaveClienteModel;

class ServicioClaveCliente extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'clave_cliente';
        $this->modelo = new ClaveClienteModel();
    }

    public function getReglasGuardar()
    {
        return [
            ClaveClienteModel::CLIENTE_ID => 'required',
            ClaveClienteModel::CLAVE_ID => 'required'
        ];
    }
    
    public function getReglasUpdate()
    {
        return [
            ClaveClienteModel::CLIENTE_ID => 'required',
            ClaveClienteModel::CLAVE_ID => 'required'
        ];
    }

    public function searchClientesId($id)
    {
        $data = $this->modelo->where(ClaveClienteModel::CLIENTE_ID, '=', $id)
            ->orderBy(ClaveClienteModel::ID, 'desc')
            ->get();
        
        return $data;
    }


}
