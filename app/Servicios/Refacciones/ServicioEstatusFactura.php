<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\EstatusFacturaModel;

class ServicioEstatusFactura extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'estatus factura';
        $this->modelo = new EstatusFacturaModel();
    }

    public function getReglasGuardar()
    {
        return [
            EstatusFacturaModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre',

        ];
    }
    public function getReglasUpdate()
    {
        return [
            EstatusFacturaModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre',
        ];
    }

    public const ESTATUS_CREADA = 1;
    public const ESTATUS_CANCELADA = 2;
}
