<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Models\Refacciones\HistoricoProductosServicioModel;
use App\Servicios\Core\ServicioDB;

class ServicioHistoricoProductosServicio extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'productos servicio';
        $this->modelo = new HistoricoProductosServicioModel();
    }

    public function getReglasGuardar()
    {
        return [
            HistoricoProductosServicioModel::PRODUCTO_ID => 'required|exists:producto,id',
            HistoricoProductosServicioModel::PRODUCTO_SERVICIO_ID => 'required|numeric',
            HistoricoProductosServicioModel::NO_IDENTIFICACION => 'required',
            HistoricoProductosServicioModel::CANTIDAD => 'required'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            HistoricoProductosServicioModel::PRODUCTO_ID => 'nullable|exists:producto,id',
            HistoricoProductosServicioModel::PRODUCTO_SERVICIO_ID => 'nullable|numeric',
            HistoricoProductosServicioModel::NO_IDENTIFICACION => 'nullable',
            HistoricoProductosServicioModel::CANTIDAD => 'required'
        ];
    }

    public function store($parametros)
    {
        $validar = $this->validarProductos($parametros); 
        if (isset($validar)) {
            throw new ParametroHttpInvalidoException([
                'msg' => "El producto ya se registro previamente."
            ]);
        }

        $array_parametros = $parametros->all();
        $array_parametros[HistoricoProductosServicioModel::CANTIDAD] = round($parametros->get(HistoricoProductosServicioModel::CANTIDAD));
        return $this->crear($array_parametros);
    }

    public function validarProductos($parametros)
    {
        return $this->modelo
            ->where(HistoricoProductosServicioModel::PRODUCTO_ID, '=', $parametros->get(HistoricoProductosServicioModel::PRODUCTO_ID))
            ->Orwhere(HistoricoProductosServicioModel::PRODUCTO_SERVICIO_ID, '=', $parametros->get(HistoricoProductosServicioModel::PRODUCTO_SERVICIO_ID))
            ->get()->first();
    }
}
