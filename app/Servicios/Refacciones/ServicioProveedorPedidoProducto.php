<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Models\Refacciones\ProductosPedidosModel;
use App\Models\Refacciones\ProveedorPedidoProductoModel;
use App\Models\Refacciones\ProveedorRefacciones;
use App\Servicios\Core\ServicioDB;
use Illuminate\Support\Facades\DB;

class ServicioProveedorPedidoProducto extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'proveedor pedido';
        $this->modelo = new ProveedorPedidoProductoModel();
        $this->servicioProductoPedidos = new ServicioProductoPedidos();
    }

    public function getReglasGuardar()
    {
        return [
            ProveedorPedidoProductoModel::PROVEEDOR_ID => 'required|numeric|exists:' . ProveedorRefacciones::getTableName() . ',id',
            ProveedorPedidoProductoModel::PRODUCTO_PEDIDO_ID => 'required|numeric|exists:' . ProductosPedidosModel::getTableName() . ',id',
            ProveedorPedidoProductoModel::CANTIDAD_PROVEEDOR => 'required|numeric'

        ];
    }
    public function getReglasUpdate()
    {
        return [
            ProveedorPedidoProductoModel::PROVEEDOR_ID => 'required|numeric|exists:' . ProveedorRefacciones::getTableName() . ',id',
            ProveedorPedidoProductoModel::PRODUCTO_PEDIDO_ID => 'required|numeric|exists:' . ProductosPedidosModel::getTableName() . ',id',
            ProveedorPedidoProductoModel::CANTIDAD_PROVEEDOR => 'nullable|numeric'
        ];
    }

    public function getProveedorByItem($item_id)
    {
        return $this->modelo
            ->select(
                ProveedorPedidoProductoModel::getTableName() . '.' . ProveedorPedidoProductoModel::ID,
                ProveedorPedidoProductoModel::getTableName() . '.' . ProveedorPedidoProductoModel::CANTIDAD_PROVEEDOR,
                ProveedorRefacciones::getTableName() . '.' . ProveedorRefacciones::PROVEEDOR_NOMBRE
            )
            ->join(
                ProveedorRefacciones::getTableName(),
                ProveedorRefacciones::getTableName() . '.' . ProveedorRefacciones::ID,
                ProveedorPedidoProductoModel::getTableName() . '.' . ProveedorPedidoProductoModel::PROVEEDOR_ID
            )
            ->where(ProveedorPedidoProductoModel::PRODUCTO_PEDIDO_ID, $item_id)
            ->get();
    }

    public function validarGuardar($parametros)
    {
        $total_proveedores = $this->sumaTotalPiezas($parametros[ProveedorPedidoProductoModel::PRODUCTO_PEDIDO_ID]);
        $pedido = $this->servicioProductoPedidos->getById($parametros[ProveedorPedidoProductoModel::PRODUCTO_PEDIDO_ID]);
        $cantidad_actual_proveedores = is_null($total_proveedores->cantidad) ? 0 +  $parametros[ProveedorPedidoProductoModel::CANTIDAD_PROVEEDOR] : $total_proveedores->cantidad + $parametros[ProveedorPedidoProductoModel::CANTIDAD_PROVEEDOR];
        $cantidad_solicitada_pedido = $pedido->cantidad_solicitada;

        if($cantidad_solicitada_pedido >= $cantidad_actual_proveedores){
            return $this->crear($parametros);
        }else{
            throw new ParametroHttpInvalidoException([
                'msg' => "la cantidad solicitada sobrepasa al pedido total."
            ]);
        }
    }

    public function sumaTotalPiezas($item_id)
    {
        return $this->modelo
            ->select(
                DB::raw('sum(' . ProveedorPedidoProductoModel::CANTIDAD_PROVEEDOR . ') as cantidad')
            )->where(ProveedorPedidoProductoModel::PRODUCTO_PEDIDO_ID, '=', $item_id)->first();
        
    }
}
