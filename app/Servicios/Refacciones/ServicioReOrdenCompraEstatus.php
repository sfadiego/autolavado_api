<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\ReComprasEstatusModel;
use App\Models\Refacciones\EstatusCompra;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ServicioReOrdenCompraEstatus extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 're estatus compras';
        $this->modelo = new ReComprasEstatusModel();
    }

    public function getReglasGuardar() {
        return [
            ReComprasEstatusModel::COMPRA_ID => 'required|numeric|exists:orden_compra,id',
            ReComprasEstatusModel::ESTATUS_COMPRA_ID => 'required|numeric|exists:estatus_compra,id',
            ReComprasEstatusModel::ACTIVO => 'nullable|boolean',
            ReComprasEstatusModel::USER_ID => 'nullable',
        ];
    }
    
    public function getReglasUpdate() {
        return [
            ReComprasEstatusModel::COMPRA_ID => 'required|numeric|exists:orden_compra,id',
            ReComprasEstatusModel::ESTATUS_COMPRA_ID => 'required|numeric|exists:estatus_compra,id',
            ReComprasEstatusModel::ACTIVO => 'nullable|boolean',
            ReComprasEstatusModel::USER_ID => 'nullable',
        ];
    }

    public function storeReOrdenCompraEstatus($request) {
        try {
            $params = $this->arrayReOrdenCompraEstatus($request);
            //ParametrosHttpValidador::validar($params, $this->getReglasGuardar());
            $this->updateActivosByOrdenCompra($params[ReComprasEstatusModel::COMPRA_ID]);
            return $this->modelo->create($params);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function updateActivosByOrdenCompra($orden_compra_id) {
        return DB::table($this->modelo->getTableName())
        ->where(ReComprasEstatusModel::COMPRA_ID, $orden_compra_id)
        ->update([ReComprasEstatusModel::ACTIVO => false]);
    }

    private function arrayReOrdenCompraEstatus($request) {
        return  [
            ReComprasEstatusModel::COMPRA_ID => $request[ReComprasEstatusModel::COMPRA_ID],
            ReComprasEstatusModel::ESTATUS_COMPRA_ID => isset($request[ReComprasEstatusModel::ESTATUS_COMPRA_ID]) ? $request[ReComprasEstatusModel::ESTATUS_COMPRA_ID] : EstatusCompra::ESTATUS_PROCESO, 
            ReComprasEstatusModel::USER_ID => Auth::user() ? Auth::user()->id : 1  //Ver de donde obtendremos este valor
        ];
    }
}
