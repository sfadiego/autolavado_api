<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\CatalogoUbicacionModel;
use App\Servicios\Core\ServicioDB;

class ServicioCatalogoUbicacion extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'ubicacion';
        $this->modelo = new CatalogoUbicacionModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoUbicacionModel::NOMBRE => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatalogoUbicacionModel::NOMBRE => 'required'
        ];
    }
}
