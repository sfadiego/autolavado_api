<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\CatalogoClaveClienteModel;

class ServicioCatalogoClaveCliente extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catalogo_clave_cliente';
        $this->modelo = new CatalogoClaveClienteModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoClaveClienteModel::CLAVE => 'required',
            CatalogoClaveClienteModel::NOMBRE => 'required'
        ];
    }
    
    public function getReglasUpdate()
    {
        return [
            CatalogoClaveClienteModel::CLAVE => 'required',
            CatalogoClaveClienteModel::NOMBRE => 'required'
        ];
    }

    public function getlistado()
    {
        return $this->modelo->orderBy(CatalogoClaveClienteModel::ID, 'asc')->get();
    }
}
