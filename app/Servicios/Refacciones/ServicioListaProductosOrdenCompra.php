<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\ListaProductosOrdenCompraModel;
use Illuminate\Support\Facades\DB;

class ServicioListaProductosOrdenCompra extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'productos orden compra';
        $this->servicioProducto = new ServicioProductos();
        $this->modelo = new ListaProductosOrdenCompraModel();
    }

    public function getReglasGuardar()
    {
        return [
            ListaProductosOrdenCompraModel::PRODUCTO_ID => 'required',
            ListaProductosOrdenCompraModel::TOTAL => 'nullable',
            ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID => 'required',
            ListaProductosOrdenCompraModel::CANTIDAD => 'required|numeric',
            ListaProductosOrdenCompraModel::PRECIO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ListaProductosOrdenCompraModel::PRODUCTO_ID => 'required',
            ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID => 'required',
            ListaProductosOrdenCompraModel::CANTIDAD => 'numeric',
            ListaProductosOrdenCompraModel::PRECIO => '',
            ListaProductosOrdenCompraModel::TOTAL => 'nullable'
        ];
    }

    public function store($request)
    {
        $producto = $this->existeProducto($request->orden_compra_id, $request->producto_id);

        if (count($producto) > 0) {
            $total_productos = $producto[0][ListaProductosOrdenCompraModel::CANTIDAD] + $request->cantidad;
            $total = $total_productos * $request->precio;
            $data[ListaProductosOrdenCompraModel::PRODUCTO_ID] = $request->producto_id;
            $data[ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID] = $request->orden_compra_id;
            $data[ListaProductosOrdenCompraModel::PRECIO] = $request->precio;
            $data[ListaProductosOrdenCompraModel::CANTIDAD] = $total_productos;
            $data[ListaProductosOrdenCompraModel::TOTAL] = $total;
            return DB::table($this->modelo->getTableName())
                ->where(ListaProductosOrdenCompraModel::ID, $producto[0][ListaProductosOrdenCompraModel::ID])
                ->update($data);
        } else {
            $data[ListaProductosOrdenCompraModel::PRODUCTO_ID] = $request->producto_id;
            $data[ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID] = $request->orden_compra_id;
            $data[ListaProductosOrdenCompraModel::PRECIO] = $request->precio;
            $data[ListaProductosOrdenCompraModel::CANTIDAD] = $request->cantidad;
            $data[ListaProductosOrdenCompraModel::TOTAL] = $request->cantidad * $request->precio;
            return $this->modelo->create($data);
        }
    }

    public function existeProducto($orden_compra_id, $pructo_id)
    {
        return $this->modelo
            ->where(ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID, $orden_compra_id)
            ->where(ListaProductosOrdenCompraModel::PRODUCTO_ID, $pructo_id)
            ->get()->toArray();
    }

    public function relacionByOrdenCompra($orden_compra_id)
    {
        $data = $this->modelo
            ->where(ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID, $orden_compra_id)
            ->get();
        $productos = [];
        foreach ($data as $key => $items) {
            $productos[$key]['productos_orden_compra'] = $items;
            $productos[$key]['productos'] = $this->servicioProducto->getproductodataByid($items->producto_id)->toArray();
        }

        return $productos;
    }

    public function totalByOrdenCompra($id)
    {
        $data = $this->modelo
            ->where(ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID, $id)
            ->get();

        $total = 0;
        foreach ($data as $key => $item) {
            $total = $total + $item->total;
        }

        return ["total" => $total];
    }
}
