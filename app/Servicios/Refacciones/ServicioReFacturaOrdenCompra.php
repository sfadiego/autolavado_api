<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\ReFacturaOrdenCompraModel;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Support\Facades\DB;
use App\Servicios\Refacciones\ServicioEstatusFactura;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\EstatusFacturaModel;
use App\Models\Refacciones\ListaProductosOrdenCompraModel;
use App\Models\Refacciones\OrdenCompraModel;
use App\Models\Refacciones\ReComprasEstatusModel;
use Illuminate\Support\Facades\Auth;


class ServicioReFacturaOrdenCompra extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 're factura producto';
        $this->modelo = new ReFacturaOrdenCompraModel();
        $this->modeloOrdenCompra = new OrdenCompraModel();
        $this->modeloProductosOrdenCompra = new ListaProductosOrdenCompraModel();
        $this->servicioProductosOrdenCompra = new ServicioListaProductosOrdenCompra();
        $this->servicioVentaProducto = new ServicioVentaProducto();
        $this->servicioReOrdenCompraEstatus = new ServicioReOrdenCompraEstatus();
        $this->modeloReComprasEstatus = new ReComprasEstatusModel();
    }

    public function getReglasGuardar()
    {
        return [
            ReFacturaOrdenCompraModel::FACTURA_ID => 'required|numeric|exists:factura,id',
            ReFacturaOrdenCompraModel::ORDEN_COMPRA_ID => 'required|numeric|exists:producto,id',
            ReFacturaOrdenCompraModel::ESTATUS_FACTURA_ID => 'required|numeric|exists:estatus_factura,id',
            ReFacturaOrdenCompraModel::ACTIVO => 'required|boolean',
            ReFacturaOrdenCompraModel::USER_ID => 'nullable',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ReFacturaOrdenCompraModel::FACTURA_ID => 'required|numeric|exists:factura,id',
            ReFacturaOrdenCompraModel::ORDEN_COMPRA_ID => 'required|numeric|exists:producto,id',
            ReFacturaOrdenCompraModel::ESTATUS_FACTURA_ID => 'required|numeric|exists:estatus_factura,id',
            ReFacturaOrdenCompraModel::ACTIVO => 'required|boolean',
            ReFacturaOrdenCompraModel::USER_ID => 'nullable',
        ];
    }

    public function storeReFacturaOrden($request)
    {
        try {
            //ParametrosHttpValidador::validar($params, $this->getReglasGuardar());
            $params = $this->arrayReFacturaOrden($request);
            $this->updateActivosByFacturaOrdenCompra($params); //Actualizar registros anteriores como inactivos
            return $this->modelo->create($params);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function updateActivosByFacturaOrdenCompra($params)
    {
        $params['activo'] = false;
        return DB::table($this->modelo->getTableName())
            ->where(ReFacturaOrdenCompraModel::FACTURA_ID, $params['factura_id'])
            ->where(ReFacturaOrdenCompraModel::ORDEN_COMPRA_ID, $params['orden_compra_id'])
            ->update($params);
    }

    private function arrayReFacturaOrden($request)
    {
        return  [
            'factura_id' => $request['factura_id'],
            'orden_compra_id' => $request['orden_compra_id'],
            'estatus_factura_id' => ServicioEstatusFactura::ESTATUS_CREADA,
            'user_id' => Auth::user() ? Auth::user()->id : 1  //Ver de donde obtendremos este valor
        ];
    }

    public function getReglasCancelarFactura()
    {
        return [
            ReFacturaOrdenCompraModel::FACTURA_ID => 'required|exists:re_factura_ordencompra,factura_id',
            ReFacturaOrdenCompraModel::USER_ID => 'required|numeric|exists:usuarios,id'
        ];
    }

    public function cancelarFactura($parametros)
    {
        $orden_compra = $this->modeloOrdenCompra
            ->where(OrdenCompraModel::FACTURA_ID, $parametros[OrdenCompraModel::FACTURA_ID])
            ->first();

        $productos_orden_compra = $this->modeloProductosOrdenCompra
            ->where(ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID, $orden_compra->id)
            ->get();
        if ($this->tieneVentasProductos($productos_orden_compra)) {
            throw new ParametroHttpInvalidoException([
                'error' => self::$I0009_ORDEN_COMPRA_TIENE_VENTAS
            ]);
        }

        // SE TIENE QUE HACER UN INSERT Y PONER ACTIVO EN TRUE, LOS DEMAS SERÍA EN ACTIVO EN FALSE PARA LLEVAR HISTORICO
        $this->massUpdateWhereId(
            ReFacturaOrdenCompraModel::FACTURA_ID,
            $parametros[ReFacturaOrdenCompraModel::FACTURA_ID],
            [
                ReFacturaOrdenCompraModel::ESTATUS_FACTURA_ID => EstatusFacturaModel::ESTATUS_CANCELADA
            ]
        );

        $re_estatus_compra = [
            ReComprasEstatusModel::COMPRA_ID => $orden_compra->id,
            ReComprasEstatusModel::ESTATUS_COMPRA_ID =>  EstatusCompra::ESTATUS_CANCELADA,
            ReComprasEstatusModel::USER_ID => $parametros[ReComprasEstatusModel::USER_ID]
        ];
        
        return $this->servicioReOrdenCompraEstatus->storeReOrdenCompraEstatus($re_estatus_compra);
          
    }

    public function tieneVentasProductos($item_productos)
    {
        $ventas = 0;
        foreach ($item_productos as $key => $item) {
            $respuesta['producto_id'] = $item->producto_id;
            $venta_producto = $this->servicioVentaProducto->getVentasProductoByIdProducto($respuesta);
            if ($venta_producto) {
                $ventas++;
            }
        }
        return $ventas > 0 ? true : false;

    }
}
