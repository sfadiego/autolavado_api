<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\ReEstatusTraspasoModel;

class ServicioReEstatusTraspaso extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 're_estatus_traspaso';
        $this->modelo = new ReEstatusTraspasoModel();
    }

    public function getReglasGuardar()
    {
        return [
            ReEstatusTraspasoModel::TRASPASO_ID => 'required|numeric|exists:traspasos:id',
            ReEstatusTraspasoModel::ESTATUS_ID => 'required|numeric|exists:estatus_traspaso:id',
            ReEstatusTraspasoModel::STOCK => 'nullable',
            ReEstatusTraspasoModel::USER_ID => 'required|numeric|exists:usuarios:id'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ReEstatusTraspasoModel::TRASPASO_ID => 'nullable|numeric|exists:traspasos:id',
            ReEstatusTraspasoModel::ESTATUS_ID => 'nullable|numeric|exists:estatus_traspaso:id',
            ReEstatusTraspasoModel::STOCK => 'nullable',
            ReEstatusTraspasoModel::ACTIVO => 'nullable',
            ReEstatusTraspasoModel::USER_ID => 'nullable|numeric|exists:usuarios:id'
        ];
    }
}
