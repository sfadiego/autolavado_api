<?php

namespace App\Servicios\Refacciones;


use App\Servicios\Core\ServicioDB;
use App\Exceptions\ParametroHttpInvalidoException;
use App\Models\Refacciones\DevolucionProveedorModel;
use App\Models\Refacciones\ListaProductosOrdenCompraModel;
use App\Models\Refacciones\ProductoAlmacenModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\Traspasos;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\ReComprasEstatusModel;
use App\Models\Refacciones\OrdenCompraModel;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Support\Facades\DB;

class ServicioDevolucionProveedor extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'devolucion proveedor';
        $this->modelo = new DevolucionProveedorModel();
        $this->modeloProductosOrdenCompra = new ListaProductosOrdenCompraModel();
        $this->modeloProductoAlmacen = new ProductoAlmacenModel();
        $this->servicioVentaProducto = new ServicioVentaProducto();
        $this->servicioProductoAlmacen = new ServicioProductoAlmacen();
        $this->servicioProductos = new ServicioProductos();
        $this->servicioDesgloseProducto = new ServicioDesgloseProductos();
        $this->servicioDevolucionAlmacen = new ServicioDevolucionAlmacen();
    }

    public function getReglasGuardar()
    {
        return [
            DevolucionProveedorModel::ORDEN_COMPRA_ID => 'required|numeric|exists:orden_compra,id',
            DevolucionProveedorModel::FACTURA => 'required',
            DevolucionProveedorModel::OBSERVACIONES => 'required',
            DevolucionProveedorModel::FECHA => 'required|date',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            DevolucionProveedorModel::ORDEN_COMPRA_ID => 'nullable|numeric|exists:orden_compra,id',
            DevolucionProveedorModel::FACTURA => 'nullable',
            DevolucionProveedorModel::OBSERVACIONES => 'nullable',
            DevolucionProveedorModel::FECHA => 'nullable|date',
        ];
    }

    public function getReglasBusquedaTraspaso()
    {
        return [
            Traspasos::FECHA_INICIO => 'nullable|date',
            Traspasos::FECHA_FIN => 'nullable|date',
            OrdenCompraModel::FOLIO_ID => 'nullable|exists:folios,id',
        ];
    }

    public function getByOrdenCompraId($id)
    {
        return $this->modelo->where(DevolucionProveedorModel::ORDEN_COMPRA_ID, $id)->first();
    }

    public function verificaVentasOrdenCompra($orden_compra_id)
    {
        $productos_orden_compra = $this->modeloProductosOrdenCompra
            ->where(ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID, $orden_compra_id)
            ->get();
        
        return $this->existeStockSuficiente($productos_orden_compra);    
        
    }

    private function existeStockSuficiente($productos_orden_compra)
    {
        $data = [];
        foreach ($productos_orden_compra  as $item) {
            $stock = $this->servicioDesgloseProducto->getStockByProducto(['producto_id' => $item->producto_id]);
            if (isset($stock) && $stock) {
                if ($item->cantidad > $stock->cantidad_actual) {
                    throw new ParametroHttpInvalidoException([
                        'producto' => __(self::$I0007_INSUFICIENTES_PRODUCTOS_ALMACEN, ["parametro" => $item->producto_id])
                    ]);
                } else {
                    $total = $stock->cantidad_actual - $item->cantidad;
                    $data[$item->producto_id] = $total;
                }
            }
            else {
                throw new ParametroHttpInvalidoException([
                    'producto' => __(self::$I0008_NO_EXISTE_PRODUCTO, ["parametro" => $item->producto_id])
                ]);
            }
        }
        return $data;
       
    }
    public function descontarAlmacenPorOrdenCompra($orden_compra_id)
    {
        $productos_orden_compra = $this->modeloProductosOrdenCompra
            ->where(ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID, $orden_compra_id)
            ->get();

        return $this->descontarProductoAlmacen($productos_orden_compra);
    }

    public function descontarProductoAlmacen($productos_orden_compra)
    {
        $data = [];
        foreach ($productos_orden_compra  as $item) {
            $stock = $this->servicioDesgloseProducto->getStockByProducto(['producto_id' => $item->producto_id]);
            if ($stock) {
                if ($item->cantidad > $stock->cantidad_actual) {
                    throw new ParametroHttpInvalidoException([
                        'producto' => __(self::$I0007_INSUFICIENTES_PRODUCTOS_ALMACEN, ["parametro" => $item->producto_id])
                    ]);
                } else {
                    $params = [
                        'orden_compra_id' => $item->orden_compra_id,
                        'producto_id' => $item->producto_id,
                        'almacen_id' => 1,
                        'cantidad' => $item->cantidad
                    ];
                    $data[$item->producto_id] = $this->servicioDevolucionAlmacen->crear($params);
                }
            }

            return $data;
        }
    }

    public function actualizarInventarioPorOrdenCompra($orden_compra_id)
    {
        $productos_orden_compra = $this->modeloProductosOrdenCompra
            ->where(ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID, $orden_compra_id)
            ->get();

        return $this->actualizaInventario($productos_orden_compra);
    }

    public function actualizaInventario($productos_orden_compra)
    {
        $data = [];
        foreach ($productos_orden_compra  as $item) {
            $data[$item->producto_id] = $this->servicioDesgloseProducto->actualizaStockByProducto(['producto_id' => $item->producto_id]);
        }
        return $data;
    }

    public function getDevolucionProveedorByFechas($request) {
        $tableDevolucionProveedor = DevolucionProveedorModel::getTableName();
        $tableCompras = OrdenCompraModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableProductoCompra = ListaProductosOrdenCompraModel::getTableName();
        $tableProducto = ProductosModel::getTableName();
        $tableReCompraEstatus = ReComprasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusCompra::getTableName();
        $query = DB::table($tableDevolucionProveedor)
            ->join($tableCompras, $tableCompras . '.' . OrdenCompraModel::ID, '=', $tableDevolucionProveedor . '.' . DevolucionProveedorModel::ORDEN_COMPRA_ID)
            ->join($tableProductoCompra, $tableProductoCompra . '.' . ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID, '=', $tableCompras . '.' . OrdenCompraModel::ID)
            ->join($tableFolios, $tableCompras . '.' . OrdenCompraModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableProducto, $tableProductoCompra . '.' . ListaProductosOrdenCompraModel::PRODUCTO_ID, '=', $tableProducto . '.' . ProductosModel::ID)
            ->join($tableReCompraEstatus, $tableReCompraEstatus . '.' . ReComprasEstatusModel::COMPRA_ID, '=', $tableCompras . '.' . OrdenCompraModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusCompra::ID, '=', $tableReCompraEstatus . '.' . ReComprasEstatusModel::ESTATUS_COMPRA_ID)
            ->select(
                'orden_compra.id',
                'folios.folio',
                'productos_orden_compra.cantidad',
                'productos_orden_compra.total as total_compra',
                'productos_orden_compra.precio',
                'producto.no_identificacion',
                'producto.descripcion',
                'producto.unidad',
                'orden_compra.created_at',
                'devolucion_proveedor.observaciones',
                'devolucion_proveedor.factura',
                $tableEstatusVenta . '.' . EstatusCompra::NOMBRE . ' as estatusCompra',
                $tableEstatusVenta . '.' . EstatusCompra::ID . ' as estatusId'
            );
            if ($request->get(Traspasos::FECHA_INICIO) && $request->get(Traspasos::FECHA_FIN)) {
                $query->whereBetween('orden_compra.created_at', [$request->get(Traspasos::FECHA_INICIO) . ' 00:00:00', $request->get(Traspasos::FECHA_FIN) . ' 23:59:59']);
            }
            if ($request->get(DevolucionProveedorModel::FOLIO_ID)) {
                $query->where($tableDevolucionProveedor . '.' . DevolucionProveedorModel::FOLIO_ID, $request->get(DevolucionProveedorModel::FOLIO_ID));
            }
            $query->where($tableReCompraEstatus . '.' . ReComprasEstatusModel::ACTIVO, true)->get();
        return [
            'data' => $query->get()
        ];

    }

    public function getTotalesDevolucionProveedorByFechas($request)
    {
        $tableDevolucionProveedor = DevolucionProveedorModel::getTableName();
        $tableCompras = OrdenCompraModel::getTableName();
        $tableFolios = FoliosModel::getTableName();
        $tableProductoCompra = ListaProductosOrdenCompraModel::getTableName();
        $tableProducto = ProductosModel::getTableName();
        $tableReCompraEstatus = ReComprasEstatusModel::getTableName();
        $tableEstatusVenta = EstatusCompra::getTableName();
        $query = DB::table($tableDevolucionProveedor)
            ->join($tableCompras, $tableCompras . '.' . OrdenCompraModel::ID, '=', $tableDevolucionProveedor . '.' . DevolucionProveedorModel::ORDEN_COMPRA_ID)
            ->join($tableProductoCompra, $tableProductoCompra . '.' . ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID, '=', $tableCompras . '.' . OrdenCompraModel::ID)
            ->join($tableFolios, $tableCompras . '.' . OrdenCompraModel::FOLIO_ID, '=', $tableFolios . '.' . FoliosModel::ID)
            ->join($tableProducto, $tableProductoCompra . '.' . ListaProductosOrdenCompraModel::PRODUCTO_ID, '=', $tableProducto . '.' . ProductosModel::ID)
            ->join($tableReCompraEstatus, $tableReCompraEstatus . '.' . ReComprasEstatusModel::COMPRA_ID, '=', $tableCompras . '.' . OrdenCompraModel::ID)
            ->join($tableEstatusVenta, $tableEstatusVenta . '.' . EstatusCompra::ID, '=', $tableReCompraEstatus . '.' . ReComprasEstatusModel::ESTATUS_COMPRA_ID)            ->select(
                DB::raw('count(orden_compra.id) as total_compras'),
                DB::raw('sum(productos_orden_compra.cantidad) as total_cantidad'),
                DB::raw('sum(productos_orden_compra.total) as sum_compra_total'),
                DB::raw('sum(productos_orden_compra.precio) as sum_valor_unitario')
            );
            if ($request->get(Traspasos::FECHA_INICIO) && $request->get(Traspasos::FECHA_FIN)) {
                $query->whereBetween('orden_compra.created_at', [$request->get(Traspasos::FECHA_INICIO) . ' 00:00:00', $request->get(Traspasos::FECHA_FIN) . ' 23:59:59']);
            }
            if ($request->get(DevolucionProveedorModel::FOLIO_ID)) {
                $query->where($tableDevolucionProveedor . '.' . DevolucionProveedorModel::FOLIO_ID, $request->get(DevolucionProveedorModel::FOLIO_ID));
            }
            $query->where($tableReCompraEstatus . '.' . ReComprasEstatusModel::ACTIVO, true)->get();
            return $query->first();

    }

}
