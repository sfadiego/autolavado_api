<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\EstatusInventarioModel;

class ServicioEstatusInventario extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'estatus inventario';
        $this->modelo = new EstatusInventarioModel();
    }

    public function getReglasGuardar()
    {
        return [
            EstatusInventarioModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre',

        ];
    }
    public function getReglasUpdate()
    {
        return [
            EstatusInventarioModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre',
        ];
    }
}
