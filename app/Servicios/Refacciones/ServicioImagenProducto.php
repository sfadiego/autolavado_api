<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\ImagenProductoModel;
use App\Models\Refacciones\ProductosModel;
use App\Servicios\Core\ServicioDB;

class ServicioImagenProducto extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new ImagenProductoModel();
        $this->recurso = 'imagenes';
    }

    public function getReglasGuardar()
    {
        return [
            ImagenProductoModel::NOMBRE_ARCHIVO => 'nullable',
            ImagenProductoModel::RUTA_ARCHIVO => 'nullable',
            ImagenProductoModel::PRODUCTO_ID => 'required|exists:' . ProductosModel::getTableName() . ',' . ProductosModel::ID
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ImagenProductoModel::NOMBRE_ARCHIVO => 'nullable',
            ImagenProductoModel::RUTA_ARCHIVO => 'nullable',
            ImagenProductoModel::PRODUCTO_ID => 'required|exists:' . ProductosModel::getTableName() . ',' . ProductosModel::ID
        ];
    }

    public function imagenByProducto($producto_id)
    {
        return $this->modelo->where(ImagenProductoModel::PRODUCTO_ID , $producto_id)->get();
    }

    
}
