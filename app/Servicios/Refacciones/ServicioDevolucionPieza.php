<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\DevolucionPiezaModel;
use App\Models\Refacciones\ProductosModel;


class ServicioDevolucionPieza extends ServicioDB
{

	public function __construct()
	{
		$this->recurso = 'devolucion pieza';
		$this->modelo = new DevolucionPiezaModel();
	}

	public function getReglasGuardar()
	{
		return [
			DevolucionPiezaModel::ORDEN_COMPRA_ID => 'required|exists:orden_compra,id',
			DevolucionPiezaModel::PRODUCTO_ID => 'required|exists:producto,id',
			DevolucionPiezaModel::CANTIDAD => 'required|numeric',
			DevolucionPiezaModel::TOTAL => 'required|numeric',
			DevolucionPiezaModel::NOTA_CREDITO => 'nullable',
			DevolucionPiezaModel::OBSERVACIONES => 'nullable'
		];
	}

	public function getPiezasDevueltasByOrdenCompraId($orden_compra_id) {
        $table_productos = ProductosModel::getTableName();
        $tableDevolucion = DevolucionPiezaModel::getTableName();
		return $this->modelo
			->select(
				DevolucionPiezaModel::getTableName() . "." .'*',
				ProductosModel::getTableName() . "." .ProductosModel::DESCRIPCION,
				ProductosModel::getTableName() . "." .ProductosModel::NO_IDENTIFICACION_FACTURA
			)
            ->join($table_productos, $table_productos . '.' . ProductosModel::ID, '=', $tableDevolucion . '.' . DevolucionPiezaModel::PRODUCTO_ID)
			->where(DevolucionPiezaModel::ORDEN_COMPRA_ID, $orden_compra_id)
			->get();
	}

}
