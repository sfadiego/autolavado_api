<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Models\Refacciones\InventarioModel;
use App\Models\Refacciones\EstatusInventarioModel;
use App\Models\Refacciones\InventarioProductosModel;
use App\Models\Usuarios\User;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioDB;

class ServicioInventario extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'inventario';
        $this->modelo = new InventarioModel();
        $this->modelo_productos_inventario = new InventarioProductosModel();

    }

    public function getReglasGuardar()
    {
        return [
            InventarioModel::USUARIO_REGISTRO => 'required|exists:' . User::getTableName() . ',id',
            InventarioModel::ESTATUS_INVENTARIO_ID => 'required|numeric'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            InventarioModel::USUARIO_REGISTRO => 'required|exists:' . User::getTableName() . ',id',
            InventarioModel::ESTATUS_INVENTARIO_ID => 'required|numeric',
            InventarioModel::JUSTIFICACION => 'required|string',
            InventarioModel::USUARIO_ACTUALIZO => 'required|exists:' . User::getTableName() . ',id'         
        ];
    }

    public function buscarTodos()
    {
        return $this->modelo->select(
            InventarioModel::getTableName() . '.' . InventarioModel::ID,
            InventarioModel::getTableName() . '.' . InventarioModel::ESTATUS_INVENTARIO_ID,
            InventarioModel::getTableName() . '.' . InventarioModel::USUARIO_REGISTRO,
            InventarioModel::getTableName() . '.' . InventarioModel::JUSTIFICACION,
            InventarioModel::getTableName() . '.' . InventarioModel::CREATED_AT,
            User::getTableName() . '.' . User::NOMBRE,
            User::getTableName() . '.' . User::APELLIDO_MATERNO,
            User::getTableName() . '.' . User::APELLIDO_PATERNO,
            'tbl_usuario' . '.' . User::NOMBRE . ' as conciliacion_nombre',
            'tbl_usuario' . '.' . User::APELLIDO_MATERNO . ' as conciliacion_apellido1',
            'tbl_usuario' . '.' . User::APELLIDO_PATERNO . ' as conciliacion_apellido2',
            EstatusInventarioModel::getTableName() . '.' . EstatusInventarioModel::NOMBRE . ' as EstatusInventario'
        )
            ->join(
                User::getTableName(),
                User::getTableName() . '.' . User::ID,
                '=',
                InventarioModel::getTableName() . '.' . InventarioModel::USUARIO_REGISTRO
            )
            ->leftJoin(
                User::getTableName() . ' as tbl_usuario',
                'tbl_usuario' . '.' . User::ID,
                '=',
                InventarioModel::getTableName() . '.' . InventarioModel::USUARIO_ACTUALIZO
            )
            ->join(
                EstatusInventarioModel::getTableName(),
                EstatusInventarioModel::getTableName() . '.' . EstatusInventarioModel::ID,
                '=',
                InventarioModel::getTableName() . '.' . InventarioModel::ESTATUS_INVENTARIO_ID
            )
            ->get();
    }

    public function validarInventarioActivo()
    {
        $activo = $this->getWhere(InventarioModel::ESTATUS_INVENTARIO_ID, 1);
        return count($activo) > 0 ? true : false;
    }

    public function store($request)
    {
        if ($this->validarInventarioActivo()) {
            throw new ParametroHttpInvalidoException([
                'msg' => 'Ya existe un inventario activo'
            ]);
        }

        return $this->crear([
            InventarioModel::ESTATUS_INVENTARIO_ID => 1,
            InventarioModel::USUARIO_REGISTRO => $request->get(InventarioModel::USUARIO_REGISTRO),
        ]);
    }

    public function getProductosInventario($inventario_id) {
        return $this->modelo_productos_inventario->where(InventarioProductosModel::INVENTARIO_ID, $inventario_id)->get();
    }
}
