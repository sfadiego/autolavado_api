<?php

namespace App\Servicios\Refacciones\Kardex;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\Cardex\MovimientosModel;

class ServicioTipoMovimientos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catalogo movimientos';
        $this->modelo = new MovimientosModel();
    }

    public function getReglasGuardar()
    {
        return [
            MovimientosModel::NOMBRE => 'required'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            MovimientosModel::NOMBRE => 'required'
        ];
    }
}
