<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorCobrar\CuentasMorosasModel;

class ServicioCuentasMorosas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'cuentas morosas';
        $this->modelo = new CuentasMorosasModel();
    }

    public function getReglasGuardar()
    {
        return [
            CuentasMorosasModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            CuentasMorosasModel::ABONO_ID => 'required|exists:abonos_por_cobrar,id',
            CuentasMorosasModel::DIAS_MORATORIOS => 'nullable|numeric',
			CuentasMorosasModel::MONTO_MORATORIO => 'nullable|numeric'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CuentasMorosasModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
            CuentasMorosasModel::ABONO_ID => 'required|exists:abonos_por_cobrar,id',
            CuentasMorosasModel::DIAS_MORATORIOS => 'nullable|numeric',
			CuentasMorosasModel::MONTO_MORATORIO => 'nullable|numeric'        ];
    }

    public function crearActualizarCuentasMorosas($cuenta_por_cobrar_id, array $data)
    {
        return $this->createOrUpdate(
            [
                CuentasMorosasModel::CUENTA_POR_COBRAR_ID => $cuenta_por_cobrar_id,
                CuentasMorosasModel::ABONO_ID => $data[CuentasMorosasModel::ABONO_ID],
                CuentasMorosasModel::DIAS_MORATORIOS => $data[CuentasMorosasModel::DIAS_MORATORIOS],
                CuentasMorosasModel::MONTO_MORATORIO => $data[CuentasMorosasModel::MONTO_MORATORIO]
            ],
            $data
        );
    }
}
