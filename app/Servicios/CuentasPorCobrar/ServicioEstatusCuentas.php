<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;

class ServicioEstatusCuentas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'estatus cuentas';
        $this->modelo = new EstatusCuentaModel();
    }

  
}
