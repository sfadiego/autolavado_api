<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorCobrar\AbonosModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\CuentasPorPagar\CatTipoAbonoModel;
use App\Models\CuentasPorPagar\CatEstatusAbonoModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\FoliosModel;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Servicios\CuentasPorCobrar\ServicioCuentasMorosas;


class ServicioAbono extends ServicioDB
{
	public function __construct()
	{
		$this->recurso = 'abonos';
		$this->modelo = new AbonosModel();
		$this->modelo_cuentas_cobrar = new CuentasPorCobrarModel();
		$this->modelo_plazo_credito = new PlazoCreditoModel();
		$this->servicio_cuentas_morosas = new ServicioCuentasMorosas();

	}

	public function getReglasGuardar()
	{
		return [
			AbonosModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
			AbonosModel::TIPO_ABONO_ID => 'required|numeric',
			AbonosModel::TIPO_PAGO_ID => 'nullable|exists:cat_tipo_pago,id',
			AbonosModel::ESTATUS_ABONO_ID => 'required|exists:cat_estatus_abono,id',
			AbonosModel::FECHA_VENCIMIENTO => 'required|date',
			AbonosModel::TOTAL_ABONO => 'required|numeric',
			AbonosModel::FECHA_PAGO => 'nullable|date',
			AbonosModel::TOTAL_PAGO => 'nullable|numeric',
			AbonosModel::CFDI_ID => 'required|exists:catalogo_cfdi,id'
		];
	}
	public function getReglasUpdate()
	{
		return [
			AbonosModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
			AbonosModel::TIPO_ABONO_ID => 'required|numeric',
			AbonosModel::TIPO_PAGO_ID => 'required|exists:cat_tipo_pago,id',
			AbonosModel::ESTATUS_ABONO_ID => 'required|exists:cat_estatus_abono,id',
			AbonosModel::FECHA_VENCIMIENTO => 'nullable|date',
			AbonosModel::TOTAL_ABONO => 'nullable|numeric',
			AbonosModel::FECHA_PAGO => 'required|date',
			AbonosModel::TOTAL_PAGO => 'required|numeric',
			AbonosModel::DIAS_MORATORIOS => 'nullable|numeric',
			AbonosModel::MONTO_MORATORIO => 'nullable|numeric',
			AbonosModel::CFDI_ID => 'required|exists:catalogo_cfdi,id'
		];
	}

	public function getReglasVerificaAbonosPendientes()
	{
		return [
			AbonosModel::CUENTA_POR_COBRAR_ID => 'required|exists:cuentas_por_cobrar,id',
			AbonosModel::FECHA_ACTUAL => 'required|date'
		];
	}

	public function getReglasEstatus()
	{
		return [
			AbonosModel::ESTATUS_ABONO_ID => 'required|exists:cat_estatus_abono,id',
		];
	}

	public function getById($id) {
		return $this->modelo
		->select(
			'abonos_por_cobrar.*',
			'cat_tipo_pago.nombre as tipo_pago',
			'cat_tipo_abono.nombre as tipo_abono',
			'catalogo_cfdi.clave as clave_cfdi',
			'catalogo_cfdi.descripcion as descripcion_cfdi',
			'cat_estatus_abono.nombre as estatus_abono'
			)
		->leftJoin('cat_tipo_pago' , 'cat_tipo_pago.id', '=' , 'abonos_por_cobrar.tipo_pago_id')
		->leftjoin('cat_tipo_abono' , 'cat_tipo_abono.id', '=' , 'abonos_por_cobrar.tipo_abono_id')
		->leftjoin('catalogo_cfdi' , 'catalogo_cfdi.id', '=' , 'abonos_por_cobrar.cfdi_id')
		->leftJoin('cat_estatus_abono' , 'cat_estatus_abono.id', '=' , 'abonos_por_cobrar.estatus_abono_id')
		->find($id);
	}
	public function getAbonosPorPagar($params)
	{
		$query = $this->modelo
		->select(
			'abonos_por_cobrar.*',
			'cat_tipo_pago.nombre as tipo_pago',
			'cat_tipo_abono.nombre as tipo_abono',
			'catalogo_cfdi.clave as clave_cfdi',
			'catalogo_cfdi.descripcion as descripcion_cfdi',
			'cat_estatus_abono.nombre as estatus_abono'
			)
		->leftJoin('cat_tipo_pago' , 'cat_tipo_pago.id', '=' , 'abonos_por_cobrar.tipo_pago_id')
		->leftjoin('cat_tipo_abono' , 'cat_tipo_abono.id', '=' , 'abonos_por_cobrar.tipo_abono_id')
		->leftjoin('catalogo_cfdi' , 'catalogo_cfdi.id', '=' , 'abonos_por_cobrar.cfdi_id')
		->leftJoin('cat_estatus_abono' , 'cat_estatus_abono.id', '=' , 'abonos_por_cobrar.estatus_abono_id');

		if (isset($params['orden_entrada_id']) && $params['orden_entrada_id']){
			$query->where(AbonosModel::CUENTA_POR_COBRAR_ID, $params['orden_entrada_id']);
		} 
		if (isset($params['tipo_abono_id']) && $params['tipo_abono_id']){
			$query->where(AbonosModel::TIPO_ABONO_ID, $params['tipo_abono_id']);
		} 
		if (isset($params['estatus_abono_id']) && $params['estatus_abono_id']){
			$query->where(AbonosModel::ESTATUS_ABONO_ID, $params['estatus_abono_id']);
		} 
		$query->orderBy(AbonosModel::getTableName().'.'.AbonosModel::ID, 'ASC');
		return $query->get();

	}

	public function getVerificaAbonosPendientes($params)
	{
		$abonos_pendientes = $this->modelo
			->where(AbonosModel::CUENTA_POR_COBRAR_ID, $params['cuenta_por_cobrar_id'])
			->where(AbonosModel::TIPO_ABONO_ID, CatTipoAbonoModel::ABONO)
			->whereNotIn(AbonosModel::ESTATUS_ABONO_ID, [CatEstatusAbonoModel::PAGADO])
			->get();
		
			$cuentas_pagar = false;
			foreach ($abonos_pendientes as $abono) { 
				$date_pago = new Carbon($params[AbonosModel::FECHA_ACTUAL]);
				$date_vencimiento = new Carbon($abono->fecha_vencimiento);
				if ($date_pago > $date_vencimiento) {
					$dias_moratorios = $date_vencimiento->diffInDays($date_pago);
					$porcentaje_interes_moratorio = 3;
					$monto_moratorio = (((($abono->total_abono * $porcentaje_interes_moratorio) / 100) / 30) * $dias_moratorios);

					$modelo = $this->modelo->find($abono->id);
					$modelo->estatus_abono_id = 2;
					$modelo->dias_moratorios = $dias_moratorios;
					$modelo->monto_moratorio = $monto_moratorio;
					$saveAbono = $modelo->save();

					if ($saveAbono) {

						$dataCuentasMorosas = [
							'abono_id' => $abono->id,
							'dias_moratorios' => $dias_moratorios,
							'monto_moratorio'  => $monto_moratorio
						];
						$this->servicio_cuentas_morosas->crearActualizarCuentasMorosas($abono->cuenta_por_cobrar_id, $dataCuentasMorosas);

						$cuentas_pagar = $this->modelo_cuentas_cobrar->find($abono->cuenta_por_cobrar_id); 
						$cuentas_pagar->estatus_cuenta_id = EstatusCuentaModel::ESTATUS_ATRASADO;
						$cuentas_pagar->save();
					}
				}
			}
			return $cuentas_pagar;

	}

	public function getVerificaDiasParaPago($params)
	{

		$facturas_abonar = [];

		$abonos_proximos = $this->getAbonosProximos($params['cuenta_por_cobrar_id']);
		foreach($abonos_proximos as $abono) { 
			$fecha_actual = new Carbon($params[AbonosModel::FECHA_ACTUAL]);
			$date_vencimiento = new Carbon($abono->fecha_vencimiento);
			$dias = $date_vencimiento->diffInDays($fecha_actual);
			if ($dias == 7) {
				$abono['dias_faltantes'] = $dias;
				$facturas_abonar = $abono;
			} 
			else if ($dias == 0) {
				$abono['dias_faltantes'] = $dias;
				$facturas_abonar = $abono;
			} 
		}

		return $facturas_abonar;

	}
	private function getAbonosProximos($cuenta_por_cobrar_id) {
		$tabla_cuentas = CuentasPorCobrarModel::getTableName();
		$tabla_abonos = AbonosModel::getTableName();
		$tabla_clientes = ClientesModel::getTableName();
		$tabla_folios = FoliosModel::getTableName();

		return $this->modelo
		->select(
			$tabla_abonos . '.'. AbonosModel::ID .' as abono_id',
			$tabla_abonos . '.'. AbonosModel::TOTAL_ABONO,
			$tabla_abonos . '.'. AbonosModel::FECHA_VENCIMIENTO,
			$tabla_cuentas. '.'. CuentasPorCobrarModel::ID . ' as cuenta_por_cobrar_id',
			$tabla_cuentas. '.'. CuentasPorCobrarModel::FECHA,
			$tabla_cuentas. '.'. CuentasPorCobrarModel::TOTAL,
			$tabla_cuentas. '.'. CuentasPorCobrarModel::CONCEPTO,
			$tabla_folios. '.'. FoliosModel::FOLIO,
			$tabla_clientes. '.'. ClientesModel::NUMERO_CLIENTE,
			$tabla_clientes. '.'. ClientesModel::NOMBRE,
			$tabla_clientes. '.'. ClientesModel::TELEFONO,
			$tabla_clientes. '.'. ClientesModel::CORREO_ELECTRONICO,
			$tabla_clientes. '.'. ClientesModel::APELLIDO_PATERNO,
			$tabla_clientes. '.'. ClientesModel::APELLIDO_MATERNO
		)
		->join($tabla_cuentas, $tabla_cuentas. '.' .CuentasPorCobrarModel::ID, '=' ,$tabla_abonos. '.' .AbonosModel::CUENTA_POR_COBRAR_ID)
		->join($tabla_clientes, $tabla_clientes. '.' .ClientesModel::ID, '=' ,$tabla_cuentas. '.' .CuentasPorCobrarModel::CLIENTE_ID)
		->join($tabla_folios, $tabla_folios. '.' .FoliosModel::ID, '=' ,$tabla_cuentas. '.' .CuentasPorCobrarModel::FOLIO_ID)
		
		->where([
			AbonosModel::CUENTA_POR_COBRAR_ID => $cuenta_por_cobrar_id,
			AbonosModel::TIPO_ABONO_ID => CatTipoAbonoModel::ABONO
		 ])
		->whereNotIn(AbonosModel::ESTATUS_ABONO_ID, [CatEstatusAbonoModel::PAGADO])
		->orderBy($tabla_abonos .'.'. AbonosModel::ID,'ASC')
		->limit(2)
		
		->get();
	}

	public function getCuentaClienteInfo($abono_id) {
		$tabla_cuentas = CuentasPorCobrarModel::getTableName();
		$tabla_abonos = AbonosModel::getTableName();
		$tabla_clientes = ClientesModel::getTableName();
		$tabla_folios = FoliosModel::getTableName();

		return $this->modelo
		->select(
			$tabla_abonos . '.'. AbonosModel::ID .' as abono_id',
			$tabla_abonos . '.'. AbonosModel::TOTAL_ABONO,
			$tabla_abonos . '.'. AbonosModel::FECHA_VENCIMIENTO,
			$tabla_cuentas. '.'. CuentasPorCobrarModel::ID . ' as cuenta_por_cobrar_id',
			$tabla_cuentas. '.'. CuentasPorCobrarModel::FECHA,
			$tabla_cuentas. '.'. CuentasPorCobrarModel::TOTAL,
			$tabla_cuentas. '.'. CuentasPorCobrarModel::CONCEPTO,
			$tabla_folios. '.'. FoliosModel::FOLIO,
			$tabla_clientes. '.'. ClientesModel::NUMERO_CLIENTE,
			$tabla_clientes. '.'. ClientesModel::NOMBRE,
			$tabla_clientes. '.'. ClientesModel::TELEFONO,
			$tabla_clientes. '.'. ClientesModel::CORREO_ELECTRONICO,
			$tabla_clientes. '.'. ClientesModel::APELLIDO_PATERNO,
			$tabla_clientes. '.'. ClientesModel::APELLIDO_MATERNO
		)
		->join($tabla_cuentas, $tabla_cuentas. '.' .CuentasPorCobrarModel::ID, '=' ,$tabla_abonos. '.' .AbonosModel::CUENTA_POR_COBRAR_ID)
		->join($tabla_clientes, $tabla_clientes. '.' .ClientesModel::ID, '=' ,$tabla_cuentas. '.' .CuentasPorCobrarModel::CLIENTE_ID)
		->join($tabla_folios, $tabla_folios. '.' .FoliosModel::ID, '=' ,$tabla_cuentas. '.' .CuentasPorCobrarModel::FOLIO_ID)
		
		->where([
			$tabla_abonos . '.' . AbonosModel::ID => $abono_id
		 ])
		->first();
	}
	public function crearAbonosByCuentaId(int $cuentas_por_cobrar_id)
	{
		$cuentas_cobrar = $this->modelo_cuentas_cobrar->find($cuentas_por_cobrar_id);
		$tipo_forma_pago_id = $cuentas_cobrar->tipo_forma_pago_id;
		$abonos = [];
		if ($tipo_forma_pago_id == TipoFormaPagoModel::FORMA_CREDITO) {
			$enganche = $cuentas_cobrar->enganche;
			$cantidad_abonos = $this->modelo_plazo_credito->find($cuentas_cobrar->plazo_credito_id)->cantidad_mes;
			if ($enganche >= 1) {
				$pago_enganche = [
					AbonosModel::CUENTA_POR_COBRAR_ID => $cuentas_cobrar->id,
					AbonosModel::TIPO_ABONO_ID => CatTipoAbonoModel::ENGANCHE,
					AbonosModel::ESTATUS_ABONO_ID => CatEstatusAbonoModel::PENDIENTE,
					AbonosModel::FECHA_VENCIMIENTO => $cuentas_cobrar->fecha,
					AbonosModel::TOTAL_ABONO => $cuentas_cobrar->enganche
				];

				$abonos[] = $this->modelo->create($pago_enganche);
			}

			for($num_abono = 1; $num_abono <= $cantidad_abonos; $num_abono++) {
				$abono = ($cuentas_cobrar->importe - $cuentas_cobrar->enganche) / $cantidad_abonos;
				$interesAbono = ($abono * $cuentas_cobrar->tasa_interes) / 100;
				$totalAbono = $abono + $interesAbono;
				$pago_abono = [
					AbonosModel::CUENTA_POR_COBRAR_ID => $cuentas_cobrar->id,
					AbonosModel::TIPO_ABONO_ID => CatTipoAbonoModel::ABONO,
					AbonosModel::ESTATUS_ABONO_ID => CatEstatusAbonoModel::PENDIENTE,
					AbonosModel::FECHA_VENCIMIENTO => Carbon::createFromDate($cuentas_cobrar->fecha)->addMonths($num_abono)->format('Y-m-d'),
					AbonosModel::TOTAL_ABONO => $totalAbono
				];
				$abonos[] = $this->modelo->create($pago_abono);
			}
			return $abonos;

		} else {
			return $this->crear_abonos_contado($cuentas_cobrar);
		}

	}

	private function crear_abonos_contado($cuentas_cobrar) {
		$pago_contado = [
			AbonosModel::CUENTA_POR_COBRAR_ID => $cuentas_cobrar->id,
			AbonosModel::TIPO_ABONO_ID => CatTipoAbonoModel::UN_SOLO_PAGO,
			AbonosModel::ESTATUS_ABONO_ID => CatEstatusAbonoModel::PENDIENTE,
			AbonosModel::FECHA_VENCIMIENTO => $cuentas_cobrar->fecha,
			AbonosModel::TOTAL_ABONO => $cuentas_cobrar->total
		];
		return $this->modelo->create($pago_contado);
	}

	public function actualiza_abonos($request, $id)
	{
		// $data_abono = parent::getById($id);
		// $dias_moratorios = null;
		// $date_pago = new Carbon($request->get(AbonosModel::FECHA_PAGO));
		// $date_vencimiento = new Carbon($data_abono->fecha_vencimiento);
		// if ($date_pago > $date_vencimiento) {
		// 	$dias_moratorios = $date_vencimiento->diffInDays($date_pago);
		// 	$request->merge([AbonosModel::DIAS_MORATORIOS => $dias_moratorios]);
		// }
		$estatus_cuenta = false;
		$request->merge([AbonosModel::TOTAL_ABONO => $request->get(AbonosModel::TOTAL_PAGO)]);
		$update = $this->massUpdateWhereId(AbonosModel::ID, $id, $request->all());
		if ($request->get(AbonosModel::TIPO_ABONO_ID) != CatTipoAbonoModel::UN_SOLO_PAGO)
		{
			$cuentas_por_cobrar_id = $request->get(AbonosModel::CUENTA_POR_COBRAR_ID);
			$cuentas_pagar = $this->modelo_cuentas_cobrar->find($cuentas_por_cobrar_id);
			$total_cuenta = $cuentas_pagar->total;
			$abonos_pendientes = $this->getAbonosPendientes($cuentas_por_cobrar_id);
			$monto = $this->getMontoAbonadoByCuentaId($cuentas_por_cobrar_id);
			$monto_pagado = isset($monto) ? $monto->monto_pagado : null;

			$contar_abonos_pendientes = count($abonos_pendientes) - 1; // Se obtiene el total de abonos pendientes menos el que se va realizar
			$interes_abono = ((($abonos_pendientes[0]->total_abono) * $cuentas_pagar->tasa_interes) / 100); // Obtenemos el interes que se aplica a los abonos

			$abono_sin_interes = ($abonos_pendientes[0]->total_abono - $interes_abono) * $contar_abonos_pendientes; // Restamos el interes a los abonos y lo multiplicamos por los abonos pendientes
			$liquidar_saldo = $abono_sin_interes +  $abonos_pendientes[0]->total_abono; // Suma de los abonos pendientes sin el interes + el abono mensual actual.
			$saldo_actual = $total_cuenta - $monto_pagado;
			if ($saldo_actual == 0 ||  $request->get(AbonosModel::TOTAL_PAGO) >= $liquidar_saldo ) {
				$estatus_cuenta = true;
				$monto_abono_pendiente = 0;
				$estatus_abono = CatEstatusAbonoModel::PAGADO;

				$cuentas_pagar = $this->modelo_cuentas_cobrar->find($cuentas_por_cobrar_id); 
				$cuentas_pagar->total = $monto_pagado;
				$cuentas_pagar->intereses = $monto_pagado - $cuentas_pagar->importe;
				$cuentas_pagar->save();
			} else {
				$monto_abono_pendiente = $saldo_actual / count($abonos_pendientes);
				$estatus_abono = CatEstatusAbonoModel::PENDIENTE;
			}

			foreach ($abonos_pendientes as $item) {
				parent::massUpdateWhereId('id', $item->id, [
					AbonosModel::TOTAL_ABONO => round($monto_abono_pendiente,2),
					AbonosModel::ESTATUS_ABONO_ID => $estatus_abono
				]);
			}

			$abonos_morosos = $this->modelo
			->where(AbonosModel::CUENTA_POR_COBRAR_ID, $cuentas_por_cobrar_id)
			->where(AbonosModel::TIPO_ABONO_ID, CatTipoAbonoModel::ABONO)
			->where(AbonosModel::ESTATUS_ABONO_ID, CatEstatusAbonoModel::MORATORIO)
			->get();
			if ($abonos_morosos && count($abonos_morosos) >= 1) {
				$cuentas_pagar = $this->modelo_cuentas_cobrar->find($cuentas_por_cobrar_id); 
				$cuentas_pagar->estatus_cuenta_id = EstatusCuentaModel::ESTATUS_ATRASADO;
				$cuentas_pagar->save();
			} else {
				
				$cuentas_pagar = $this->modelo_cuentas_cobrar->find($cuentas_por_cobrar_id);
				$cuentas_pagar->estatus_cuenta_id = $estatus_cuenta == true ?  EstatusCuentaModel::ESTATUS_LIQUIDADO : EstatusCuentaModel::ESTATUS_PROCESO;
				$cuentas_pagar->save();
			}
		}
		if ($update) {
			return $this->getCuentaClienteInfo($id);
		} else {
			return false;
		}
	}

	public function getAbonosPendientes($cuentas_por_cobrar_id) {
		return $this->modelo
			->where(AbonosModel::CUENTA_POR_COBRAR_ID, $cuentas_por_cobrar_id)
			->where(AbonosModel::ESTATUS_ABONO_ID, CatEstatusAbonoModel::PENDIENTE)
			->get();
	}

	public function getMontoAbonadoByCuentaId($cuentas_por_cobrar_id)
	{
		$tabla_abonos = AbonosModel::getTableName();
		return $this->modelo
			->select(DB::raw('sum(' . $tabla_abonos . '.total_pago) as monto_pagado'))
			->where(AbonosModel::CUENTA_POR_COBRAR_ID, $cuentas_por_cobrar_id)
			//->where(AbonosModel::TIPO_ABONO_ID, CatTipoAbonoModel::ABONO)
			->first();
	}

	public function changeEstatus(Request $request, $id)
	{ 
		$modelo = $this->modelo->find($id);
		$modelo->estatus_pre_orden_id = $request->get(AbonosModel::ESTATUS_ABONO_ID);
		return $modelo->save();
	}
}
