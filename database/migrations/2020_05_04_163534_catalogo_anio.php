<?php

use App\Models\Refacciones\CatalogoAnioModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CatalogoAnio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatalogoAnioModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatalogoAnioModel::ID);
            $table->string(CatalogoAnioModel::NOMBRE);
            $table->timestamps();
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatalogoAnioModel::getTableName());
    }
}
