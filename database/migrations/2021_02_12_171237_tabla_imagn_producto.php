<?php

use App\Models\Refacciones\ImagenProductoModel;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaImagnProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ImagenProductoModel::getTableName(), function (Blueprint $table) {
            $table->increments(ImagenProductoModel::ID);
            $table->string(ImagenProductoModel::NOMBRE_ARCHIVO);
            $table->string(ImagenProductoModel::RUTA_ARCHIVO);
            $table->boolean(ImagenProductoModel::PRINCIPAL)->default(0);
            $table->unsignedInteger(ImagenProductoModel::PRODUCTO_ID);
            $table->integer(ImagenProductoModel::CATEGORIA_ID)->nullable()->default(ImagenProductoModel::CAT_PRODUCTO);
            $table->foreign(ImagenProductoModel::PRODUCTO_ID)
                ->references(ProductosModel::ID)
                ->on(ProductosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ImagenProductoModel::getTableName());
    }
}
