<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\DevolucionAlmacenModel;
use App\Models\Refacciones\Almacenes;

class CreateTablaDevolucionAlmacen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DevolucionAlmacenModel::getTableName(), function (Blueprint $table) {
            $table->increments(DevolucionAlmacenModel::ID);
            $table->unsignedInteger(DevolucionAlmacenModel::ORDEN_COMPRA_ID);
            $table->unsignedInteger(DevolucionAlmacenModel::PRODUCTO_ID);
            $table->unsignedInteger(DevolucionAlmacenModel::ALMACEN_ID);
            $table->foreign(DevolucionAlmacenModel::ALMACEN_ID)->references(Almacenes::ID)->on(Almacenes::getTableName());
            $table->unsignedInteger(DevolucionAlmacenModel::CANTIDAD);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DevolucionAlmacenModel::getTableName());
    }
}
