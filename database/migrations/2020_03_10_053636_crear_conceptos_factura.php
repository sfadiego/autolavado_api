<?php

use App\Models\Facturas\ConceptosFactura;
use App\Models\Facturas\Factura;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearConceptosFactura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ConceptosFactura::getTableName(), function (Blueprint $table) {
            $table->increments(ConceptosFactura::ID);
            $table->string(ConceptosFactura::CONCEPTO_CLAVE_PROD_SERV);
            $table->string(ConceptosFactura::CONCEPTO_NO_IDENTIFICACION);
            $table->string(ConceptosFactura::CONCEPTO_CANTIDAD);
            $table->string(ConceptosFactura::CONCEPTO_CLAVE_UNIDAD);
            $table->string(ConceptosFactura::CONCEPTO_DESCRIPCION);
            $table->string(ConceptosFactura::CONCEPTO_VALOR_UNITARIO);
            $table->string(ConceptosFactura::CONCEPTO_IMPORTE);
            $table->unsignedInteger(ConceptosFactura::FACTURA_ID);
            $table->foreign(ConceptosFactura::FACTURA_ID)->references(Factura::ID)->on(Factura::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ConceptosFactura::getTableName());
    }
}
