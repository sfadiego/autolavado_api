<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorPagar\CatEstatusAbonoModel;

class CrearTablaEstatusAbonos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatEstatusAbonoModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatEstatusAbonoModel::ID);
            $table->string(CatEstatusAbonoModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatEstatusAbonoModel::getTableName());
    }
}
