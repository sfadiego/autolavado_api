<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\MotivoCancelacionFactura;
use App\Models\Facturas\Factura;
use App\Models\Refacciones\CatalogoMotivoCancelacion;


class CreateTableMotivoCancelacionFactura extends Migration
{
    public function up()
    {
        Schema::create(MotivoCancelacionFactura::getTableName(), function (Blueprint $table) {
            $table->increments(MotivoCancelacionFactura::ID);
            $table->unsignedInteger(MotivoCancelacionFactura::FACTURA_ID);
            $table->unsignedInteger(MotivoCancelacionFactura::MOTIVO_ID);
            $table->foreign(MotivoCancelacionFactura::FACTURA_ID)->references(Factura::ID)->on(Factura::getTableName());
            $table->foreign(MotivoCancelacionFactura::MOTIVO_ID)->references(CatalogoMotivoCancelacion::ID)->on(CatalogoMotivoCancelacion::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MotivoCancelacionFactura::getTableName());
    }
}
