<?php

use App\Models\Refacciones\RemplazoProductoAlmacenModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaRemplazoProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(RemplazoProductoAlmacenModel::getTableName(), function (Blueprint $table) {
            $table->increments(RemplazoProductoAlmacenModel::ID);
            $table->integer(RemplazoProductoAlmacenModel::PRODUCTO_ID);
            $table->string(RemplazoProductoAlmacenModel::NOMBRE_PRODUCTO_ANTERIOR);
            $table->string(RemplazoProductoAlmacenModel::NOMBRE_PRODUCTO_ACTUAL);
            $table->string(RemplazoProductoAlmacenModel::NO_IDENTIFICACION);
            $table->string(RemplazoProductoAlmacenModel::NO_IDENTIFICACION_ANTERIOR);
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(RemplazoProductoAlmacenModel::getTableName());
    }
}
