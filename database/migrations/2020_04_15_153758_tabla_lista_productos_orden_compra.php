<?php

use App\Models\Refacciones\ListaProductosOrdenCompraModel;
use App\Models\Refacciones\OrdenCompraModel;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaListaProductosOrdenCompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ListaProductosOrdenCompraModel::getTableName(), function (Blueprint $table) {
            $table->increments(ListaProductosOrdenCompraModel::ID);
            
            $table->unsignedInteger(ListaProductosOrdenCompraModel::PRODUCTO_ID);
            $table->foreign(ListaProductosOrdenCompraModel::PRODUCTO_ID)->references(ProductosModel::ID)->on(ProductosModel::getTableName());

            $table->unsignedInteger(ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID);
            $table->foreign(ListaProductosOrdenCompraModel::ORDEN_COMPRA_ID)->references(OrdenCompraModel::ID)->on(OrdenCompraModel::getTableName());

            $table->float(ListaProductosOrdenCompraModel::TOTAL);
            $table->float(ListaProductosOrdenCompraModel::PRECIO);
            $table->float(ListaProductosOrdenCompraModel::CANTIDAD);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ListaProductosOrdenCompraModel::getTableName());
    }
}
