<?php

use App\Models\Refacciones\Precios;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CatalogoPrecios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Precios::getTableName(), function (Blueprint $table) {
            $table->increments(Precios::ID);
            $table->bigInteger(Precios::PRECIO_PUBLICO);
            $table->bigInteger(Precios::PRECIO_MAYOREO);
            $table->bigInteger(Precios::PRECIO_INTERNO);
            $table->bigInteger(Precios::PRECIO_TALLER);
            $table->bigInteger(Precios::PRECIO_OTRAS_DISTRIBUIDORAS);
            $table->bigInteger(Precios::IMPUESTO);
            $table->string(Precios::DESCRIPCION);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Precios::getTableName());
    }
}
