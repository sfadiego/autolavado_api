<?php

use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\RequisicionesModel;
use App\Models\Refacciones\VendedorModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Requisiciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(RequisicionesModel::getTableName(), function (Blueprint $table) {
            $table->increments(RequisicionesModel::ID);
            $table->string(RequisicionesModel::FOLIO);
            $table->string(RequisicionesModel::REQUISICION);
            $table->string(RequisicionesModel::CLASE_REQUISICION);
            $table->string(RequisicionesModel::CONSECUTIVO)->nullable();
            $table->unsignedInteger(RequisicionesModel::CLIENTE_ID)->nullable();
            $table->foreign(RequisicionesModel::CLIENTE_ID)->references(ClientesModel::ID)->on(ClientesModel::getTableName());
            $table->unsignedInteger(RequisicionesModel::PRODUCTO_ID)->nullable();
            $table->foreign(RequisicionesModel::PRODUCTO_ID)->references(ProductosModel::ID)->on(ProductosModel::getTableName());
            $table->unsignedInteger(RequisicionesModel::VENDEDOR_ID)->nullable();
            $table->foreign(RequisicionesModel::VENDEDOR_ID)->references(VendedorModel::ID)->on(VendedorModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(RequisicionesModel::getTableName());
    }
}
