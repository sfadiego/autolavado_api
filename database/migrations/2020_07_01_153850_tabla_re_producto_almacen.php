<?php

use App\Models\Refacciones\ProductoAlmacenModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ReProductoAlmacen;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaReProductoAlmacen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ReProductoAlmacen::getTableName(), function (Blueprint $table) {
            $table->increments(ReProductoAlmacen::ID);
            $table->unsignedInteger(ReProductoAlmacen::ID_PRODUCTO_ALMACEN);
            $table->unsignedInteger(ReProductoAlmacen::ID_PRODUCTO);
            $table->foreign(ReProductoAlmacen::ID_PRODUCTO_ALMACEN)->references(ProductoAlmacenModel::ID)->on(ProductoAlmacenModel::getTableName());
            $table->foreign(ReProductoAlmacen::ID_PRODUCTO)->references(ProductosModel::ID)->on(ProductosModel::getTableName());
            $table->integer(ReProductoAlmacen::CANTAIDAD);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ReProductoAlmacen::getTableName());
    }
}
