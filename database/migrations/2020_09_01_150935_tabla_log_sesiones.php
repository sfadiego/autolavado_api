<?php

use App\Models\Usuarios\Log\LogSessionesModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaLogSesiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(LogSessionesModel::getTableName(), function (Blueprint $table) {
            $table->increments(LogSessionesModel::ID);
            $table->unsignedInteger(LogSessionesModel::USUARIO_ID);
            $table->foreign(LogSessionesModel::USUARIO_ID)
                ->references(User::ID)
                ->on(User::getTableName());

            $table->string(LogSessionesModel::DIRECCION_IP);
            $table->string(LogSessionesModel::DESCRIPCION);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop(LogSessionesModel::getTableName());
    }
}
