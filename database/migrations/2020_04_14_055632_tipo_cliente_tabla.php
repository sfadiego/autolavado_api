<?php

use App\Models\Refacciones\TipoClienteModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TipoClienteTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TipoClienteModel::getTableName(), function (Blueprint $table) {
            $table->increments(TipoClienteModel::ID);
            $table->string(TipoClienteModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(TipoClienteModel::getTableName());
    }
}
