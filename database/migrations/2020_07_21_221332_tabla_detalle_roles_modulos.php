<?php

use App\Models\Usuarios\Menu\ModulosModel;
use App\Models\Usuarios\RolesModulosModel;
use App\Models\Usuarios\RolModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaDetalleRolesModulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(RolesModulosModel::getTableName(), function (Blueprint $table) {
            $table->increments(RolesModulosModel::ID);
            $table->unsignedInteger(RolesModulosModel::ROL_ID);
            $table->foreign(RolesModulosModel::ROL_ID)->references(RolModel::ID)->on(RolModel::getTableName());
            
            $table->unsignedInteger(RolesModulosModel::MODULO_ID);
            $table->foreign(RolesModulosModel::MODULO_ID)->references(ModulosModel::ID)->on(ModulosModel::getTableName());
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
