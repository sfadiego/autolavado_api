<?php

use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\CatalogoUbicacionProductoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Productos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ProductosModel::getTableName(), function (Blueprint $table) {
            $table->increments(ProductosModel::ID);

            $table->string(ProductosModel::NO_IDENTIFICACION_FACTURA)->nullable();
            $table->string(ProductosModel::CLAVE_UNIDAD_FACTURA)->nullable();
            $table->string(ProductosModel::CLAVE_PROD_SERV_FACTURA)->nullable();
            $table->string(ProductosModel::DESCRIPCION);
            $table->string(ProductosModel::UNIDAD);
            $table->float(ProductosModel::CANTIDAD)->nullable();
            $table->float(ProductosModel::PRECIO_FACTURA);
            $table->float(ProductosModel::VALOR_UNITARIO);
            $table->boolean(ProductosModel::INVENTARIABLE)->default(1);
            $table->boolean(ProductosModel::EXISTENCIA)->default(1);
            $table->unsignedInteger(ProductosModel::UBICACION_PRODUCTO_ID)->nullable();
            $table->foreign(ProductosModel::UBICACION_PRODUCTO_ID)->references(CatalogoUbicacionProductoModel::ID)->on(CatalogoUbicacionProductoModel::getTableName());
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ProductosModel::getTableName());
    }
}
