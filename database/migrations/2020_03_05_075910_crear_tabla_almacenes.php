<?php

use App\Models\Refacciones\Almacenes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaAlmacenes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Almacenes::getTableName(), function (Blueprint $table) {
            $table->increments(Almacenes::ID);
            $table->string(Almacenes::NOMBRE);
            $table->string(Almacenes::UBICACION);
            $table->string(Almacenes::CODIGO_ALMACEN);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Almacenes::getTableName());
    }
}
