<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;

class CrearTablaEstatusCuenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EstatusCuentaModel::getTableName(), function (Blueprint $table) {
            $table->increments(EstatusCuentaModel::ID);
            $table->string(EstatusCuentaModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EstatusCuentaModel::getTableName());
    }
}
