<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Caja\CatalogoCajaModel;


class CrearTablaCatalogoCaja extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatalogoCajaModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatalogoCajaModel::ID);
            $table->string(CatalogoCajaModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatalogoCajaModel::getTableName());
    }
}
