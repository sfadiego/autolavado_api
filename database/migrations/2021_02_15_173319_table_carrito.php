<?php

use App\Models\Ecommerce\CarritoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCarrito extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CarritoModel::getTableName(), function (Blueprint $table) {
            $table->increments(CarritoModel::ID);
            $table->integer(CarritoModel::ID_USUARIO);
            $table->integer(CarritoModel::ID_PRODUCTO);
            $table->float(CarritoModel::CANTIDAD);
            $table->integer(CarritoModel::VENDIDO);
            $table->timestamps();
            



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CarritoModel::getTableName());
    }
}
