<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Caja\MovimientoCajaModel;
use App\Models\Caja\CorteCajaModel;
use App\Models\Caja\CatalogoCajaModel;

class CrearTablaCorteCaja extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CorteCajaModel::getTableName(), function (Blueprint $table) {
            $table->increments(CorteCajaModel::ID);
            $table->unsignedInteger(CorteCajaModel::CAJA_ID);
            $table->foreign(CorteCajaModel::CAJA_ID)->references(CatalogoCajaModel::ID)->on(CatalogoCajaModel::getTableName());
            $table->unsignedInteger(CorteCajaModel::MOVIMIENTO_CAJA_ID);
            $table->foreign(CorteCajaModel::MOVIMIENTO_CAJA_ID)->references(MovimientoCajaModel::ID)->on(MovimientoCajaModel::getTableName());
            $table->date(CorteCajaModel::FECHA_TRANSACCION);
            $table->unsignedInteger(CorteCajaModel::USUARIO_REGISTRO);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CorteCajaModel::getTableName());
    }
}
