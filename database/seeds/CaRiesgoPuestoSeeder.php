<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaRiesgoPuestoModel as Model;

class CaRiesgoPuestoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private function seedData(){
        return [
            [Model::ID=>1, Model::Numero => 1, Model::Descripcion => 'Clase I'],
            [Model::ID=>2, Model::Numero => 2, Model::Descripcion => 'Clase II'],
            [Model::ID=>3, Model::Numero => 3, Model::Descripcion => 'Clase III'],
            [Model::ID=>4, Model::Numero => 4, Model::Descripcion => 'Clase IV'],
            [Model::ID=>5, Model::Numero => 5, Model::Descripcion => 'Clase V']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}