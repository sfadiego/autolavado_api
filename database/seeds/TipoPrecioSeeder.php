<?php

use App\Models\Refacciones\CatTipoPrecioModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoPrecioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo_precios = [
            [
                CatTipoPrecioModel::ID => 1,
                CatTipoPrecioModel::NOMBRE => 'Público',
            ],
            [
                CatTipoPrecioModel::ID => 2,
                CatTipoPrecioModel::NOMBRE => 'Mayoreo',
            ],
            [
                CatTipoPrecioModel::ID => 3,
                CatTipoPrecioModel::NOMBRE => 'Interno',
            ],
            [
                CatTipoPrecioModel::ID => 4,
                CatTipoPrecioModel::NOMBRE => 'Taller',
            ],
            [
                CatTipoPrecioModel::ID => 5,
                CatTipoPrecioModel::NOMBRE => 'Otras distribuidoras',
            ],
            [
                CatTipoPrecioModel::ID => 6,
                CatTipoPrecioModel::NOMBRE => 'Impuesto',
            ],
           
        ];

        foreach ($tipo_precios as $key => $value) {
            DB::table(CatTipoPrecioModel::getTableName())->insert([
                CatTipoPrecioModel::ID => $value[CatTipoPrecioModel::ID],
                CatTipoPrecioModel::NOMBRE => $value[CatTipoPrecioModel::NOMBRE],
            ]);
        }
    }
}
