<?php

use App\Models\Refacciones\ImagenProductoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImagenesProductoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                ImagenProductoModel::NOMBRE_ARCHIVO => '602d3e06bb7f5_2021-02-17.jpg',
                ImagenProductoModel::RUTA_ARCHIVO => '/productos/',
                ImagenProductoModel::PRODUCTO_ID => 1,
                ImagenProductoModel::PRINCIPAL => 1
            ],
            [
                ImagenProductoModel::NOMBRE_ARCHIVO => '602d3e06bb7f5_2021-02-17.jpg',
                ImagenProductoModel::RUTA_ARCHIVO => '/productos/',
                ImagenProductoModel::PRODUCTO_ID => 2,
                ImagenProductoModel::PRINCIPAL => 1
            ],
            [
                ImagenProductoModel::NOMBRE_ARCHIVO => '602d3e06bb7f5_2021-02-17.jpg',
                ImagenProductoModel::RUTA_ARCHIVO => '/productos/',
                ImagenProductoModel::PRODUCTO_ID => 3,
                ImagenProductoModel::PRINCIPAL => 1
            ],
            [
                ImagenProductoModel::NOMBRE_ARCHIVO => '602d3e06bb7f5_2021-02-17.jpg',
                ImagenProductoModel::RUTA_ARCHIVO => '/productos/',
                ImagenProductoModel::PRODUCTO_ID => 4,
                ImagenProductoModel::PRINCIPAL => 1
            ],
            [
                ImagenProductoModel::NOMBRE_ARCHIVO => '602d3e06bb7f5_2021-02-17.jpg',
                ImagenProductoModel::RUTA_ARCHIVO => '/productos/',
                ImagenProductoModel::PRODUCTO_ID => 5,
                ImagenProductoModel::PRINCIPAL => 1
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(ImagenProductoModel::getTableName())->insert($items);
        }
    }
}
