<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaEstadoCivilModel as Model;

class CaEstadoCivilSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => 1,Model::Descripcion => 'Soltero',Model::id_Genero => 1],
            [Model::ID => 2,Model::Descripcion => 'Casado',Model::id_Genero => 1],
            [Model::ID => 3,Model::Descripcion => 'Divociado',Model::id_Genero => 1],
            [Model::ID => 4,Model::Descripcion => 'Viudo',Model::id_Genero => 1],
            [Model::ID => 5,Model::Descripcion => 'Unión Libre',Model::id_Genero => 1],
            [Model::ID => 6,Model::Descripcion => 'Soltero',Model::id_Genero => 2],
            [Model::ID => 7,Model::Descripcion => 'Casado',Model::id_Genero => 2],
            [Model::ID => 8,Model::Descripcion => 'Divociado',Model::id_Genero => 2],
            [Model::ID => 9,Model::Descripcion => 'Viudo',Model::id_Genero => 2],
            [Model::ID => 10,Model::Descripcion => 'Unión Libre',Model::id_Genero => 2],
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
