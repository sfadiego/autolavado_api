<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Refacciones\CatalogoClaveClienteModel;

class CatalogoClaveClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  [
            [CatalogoClaveClienteModel::NOMBRE => 'CLIENTE VENTA',CatalogoClaveClienteModel::CLAVE => 'CV'],
            [CatalogoClaveClienteModel::NOMBRE => 'CLIENTE SERVICIO',CatalogoClaveClienteModel::CLAVE => 'CS'],
            [CatalogoClaveClienteModel::NOMBRE => 'CLIENTE CRÉDITO',CatalogoClaveClienteModel::CLAVE => 'CC'],
            [CatalogoClaveClienteModel::NOMBRE => 'CLIENTE INTERNO',CatalogoClaveClienteModel::CLAVE => 'CI']
        ];

        foreach ($data as $key => $items) {
            DB::table(CatalogoClaveClienteModel::getTableName())->insert($items);
        }
    }
}
