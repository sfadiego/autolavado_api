<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CatalogoUbicacionesProductoSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(ModulosMenuSeeder::class);
        $this->call(CatalogoMesesSeed::class);
        
        $this->call(PreciosSeeder::class);
        $this->call(ProveedoresSeeder::class);
        $this->call(CfdiSeeder::class);
        $this->call(TipoPagoSeed::class);
        $this->call(TipoClienteSeed::class);
        $this->call(UsuariosSeeder::class);
        $this->call(MovimientosProductosSeeder::class);
        $this->call(TipoPolizaSeeder::class);
        
        $this->call(CatalogoClaveClienteSeeder::class);  

        $this->call(GlobalSeeders::class);
        
        $this->call(CatalogoCuentasSeed::class);
        $this->call(TipoPrecioSeeder::class);
        $this->call(EstatusCompraSeed::class);
        $this->call(EstatusVentaSeed::class);
        $this->call(EstatusFacturaSeed::class);
        $this->call(EstatusTraspasoSeeder::class);
        $this->call(TipoFormaPagoSeed::class);
        $this->call(TipoDocumentoSeeder::class);
        $this->call(PlazoCreditoSeeder::class);
        $this->call(EstatusCuentaSeed::class);
        $this->call(MovimientosCajaSeed::class);
        $this->call(TipoAbonosSeeder::class);
        $this->call(EstatusAbonosSeeder::class);
        $this->call(MenuModulosRolesSeeder::class);
        $this->call(MenuModulosUsuariosSeeder::class);
        
        $this->call(MenuSeccionesSeeder::class);
        $this->call(MenuSubmenusSeeder::class);
        $this->call(MenuVistasSeeder::class);
        // $this->call(UbicacionLlavesSeeder::class);///borrar ??
        $this->call(EstatusInventarioSeeder::class);
        $this->call(CatCajasSeeder::class);
        $this->call(ProductoSeeder::class);
        $this->call(ImagenesProductoSeed::class);
        
    }
}
