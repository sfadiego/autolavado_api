<?php

use App\Models\Refacciones\Precios;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PreciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array(
            [
                Precios::PRECIO_PUBLICO => 70,
                Precios::PRECIO_INTERNO => 60,
                Precios::PRECIO_TALLER => 50,
                Precios::PRECIO_MAYOREO => 40,
                Precios::PRECIO_OTRAS_DISTRIBUIDORAS => 30,
                Precios::IMPUESTO => 16,
                Precios::DESCRIPCION => "NIVEL 1",
            ],
            [
                Precios::PRECIO_PUBLICO => 65,
                Precios::PRECIO_INTERNO => 55,
                Precios::PRECIO_TALLER => 45,
                Precios::PRECIO_MAYOREO => 35,
                Precios::PRECIO_OTRAS_DISTRIBUIDORAS => 25,
                Precios::IMPUESTO => 16,
                Precios::DESCRIPCION => "NIVEL 2",
            ],
            [
                Precios::PRECIO_PUBLICO => 60,
                Precios::PRECIO_INTERNO => 50,
                Precios::PRECIO_TALLER => 40,
                Precios::PRECIO_MAYOREO => 30,
                Precios::PRECIO_OTRAS_DISTRIBUIDORAS => 20,
                Precios::IMPUESTO => 10,
                Precios::DESCRIPCION => "NIVEL 3",
            ],
            
        );
        foreach ($data as $key => $items) {
            DB::table(Precios::getTableName())->insert($items);
        }
    }
}
