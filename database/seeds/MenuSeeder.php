<?php

use App\Models\Usuarios\Menu\MenuModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                MenuModel::ID => 1,
                MenuModel::TEXTO => 'Movimientos',
                MenuModel::ICONO => '',
                MenuModel::MODULO_ID => 2,
                MenuModel::CONTROLADOR => '',
                MenuModel::FUNCION => '',
                MenuModel::CLASE => 'nav-category',
                MenuModel::ROL_ID => 1,
                MenuModel::NIVEL => 2,
                MenuModel::ID_PADRE => 0
            ],
            [
                MenuModel::ID => 9,
                MenuModel::TEXTO => 'Facturas',
                MenuModel::ICONO => '',
                MenuModel::MODULO_ID => 2,
                MenuModel::CONTROLADOR => '',
                MenuModel::FUNCION => '',
                MenuModel::CLASE => 'nav-category',
                MenuModel::ROL_ID => 1,
                MenuModel::NIVEL => 2,
                MenuModel::ID_PADRE => 0
            ],
            [
                MenuModel::ID => 11,
                MenuModel::TEXTO => 'Listado',
                MenuModel::MODULO_ID => 2,
                MenuModel::CONTROLADOR => 'factura',
                MenuModel::FUNCION => 'listado',
                MenuModel::CLASE => '',
                MenuModel::ROL_ID => 1,
                MenuModel::ES_VISIBLE => 1,
                MenuModel::NIVEL => 3,
                MenuModel::ID_PADRE => 9
            ],
            [
                MenuModel::ID => 10,
                MenuModel::TEXTO => 'Consultas',
                MenuModel::ICONO => '',
                MenuModel::MODULO_ID => 2,
                MenuModel::CONTROLADOR => '',
                MenuModel::FUNCION => '',
                MenuModel::CLASE => 'nav-category',
                MenuModel::ROL_ID => 1,
                MenuModel::NIVEL => 2,
                MenuModel::ID_PADRE => 0
            ],
            [
                MenuModel::ID => 12,
                MenuModel::TEXTO => 'Req. mostrador',
                MenuModel::MODULO_ID => 2,
                MenuModel::CONTROLADOR => '',
                MenuModel::FUNCION => '',
                MenuModel::CLASE => '',
                MenuModel::ROL_ID => 1,
                MenuModel::ES_VISIBLE => 1,
                MenuModel::NIVEL => 3,
                MenuModel::ID_PADRE => 10
            ],
            [
                MenuModel::ID => 13,
                MenuModel::TEXTO => 'Compras',
                MenuModel::MODULO_ID => 2,
                MenuModel::CONTROLADOR => '',
                MenuModel::FUNCION => '',
                MenuModel::CLASE => '',
                MenuModel::ROL_ID => 1,
                MenuModel::ES_VISIBLE => 1,
                MenuModel::NIVEL => 3,
                MenuModel::ID_PADRE => 10
            ],
            [
                MenuModel::ID => 2,
                MenuModel::TEXTO => 'Almacen',
                MenuModel::MODULO_ID => 2,
                MenuModel::CONTROLADOR => '',
                MenuModel::FUNCION => '',
                MenuModel::CLASE => '',
                MenuModel::ROL_ID => 1,
                MenuModel::NIVEL => 3,
                MenuModel::ID_PADRE => 1
            ],
            [
                MenuModel::ID => 7,
                MenuModel::TEXTO => 'Salidas',
                MenuModel::MODULO_ID => 2,
                MenuModel::CONTROLADOR => '',
                MenuModel::FUNCION => '',
                MenuModel::CLASE => '',
                MenuModel::ROL_ID => 1,
                MenuModel::NIVEL => 3,
                MenuModel::ID_PADRE => 1
            ],
            [
                MenuModel::ID => 8,
                MenuModel::TEXTO => 'Entradas',
                MenuModel::MODULO_ID => 2,
                MenuModel::CONTROLADOR => '',
                MenuModel::FUNCION => '',
                MenuModel::CLASE => '',
                MenuModel::ROL_ID => 1,
                MenuModel::NIVEL => 3,
                MenuModel::ID_PADRE => 1
            ],
            [
                MenuModel::ID => 3,
                MenuModel::TEXTO => 'Productos',
                MenuModel::MODULO_ID => 2,
                MenuModel::CONTROLADOR => 'productos',
                MenuModel::FUNCION => 'listado',
                MenuModel::CLASE => '',
                MenuModel::ROL_ID => 1,
                MenuModel::ES_VISIBLE => 1,
                MenuModel::NIVEL => 4,
                MenuModel::ID_PADRE => 2
            ],
            [
                MenuModel::ID => 4,
                MenuModel::TEXTO => 'Stock Productos',
                MenuModel::MODULO_ID => 2,
                MenuModel::CONTROLADOR => 'productos',
                MenuModel::FUNCION => 'stock',
                MenuModel::CLASE => '',
                MenuModel::ROL_ID => 1,
                MenuModel::ES_VISIBLE => 1,
                MenuModel::NIVEL => 4,
                MenuModel::ID_PADRE => 2
            ],
            [
                MenuModel::ID => 5,
                MenuModel::TEXTO => 'Traspaso almacen',
                MenuModel::MODULO_ID => 2,
                MenuModel::CONTROLADOR => 'productos',
                MenuModel::FUNCION => 'generarTraspaso',
                MenuModel::CLASE => '',
                MenuModel::ROL_ID => 1,
                MenuModel::ES_VISIBLE => 1,
                MenuModel::NIVEL => 4,
                MenuModel::ID_PADRE => 2
            ],
            [
                MenuModel::ID => 6,
                MenuModel::TEXTO => 'Remplazos',
                MenuModel::MODULO_ID => 2,
                MenuModel::CONTROLADOR => 'productos',
                MenuModel::FUNCION => 'listadoremplazos',
                MenuModel::CLASE => '',
                MenuModel::ROL_ID => 1,
                MenuModel::ES_VISIBLE => 1,
                MenuModel::NIVEL => 4,
                MenuModel::ID_PADRE => 2
            ]
        ];

        foreach ($data as $key => $items) {
            DB::table(MenuModel::getTableName())->insert($items);
        }
    }
}
