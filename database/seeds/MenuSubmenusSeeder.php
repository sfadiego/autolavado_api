<?php

use App\Models\Usuarios\MenuSubmenuModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSubmenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                MenuSubmenuModel::ID => 1,
                MenuSubmenuModel::NOMBRE => "Almacen",
                MenuSubmenuModel::SECCION_ID => 1
            ],
            [
                MenuSubmenuModel::ID => 2,
                MenuSubmenuModel::NOMBRE => "Salidas",
                MenuSubmenuModel::SECCION_ID => 1
            ],
            [
                MenuSubmenuModel::ID => 3,
                MenuSubmenuModel::NOMBRE => "Entradas",
                MenuSubmenuModel::SECCION_ID => 1
            ],
            [
                MenuSubmenuModel::ID => 4,
                MenuSubmenuModel::NOMBRE => "Listado",
                MenuSubmenuModel::SECCION_ID => 2
            ],
            [
                MenuSubmenuModel::ID => 5,
                MenuSubmenuModel::NOMBRE => "Req. Mostrador",
                MenuSubmenuModel::SECCION_ID => 3
            ],
            [
                MenuSubmenuModel::ID => 6,
                MenuSubmenuModel::NOMBRE => "Compras",
                MenuSubmenuModel::SECCION_ID => 3
            ],
            [
                MenuSubmenuModel::ID => 7,
                MenuSubmenuModel::NOMBRE => "Listado",
                MenuSubmenuModel::SECCION_ID => 9
            ],
            [
                MenuSubmenuModel::ID => 8,
                MenuSubmenuModel::NOMBRE => "Roles/modulos",
                MenuSubmenuModel::SECCION_ID => 8
            ],
            [
                MenuSubmenuModel::ID => 11,
                MenuSubmenuModel::NOMBRE => "Otros",
                MenuSubmenuModel::SECCION_ID => 4
            ],
            [
                MenuSubmenuModel::ID => 12,
                MenuSubmenuModel::NOMBRE => "Recepción",
                MenuSubmenuModel::SECCION_ID => 5
            ],
            [
                MenuSubmenuModel::ID => 13,
                MenuSubmenuModel::NOMBRE => "Equipo opcional",
                MenuSubmenuModel::SECCION_ID => 5
            ],
            [
                MenuSubmenuModel::ID => 14,
                MenuSubmenuModel::NOMBRE => "Salidas",
                MenuSubmenuModel::SECCION_ID => 5
            ],
            [
                MenuSubmenuModel::ID => 15,
                MenuSubmenuModel::NOMBRE => "Ordenes de Equipo",
                MenuSubmenuModel::SECCION_ID => 5
            ],
            [
                MenuSubmenuModel::ID => 16,
                MenuSubmenuModel::NOMBRE => "Ventas",
                MenuSubmenuModel::SECCION_ID => 6
            ],
            [
                MenuSubmenuModel::ID => 17,
                MenuSubmenuModel::NOMBRE => "Reportes",
                MenuSubmenuModel::SECCION_ID => 7
            ],
            [
                MenuSubmenuModel::ID => 18,
                MenuSubmenuModel::NOMBRE => "Unidades",
                MenuSubmenuModel::SECCION_ID => 12
            ],
            [
                MenuSubmenuModel::ID => 19,
                MenuSubmenuModel::NOMBRE => "Consultas",
                MenuSubmenuModel::SECCION_ID => 13
            ],
            [
                MenuSubmenuModel::ID => 20,
                MenuSubmenuModel::NOMBRE => "Ventas",
                MenuSubmenuModel::SECCION_ID => 14
            ],
            [
                MenuSubmenuModel::ID => 21,
                MenuSubmenuModel::NOMBRE => "Reportes",
                MenuSubmenuModel::SECCION_ID => 15
            ],
            [
                MenuSubmenuModel::ID => 22,
                MenuSubmenuModel::NOMBRE => "Catalogos",
                MenuSubmenuModel::SECCION_ID => 16
            ],
            [
                MenuSubmenuModel::ID => 23,
                MenuSubmenuModel::NOMBRE => "Movimientos",
                MenuSubmenuModel::SECCION_ID => 16
            ],
            [
                MenuSubmenuModel::ID => 24,
                MenuSubmenuModel::NOMBRE => "Consultas",
                MenuSubmenuModel::SECCION_ID => 16
            ],
            [
                MenuSubmenuModel::ID => 25,
                MenuSubmenuModel::NOMBRE => "Reportes",
                MenuSubmenuModel::SECCION_ID => 16
            ],
            [
                MenuSubmenuModel::ID => 26,
                MenuSubmenuModel::NOMBRE => "Utileria",
                MenuSubmenuModel::SECCION_ID => 16
            ],
            [
                MenuSubmenuModel::ID => 27,
                MenuSubmenuModel::NOMBRE => "Entradas",
                MenuSubmenuModel::SECCION_ID => 18
            ],
            [
                MenuSubmenuModel::ID => 28,
                MenuSubmenuModel::NOMBRE => "Salidas",
                MenuSubmenuModel::SECCION_ID => 18
            ],
            [
                MenuSubmenuModel::ID => 29,
                MenuSubmenuModel::NOMBRE => "Reportes",
                MenuSubmenuModel::SECCION_ID => 18
            ],
            [
                MenuSubmenuModel::ID => 30,
                MenuSubmenuModel::NOMBRE => "Movimientos",
                MenuSubmenuModel::SECCION_ID => 19
            ],
            [
                MenuSubmenuModel::ID => 31,
                MenuSubmenuModel::NOMBRE => "Movimientos",
                MenuSubmenuModel::SECCION_ID => 20
            ],
            [
                MenuSubmenuModel::ID => 32,
                MenuSubmenuModel::NOMBRE => "Otros",
                MenuSubmenuModel::SECCION_ID => 21
            ],
            [
                MenuSubmenuModel::ID => 33,
                MenuSubmenuModel::NOMBRE => 'Trabajador',
                MenuSubmenuModel::SECCION_ID => 22
            ],
            [
                MenuSubmenuModel::ID => 34,
                MenuSubmenuModel::NOMBRE => 'P/D',
                MenuSubmenuModel::SECCION_ID => 22
            ],
            [
                MenuSubmenuModel::ID => 35,
                MenuSubmenuModel::NOMBRE => 'Movimientos',
                MenuSubmenuModel::SECCION_ID => 22
            ],
            [
                MenuSubmenuModel::ID => 36,
                MenuSubmenuModel::NOMBRE => 'Cat. del sistema',
                MenuSubmenuModel::SECCION_ID => 23
            ],
            [
                MenuSubmenuModel::ID => 37,
                MenuSubmenuModel::NOMBRE => 'Organización',
                MenuSubmenuModel::SECCION_ID => 23
            ],
            // [
            //     MenuSubmenuModel::ID => 41,
            //     MenuSubmenuModel::NOMBRE => 'Catálogos',
            //     MenuSubmenuModel::SECCION_ID => 24
            // ],
            // [
            //     MenuSubmenuModel::ID => 42,
            //     MenuSubmenuModel::NOMBRE => 'Organización',
            //     MenuSubmenuModel::SECCION_ID => 24
            // ],
            // [
            //     MenuSubmenuModel::ID => 43,
            //     MenuSubmenuModel::NOMBRE => 'Otros',
            //     MenuSubmenuModel::SECCION_ID => 24
            // ],
            [
                MenuSubmenuModel::ID => 44,
                MenuSubmenuModel::NOMBRE => 'Paneles',
                MenuSubmenuModel::SECCION_ID => 25
            ],
            [
                MenuSubmenuModel::ID => 45,
                MenuSubmenuModel::NOMBRE => 'Paneles',
                MenuSubmenuModel::SECCION_ID => 26
            ],
            [
                MenuSubmenuModel::ID => 46,
                MenuSubmenuModel::NOMBRE => 'Param. Nómina',
                MenuSubmenuModel::SECCION_ID => 22
            ],
            [
                MenuSubmenuModel::ID => 47,
                MenuSubmenuModel::NOMBRE => 'e. Recibos',
                MenuSubmenuModel::SECCION_ID => 22
            ],
            [
                MenuSubmenuModel::ID => 48,
                MenuSubmenuModel::NOMBRE => 'Percepciones y deducciones',
                MenuSubmenuModel::SECCION_ID => 23
            ],
            [
                MenuSubmenuModel::ID => 49,
                MenuSubmenuModel::NOMBRE => 'Consulta de movimientos',
                MenuSubmenuModel::SECCION_ID => 23
            ],
            [
                MenuSubmenuModel::ID => 50,
                MenuSubmenuModel::NOMBRE => 'Dispersión',
                MenuSubmenuModel::SECCION_ID => 23
            ],
            [
                MenuSubmenuModel::ID => 51,
                MenuSubmenuModel::NOMBRE => 'Correo',
                MenuSubmenuModel::SECCION_ID => 23
            ],
            [
                MenuSubmenuModel::ID => 52,
                MenuSubmenuModel::NOMBRE => 'Depositos de documentos',
                MenuSubmenuModel::SECCION_ID => 23
            ],
            // [
            //     MenuSubmenuModel::ID => 53,
            //     MenuSubmenuModel::NOMBRE => 'Tablero de nómina',
            //     MenuSubmenuModel::SECCION_ID => 23
            // ],
            [
                MenuSubmenuModel::ID => 54,
                MenuSubmenuModel::NOMBRE => 'Acum. de bases fiscales',
                MenuSubmenuModel::SECCION_ID => 27
            ],
            [
                MenuSubmenuModel::ID => 55,
                MenuSubmenuModel::NOMBRE => 'Percepciones y deducciones',
                MenuSubmenuModel::SECCION_ID => 27
            ],
            [
                MenuSubmenuModel::ID => 56,
                MenuSubmenuModel::NOMBRE => 'Del trabajador',
                MenuSubmenuModel::SECCION_ID => 27
            ],
            [
                MenuSubmenuModel::ID => 57,
                MenuSubmenuModel::NOMBRE => 'Acumulados de nóminas',
                MenuSubmenuModel::SECCION_ID => 27
            ],
            [
                MenuSubmenuModel::ID => 58,
                MenuSubmenuModel::NOMBRE => 'Recibos electrónicos',
                MenuSubmenuModel::SECCION_ID => 27
            ],
            [
                MenuSubmenuModel::ID => 59,
                MenuSubmenuModel::NOMBRE => 'ISR',
                MenuSubmenuModel::SECCION_ID => 28
            ],
            [
                MenuSubmenuModel::ID => 60,
                MenuSubmenuModel::NOMBRE => 'SDI',
                MenuSubmenuModel::SECCION_ID => 28
            ],
            [
                MenuSubmenuModel::ID => 61,
                MenuSubmenuModel::NOMBRE => 'Avisos al IMSS',
                MenuSubmenuModel::SECCION_ID => 28
            ],
            [
                MenuSubmenuModel::ID => 62,
                MenuSubmenuModel::NOMBRE => 'SUA',
                MenuSubmenuModel::SECCION_ID => 28
            ],
            [
                MenuSubmenuModel::ID => 63,
                MenuSubmenuModel::NOMBRE => 'IMSS',
                MenuSubmenuModel::SECCION_ID => 28
            ],
            [
                MenuSubmenuModel::ID => 64,
                MenuSubmenuModel::NOMBRE => 'Bases fiscales',
                MenuSubmenuModel::SECCION_ID => 28
            ],
            [
                MenuSubmenuModel::ID => 65,
                MenuSubmenuModel::NOMBRE => 'Analizador',
                MenuSubmenuModel::SECCION_ID => 28
            ],

            [
                MenuSubmenuModel::ID => 66,
                MenuSubmenuModel::NOMBRE => 'Nómina',
                MenuSubmenuModel::SECCION_ID => 29
            ],
            [
                MenuSubmenuModel::ID => 67,
                MenuSubmenuModel::NOMBRE => 'Pago de nomina',
                MenuSubmenuModel::SECCION_ID => 29
            ],
            [
                MenuSubmenuModel::ID => 68,
                MenuSubmenuModel::NOMBRE => 'Movimientos de la nómina',
                MenuSubmenuModel::SECCION_ID => 29
            ],
            [
                MenuSubmenuModel::ID => 69,
                MenuSubmenuModel::NOMBRE => 'Catálogos',
                MenuSubmenuModel::SECCION_ID => 29
            ],
            [
                MenuSubmenuModel::ID => 70,
                MenuSubmenuModel::NOMBRE => 'Empresa',
                MenuSubmenuModel::SECCION_ID => 29
            ],
            [
                MenuSubmenuModel::ID => 71,
                MenuSubmenuModel::NOMBRE => 'Administrador de reportes',
                MenuSubmenuModel::SECCION_ID => 29
            ],
            [
                MenuSubmenuModel::ID => 72,
                MenuSubmenuModel::NOMBRE => 'Documentos',
                MenuSubmenuModel::SECCION_ID => 29
            ],
            [
                MenuSubmenuModel::ID => 73,
                MenuSubmenuModel::NOMBRE => 'Disperción',
                MenuSubmenuModel::SECCION_ID => 30
            ],
            [
                MenuSubmenuModel::ID => 74,
                MenuSubmenuModel::NOMBRE => 'Incrementos',
                MenuSubmenuModel::SECCION_ID => 30
            ],
            [
                MenuSubmenuModel::ID => 75,
                MenuSubmenuModel::NOMBRE => 'Cálculo inverso',
                MenuSubmenuModel::SECCION_ID => 30
            ],
            [
                MenuSubmenuModel::ID => 76,
                MenuSubmenuModel::NOMBRE => 'Contratos',
                MenuSubmenuModel::SECCION_ID => 30
            ],
            [
                MenuSubmenuModel::ID => 77,
                MenuSubmenuModel::NOMBRE => 'Finiquitos',
                MenuSubmenuModel::SECCION_ID => 30
            ],
            [
                MenuSubmenuModel::ID => 78,
                MenuSubmenuModel::NOMBRE => 'Fondo de ahorrro',
                MenuSubmenuModel::SECCION_ID => 30
            ],
            [
                MenuSubmenuModel::ID => 79,
                MenuSubmenuModel::NOMBRE => 'PTU',
                MenuSubmenuModel::SECCION_ID => 30
            ],
            [
                MenuSubmenuModel::ID => 80,
                MenuSubmenuModel::NOMBRE => 'FONACOT',
                MenuSubmenuModel::SECCION_ID => 30
            ],
            [
                MenuSubmenuModel::ID => 81,
                MenuSubmenuModel::NOMBRE => 'Cierre de nómina',
                MenuSubmenuModel::SECCION_ID => 30
            ],
            [
                MenuSubmenuModel::ID => 82,
                MenuSubmenuModel::NOMBRE => 'Corte Anual',
                MenuSubmenuModel::SECCION_ID => 30
            ],
            [
                MenuSubmenuModel::ID => 83,
                MenuSubmenuModel::NOMBRE => 'Periodo',
                MenuSubmenuModel::SECCION_ID => 31
            ],
            [
                MenuSubmenuModel::ID => 84,
                MenuSubmenuModel::NOMBRE => 'Acumulados',
                MenuSubmenuModel::SECCION_ID => 31
            ],
            [
                MenuSubmenuModel::ID => 85,
                MenuSubmenuModel::NOMBRE => 'Importación',
                MenuSubmenuModel::SECCION_ID => 31
            ],
            [
                MenuSubmenuModel::ID => 86,
                MenuSubmenuModel::NOMBRE => 'Exportación',
                MenuSubmenuModel::SECCION_ID => 31
            ],
            [
                MenuSubmenuModel::ID => 87,
                MenuSubmenuModel::NOMBRE => 'Eliminacion',
                MenuSubmenuModel::SECCION_ID => 31
            ],
            // [
            //     MenuSubmenuModel::ID => 88,
            //     MenuSubmenuModel::NOMBRE => 'Eliminacion',
            //     MenuSubmenuModel::SECCION_ID => 31
            // ],
            [
                MenuSubmenuModel::ID => 89,
                MenuSubmenuModel::NOMBRE => 'Parametros de la nómina',
                MenuSubmenuModel::SECCION_ID => 31
            ],
            [
                MenuSubmenuModel::ID => 90,
                MenuSubmenuModel::NOMBRE => 'Bitacora',
                MenuSubmenuModel::SECCION_ID => 31
            ],
            [
                MenuSubmenuModel::ID => 91,
                MenuSubmenuModel::NOMBRE => 'Parametros del sistema',
                MenuSubmenuModel::SECCION_ID => 32
            ],
            [
                MenuSubmenuModel::ID => 92,
                MenuSubmenuModel::NOMBRE => 'Empresas',
                MenuSubmenuModel::SECCION_ID => 32
            ],
            [
                MenuSubmenuModel::ID => 93,
                MenuSubmenuModel::NOMBRE => 'Consultas',
                MenuSubmenuModel::SECCION_ID => 32
            ],
            [
                MenuSubmenuModel::ID => 94,
                MenuSubmenuModel::NOMBRE => 'Respaldo',
                MenuSubmenuModel::SECCION_ID => 32
            ],
            [
                MenuSubmenuModel::ID => 95,
                MenuSubmenuModel::NOMBRE => 'Control de archivos',
                MenuSubmenuModel::SECCION_ID => 32
            ],
            [
                MenuSubmenuModel::ID => 96,
                MenuSubmenuModel::NOMBRE => 'Telemarketing',
                MenuSubmenuModel::SECCION_ID => 33
            ],
            [
                MenuSubmenuModel::ID => 97,
                MenuSubmenuModel::NOMBRE => 'Formulario',
                MenuSubmenuModel::SECCION_ID => 34
            ],
            [
                MenuSubmenuModel::ID => 98,
                MenuSubmenuModel::NOMBRE => 'Inventario',
                MenuSubmenuModel::SECCION_ID => 35
            ],
            [
                MenuSubmenuModel::ID => 99,
                MenuSubmenuModel::NOMBRE => 'Listados',
                MenuSubmenuModel::SECCION_ID => 35
            ],
            [
                MenuSubmenuModel::ID => 100,
                MenuSubmenuModel::NOMBRE => 'Personal administrativo',
                MenuSubmenuModel::SECCION_ID => 36
            ],
            [
                MenuSubmenuModel::ID => 101,
                MenuSubmenuModel::NOMBRE => "Polizas",
                MenuSubmenuModel::SECCION_ID => 3
            ],
            [
                MenuSubmenuModel::ID => 102,
                MenuSubmenuModel::NOMBRE => 'Tablero de nomina',
                MenuSubmenuModel::SECCION_ID => 22
            ],
            
            
        );

        foreach ($data as $key => $items) {
            DB::table(MenuSubmenuModel::getTableName())
                ->insert($items);
        }
    }
}
