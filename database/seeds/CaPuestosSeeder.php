<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaPuestosModel as Model;

class CaPuestosSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => 1, Model::Clave => 1,Model::Descripcion => 'Puesto #1',Model::SalarioDiario=> rand(2000, 10000), Model::SalarioMaximo => rand(10000, 15000), Model::id_RiesgoPuesto => rand(1,5)],
            [Model::ID => 2, Model::Clave => 2,Model::Descripcion => 'Puesto #2',Model::SalarioDiario=> rand(2000, 10000), Model::SalarioMaximo => rand(10000, 15000), Model::id_RiesgoPuesto => rand(1,5)],
            [Model::ID => 3, Model::Clave => 3,Model::Descripcion => 'Puesto #3',Model::SalarioDiario=> rand(2000, 10000), Model::SalarioMaximo => rand(10000, 15000), Model::id_RiesgoPuesto => rand(1,5)],
            [Model::ID => 4, Model::Clave => 4,Model::Descripcion => 'Puesto #4',Model::SalarioDiario=> rand(2000, 10000), Model::SalarioMaximo => rand(10000, 15000), Model::id_RiesgoPuesto => rand(1,5)],
            [Model::ID => 5, Model::Clave => 5,Model::Descripcion => 'Puesto #5',Model::SalarioDiario=> rand(2000, 10000), Model::SalarioMaximo => rand(10000, 15000), Model::id_RiesgoPuesto => rand(1,5)],
            [Model::ID => 6, Model::Clave => 6,Model::Descripcion => 'Puesto #6',Model::SalarioDiario=> rand(2000, 10000), Model::SalarioMaximo => rand(10000, 15000), Model::id_RiesgoPuesto => rand(1,5)],
            [Model::ID => 7, Model::Clave => 7,Model::Descripcion => 'Puesto #7',Model::SalarioDiario=> rand(2000, 10000), Model::SalarioMaximo => rand(10000, 15000), Model::id_RiesgoPuesto => rand(1,5)],
            [Model::ID => 8, Model::Clave => 8,Model::Descripcion => 'Puesto #8',Model::SalarioDiario=> rand(2000, 10000), Model::SalarioMaximo => rand(10000, 15000), Model::id_RiesgoPuesto => rand(1,5)],
            [Model::ID => 9, Model::Clave => 9,Model::Descripcion => 'Puesto #9',Model::SalarioDiario=> rand(2000, 10000), Model::SalarioMaximo => rand(10000, 15000), Model::id_RiesgoPuesto => rand(1,5)],
            [Model::ID => 10, Model::Clave => 10,Model::Descripcion => 'Puesto #10',Model::SalarioDiario=> rand(2000, 10000), Model::SalarioMaximo => rand(10000, 15000), Model::id_RiesgoPuesto => rand(1,5)],
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
