<?php

use App\Models\Refacciones\TipoClienteModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoClienteSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $almacenes = [
            [
                TipoClienteModel::ID => 1,
                TipoClienteModel::NOMBRE => 'esporadico',
                
            ],
            [
                TipoClienteModel::ID => 2,
                TipoClienteModel::NOMBRE => 'Registrado',
            ]
        ];

        foreach ($almacenes as $key => $value) {
            DB::table(TipoClienteModel::getTableName())->insert([
                TipoClienteModel::ID => $value[TipoClienteModel::ID],
                TipoClienteModel::NOMBRE => $value[TipoClienteModel::NOMBRE],
            ]);
        }
    }
}
