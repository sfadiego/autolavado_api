<?php

use App\Models\Caja\CatalogoCajaModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatCajasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatalogoCajaModel::ID => 1,
                CatalogoCajaModel::NOMBRE => 'Caja uno'
            ],
            [
                CatalogoCajaModel::ID => 2,
                CatalogoCajaModel::NOMBRE => 'Caja dos'
            ],
            [
                CatalogoCajaModel::ID => 3,
                CatalogoCajaModel::NOMBRE => 'Caja tres'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(CatalogoCajaModel::getTableName())->insert($items);
        }
    }
}
