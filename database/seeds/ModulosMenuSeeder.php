<?php

use App\Models\Usuarios\Menu\ModulosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModulosMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                ModulosModel::ID => 1,
                ModulosModel::NOMBRE => 'DASHBOARD',
                ModulosModel::ORDEN => 2,
                ModulosModel::ICONO => ''
            ],
            [
                ModulosModel::ID => 2,
                ModulosModel::NOMBRE => 'REFACCIONES',
                ModulosModel::ORDEN => 2,
                ModulosModel::ICONO => 'fas fa-tools'
            ],
            [
                ModulosModel::ID => 3,
                ModulosModel::NOMBRE => 'AUTOS',
                ModulosModel::ORDEN => 3,
                ModulosModel::ICONO => 'fas fa-car'
            ],
            [
                ModulosModel::ID => 4,
                ModulosModel::NOMBRE => 'SEMINUEVOS',
                ModulosModel::ORDEN => 4,
                ModulosModel::ICONO => 'fas fa-car-side'
            ],
            [
                ModulosModel::ID => 5,
                ModulosModel::NOMBRE => 'CONTABILIDAD',
                ModulosModel::ORDEN => 5,
                ModulosModel::ICONO => 'fas fa-file-invoice-dollar'
            ],
            [
                ModulosModel::ID => 6,
                ModulosModel::NOMBRE => 'CAJA',
                ModulosModel::ORDEN => 6,
                ModulosModel::ICONO => 'fas fa-cash-register'
            ],
            [
                ModulosModel::ID => 7,
                ModulosModel::NOMBRE => 'CXC',
                ModulosModel::ORDEN => 7,
                ModulosModel::ICONO => 'fas fa-hand-holding-usd'
            ],
            [
                ModulosModel::ID => 8,
                ModulosModel::NOMBRE => 'CXP',
                ModulosModel::ORDEN => 8,
                ModulosModel::ICONO => 'fas fa-donate'
            ],
            [
                ModulosModel::ID => 9,
                ModulosModel::NOMBRE => 'MONITOREO',
                ModulosModel::ORDEN => 9,
                ModulosModel::ICONO => 'fas fa-chart-pie'
            ],
            [
                ModulosModel::ID => 10,
                ModulosModel::NOMBRE => 'ADMINISTRADOR',
                ModulosModel::ORDEN => 10,
                ModulosModel::ICONO => 'fas fa-user-tie'
            ],
            [
                ModulosModel::ID => 11,
                ModulosModel::NOMBRE => 'NOMINA',
                ModulosModel::ORDEN => 11,
                ModulosModel::ICONO => 'fas fa-file-signature'
            ],
            [
                ModulosModel::ID => 12,
                ModulosModel::NOMBRE => 'SISTEMAS',
                ModulosModel::ORDEN => 12,
                ModulosModel::ICONO => 'fas fa-users'
            ],
            [
                ModulosModel::ID => 13,
                ModulosModel::NOMBRE => 'INDICADORES',
                ModulosModel::ORDEN => 13,
                ModulosModel::ICONO => 'fas fa-grip-horizontal'
            ],
            [
                ModulosModel::ID => 14,
                ModulosModel::NOMBRE => 'SERVICIOS',
                ModulosModel::ORDEN => 14,
                ModulosModel::ICONO => 'fas fa-clipboard-list'
            ],
            [
                ModulosModel::ID => 15,
                ModulosModel::NOMBRE => 'BODYSHOP',
                ModulosModel::ORDEN => 15,
                ModulosModel::ICONO => 'fas fa-spray-can'
            ],
            [
                ModulosModel::ID => 16,
                ModulosModel::NOMBRE => 'LOGISTICA',
                ModulosModel::ORDEN => 16,
                ModulosModel::ICONO => 'fas fa-clipboard-check'
            ],
            [
                ModulosModel::ID => 17,
                ModulosModel::NOMBRE => 'CONTROL DE COMBUSTIBLES',
                ModulosModel::ORDEN => 17,
                ModulosModel::ICONO => 'fas fa-gas-pump'
            ],
            [
                ModulosModel::ID => 18,
                ModulosModel::NOMBRE => 'TELEMARKETING',
                ModulosModel::ORDEN => 18,
                ModulosModel::ICONO => 'fas fa-ad'
            ],
            [
                ModulosModel::ID => 19,
                ModulosModel::NOMBRE => 'FINANCIAMIENTO Y SEGUROS',
                ModulosModel::ORDEN => 19,
                ModulosModel::ICONO => 'fas fa-file-medical-alt'
            ],
            [
                ModulosModel::ID => 20,
                ModulosModel::NOMBRE => 'MANUAL DE PROCESOS',
                ModulosModel::ORDEN => 20,
                ModulosModel::ICONO => 'fas fa-book-open'
            ],
            [
                ModulosModel::ID => 21,
                ModulosModel::NOMBRE => 'RR.HH',
                ModulosModel::ORDEN => 21,
                ModulosModel::ICONO => 'far fa-id-badge'
            ],
            [
                ModulosModel::ID => 22,
                ModulosModel::NOMBRE => 'SIN MODULO',
                ModulosModel::ORDEN => 1,
                ModulosModel::ICONO => 'far fa-id-badge',
                ModulosModel::DEFAULT => 1
            ]
        ];

        foreach ($data as $key => $items) {
            DB::table(ModulosModel::getTableName())
                ->insert($items);
        }
    }
}
