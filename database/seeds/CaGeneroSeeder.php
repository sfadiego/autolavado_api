<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaGeneroModel as Model;

class CaGeneroSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => '1',Model::Descripcion => 'Masculino'],
            [Model::ID => '2',Model::Descripcion => 'Femenino']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
