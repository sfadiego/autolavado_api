<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaFormaPagoModel as Model;

class CaFormaPagoSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => '1',Model::Descripcion => 'Efectivo'],
            [Model::ID => '2',Model::Descripcion => 'Cheque'],
            [Model::ID => '3',Model::Descripcion => 'Transferencia']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
