<?php

use App\Models\Refacciones\Almacenes;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\Talleres;
use App\Models\Refacciones\VendedorModel;
use App\Models\Refacciones\ClaveClienteModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GlobalSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(VendedorModel::getTableName())->insert([
            VendedorModel::NOMBRE => 'Vendedor '
        ]);
        
        $clientes = [
        [
            ClientesModel::NOMBRE => 'Diego armando silva  ',
            ClientesModel::NOMBRE_EMPRESA => 'SOHEX',
            ClientesModel::NUMERO_CLIENTE => '1234',
            ClientesModel::RFC => 'SIFASDASDASD123',
            ClientesModel::TIPO_REGISTRO => '1',
            ClientesModel::APELLIDO_MATERNO => 'Apellido Materno',
            ClientesModel::APELLIDO_PATERNO => 'Apellido Paterno',
            ClientesModel::DIRECCION => 'Villa de alvares',
            ClientesModel::NUMERO_INT => '02',
            ClientesModel::NUMERO_EXT => 'SN',
            ClientesModel::ESTADO => 'Colima',
            ClientesModel::MUNICIPIO => 'Colima',
            ClientesModel::COLONIA => 'centro',
            ClientesModel::CODIGO_POSTAL => '28500',
            ClientesModel::TELEFONO => '312116666',
            ClientesModel::TELEFONO_2 => '312116660',
            ClientesModel::CORREO_ELECTRONICO => 'correo_ejemplo@gmail.com',
            ClientesModel::CORREO_ELECTRONICO_2 => 'correo_ejemplo@gmail.com',
            ClientesModel::NOTAS => 'Notas',
            ClientesModel::FECHA_NACIMIENTO => '1990-10-10',
            ClientesModel::CFDI_ID => 1,
            ClientesModel::METODO_PAGO_ID => 1,
            ClientesModel::FLOTILLERO => 1,
            ClientesModel::FORMA_PAGO => 1,
            ClientesModel::LIMITE_CREDITO => 10000,
            ClientesModel::SALDO => 1000
        ],
        [
            ClientesModel::NOMBRE => 'ANGEL MIGUEL',
            ClientesModel::APELLIDO_MATERNO => 'SALAS',
            ClientesModel::APELLIDO_PATERNO => 'CALDERON',
            ClientesModel::NOMBRE_EMPRESA => 'SOHEX',
            ClientesModel::NUMERO_CLIENTE => '00004',
            ClientesModel::REGIMEN_FISCAL => 'M',
            ClientesModel::RFC => 'SOH190520AR4',
            ClientesModel::TIPO_REGISTRO => '1',
            ClientesModel::DIRECCION => 'IGNACIO SANDOVAL 1939',
            ClientesModel::NUMERO_INT => '02',
            ClientesModel::NUMERO_EXT => 'SN',
            ClientesModel::ESTADO => 'COLIMA',
            ClientesModel::MUNICIPIO => 'COLIMA',
            ClientesModel::COLONIA => 'PASEO DE LA CANTERA',
            ClientesModel::CODIGO_POSTAL => '28017',
            ClientesModel::TELEFONO => '3318953592',
            ClientesModel::TELEFONO_2 => '5535527547',
            ClientesModel::TELEFONO_3 => '3338352043',
            ClientesModel::CORREO_ELECTRONICO => 'info@sohexdms.com',
            ClientesModel::CORREO_ELECTRONICO_2 => 'info@sohexdms.com',
            ClientesModel::NOTAS => 'NINGUNA',
            ClientesModel::FECHA_NACIMIENTO => '1990-10-10',
            ClientesModel::CFDI_ID => 1,
            ClientesModel::METODO_PAGO_ID => 1,
            ClientesModel::FLOTILLERO => 1,
            ClientesModel::FORMA_PAGO => 1,
            ClientesModel::LIMITE_CREDITO => 10000,
            ClientesModel::SALDO => 10000
        ],
        [
            ClientesModel::NOMBRE => 'Martin Reyes ',
            ClientesModel::NOMBRE_EMPRESA => 'SOHEX',
            ClientesModel::NUMERO_CLIENTE => '2020',
            ClientesModel::RFC => 'RESL890315ZX8',
            ClientesModel::TIPO_REGISTRO => '2',
            ClientesModel::APELLIDO_MATERNO => 'Reyes',
            ClientesModel::APELLIDO_PATERNO => 'Silva',
            ClientesModel::DIRECCION => 'Maclovio Herrera',
            ClientesModel::NUMERO_INT => '22',
            ClientesModel::NUMERO_EXT => 'SN',
            ClientesModel::ESTADO => 'Colima',
            ClientesModel::MUNICIPIO => 'Colima',
            ClientesModel::COLONIA => 'centro',
            ClientesModel::CODIGO_POSTAL => '28040',
            ClientesModel::TELEFONO => '3122439792',
            ClientesModel::TELEFONO_2 => '3122439792',
            ClientesModel::CORREO_ELECTRONICO => 'lmartinreyes@gmail.com',
            ClientesModel::CORREO_ELECTRONICO_2 => 'lmartinreyes2@gmail.com',
            ClientesModel::NOTAS => 'Notas',
            ClientesModel::FECHA_NACIMIENTO => '1990-10-10',
            ClientesModel::CFDI_ID => 1,
            ClientesModel::METODO_PAGO_ID => 1,
            ClientesModel::FLOTILLERO => 1,
            ClientesModel::FORMA_PAGO => 1,
            ClientesModel::LIMITE_CREDITO => 15000,
            ClientesModel::SALDO => 1000
        ],
        [
            ClientesModel::NOMBRE => 'Alex Diego',
            ClientesModel::NOMBRE_EMPRESA => 'SOHEX Temp',
            ClientesModel::NUMERO_CLIENTE => '1DF56',
            ClientesModel::RFC => 'SIFASDASDASD123',
            ClientesModel::TIPO_REGISTRO => '1',
            ClientesModel::APELLIDO_MATERNO => 'Apellido Materno',
            ClientesModel::APELLIDO_PATERNO => 'Apellido Paterno',
            ClientesModel::DIRECCION => 'Villa de alvares',
            ClientesModel::NUMERO_INT => '76',
            ClientesModel::NUMERO_EXT => 'SN',
            ClientesModel::ESTADO => 'Colima',
            ClientesModel::MUNICIPIO => 'Colima',
            ClientesModel::COLONIA => 'centro',
            ClientesModel::CODIGO_POSTAL => '28500',
            ClientesModel::TELEFONO => '312116666',
            ClientesModel::TELEFONO_2 => '312116660',
            ClientesModel::CORREO_ELECTRONICO => 'correo_ejemplo@gmail.com',
            ClientesModel::CORREO_ELECTRONICO_2 => 'correo_ejemplo@gmail.com',
            ClientesModel::NOTAS => 'Notas',
            ClientesModel::FECHA_NACIMIENTO => '1990-10-10',
            ClientesModel::CFDI_ID => 1,
            ClientesModel::METODO_PAGO_ID => 1,
            ClientesModel::FLOTILLERO => 1,
            ClientesModel::FORMA_PAGO => 1,
            ClientesModel::LIMITE_CREDITO => 50000,
            ClientesModel::SALDO => 1000
        ]
        ];

        foreach ($clientes as $key => $cliente) {
            DB::table(ClientesModel::getTableName())->insert([
                ClientesModel::NOMBRE => $cliente[ClientesModel::NOMBRE],
                ClientesModel::NOMBRE_EMPRESA => $cliente[ClientesModel::NOMBRE_EMPRESA],
                ClientesModel::NUMERO_CLIENTE => $cliente[ClientesModel::NUMERO_CLIENTE],
                ClientesModel::RFC => $cliente[ClientesModel::RFC],
                ClientesModel::TIPO_REGISTRO => $cliente[ClientesModel::TIPO_REGISTRO],
                ClientesModel::APELLIDO_MATERNO => $cliente[ClientesModel::APELLIDO_MATERNO],
                ClientesModel::APELLIDO_PATERNO => $cliente[ClientesModel::APELLIDO_PATERNO],
                ClientesModel::DIRECCION => $cliente[ClientesModel::DIRECCION],
                ClientesModel::ESTADO => $cliente[ClientesModel::ESTADO],
                ClientesModel::MUNICIPIO => $cliente[ClientesModel::MUNICIPIO],
                ClientesModel::COLONIA => $cliente[ClientesModel::COLONIA],
                ClientesModel::CODIGO_POSTAL => $cliente[ClientesModel::CODIGO_POSTAL],
                ClientesModel::TELEFONO => $cliente[ClientesModel::TELEFONO],
                ClientesModel::TELEFONO_2 => $cliente[ClientesModel::TELEFONO],
                ClientesModel::CORREO_ELECTRONICO => $cliente[ClientesModel::CORREO_ELECTRONICO],
                ClientesModel::CORREO_ELECTRONICO_2 => $cliente[ClientesModel::CORREO_ELECTRONICO_2],
                ClientesModel::NOTAS => $cliente[ClientesModel::NOTAS],
                ClientesModel::FECHA_NACIMIENTO => $cliente[ClientesModel::FECHA_NACIMIENTO],
                ClientesModel::CFDI_ID => $cliente[ClientesModel::CFDI_ID],
                ClientesModel::METODO_PAGO_ID => $cliente[ClientesModel::METODO_PAGO_ID],
                ClientesModel::FLOTILLERO => $cliente[ClientesModel::FLOTILLERO],
                ClientesModel::FORMA_PAGO => $cliente[ClientesModel::FORMA_PAGO],
                ClientesModel::LIMITE_CREDITO => $cliente[ClientesModel::LIMITE_CREDITO],
                ClientesModel::SALDO => $cliente[ClientesModel::SALDO]
            ]);
        }

        $claves_clientes =  [
            [ClaveClienteModel::CLIENTE_ID => 1,ClaveClienteModel::CLAVE_ID => 1],
            [ClaveClienteModel::CLIENTE_ID => 2,ClaveClienteModel::CLAVE_ID => 1],
            [ClaveClienteModel::CLIENTE_ID => 3,ClaveClienteModel::CLAVE_ID => 2],
            [ClaveClienteModel::CLIENTE_ID => 4,ClaveClienteModel::CLAVE_ID => 1]
        ];

        foreach ($claves_clientes as $key => $items) {
            DB::table(ClaveClienteModel::getTableName())->insert($items);
        }

        $talleres = [
            [
                Talleres::ID => 1,
                Talleres::NOMBRE => 'Taller 1',
                Talleres::UBICACION => 'colima'
            ],
            [
                Talleres::ID => 2,
                Talleres::NOMBRE => 'Taller 2',
                Talleres::UBICACION => 'colima'
            ],
            [
                Talleres::ID => 3,
                Talleres::NOMBRE => 'Taller 3',
                Talleres::UBICACION => 'colima'
            ]
        ];

        foreach ($talleres as $key => $taller) {
            DB::table(Talleres::getTableName())->insert([
                Talleres::NOMBRE => $taller[Talleres::NOMBRE],
                Talleres::UBICACION => $taller[Talleres::UBICACION]
            ]);
        }

        $almacenes = [
            [
                Almacenes::ID => 1,
                Almacenes::NOMBRE => 'Almacen queretaro',
                Almacenes::UBICACION => 'Queretaro',
                Almacenes::CODIGO_ALMACEN => 'Qrt'
            ],
            [
                Almacenes::ID => 2,
                Almacenes::NOMBRE => 'Almacen San Juan del Rio',
                Almacenes::UBICACION => 'San Juan del Rio',
                Almacenes::CODIGO_ALMACEN => 'SJR'
            ]
        ];

        foreach ($almacenes as $key => $value) {
            DB::table(Almacenes::getTableName())->insert([
                Almacenes::CODIGO_ALMACEN => $value[Almacenes::CODIGO_ALMACEN],
                Almacenes::NOMBRE => $value[Almacenes::NOMBRE],
                Almacenes::UBICACION => $value[Almacenes::UBICACION]
            ]);
        }
    }
}
