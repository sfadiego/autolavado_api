<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaEntidadesFederativasModel as Model;

class CaEntidadesFederativasSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => 1, Model::Clave => 'AGU', Model::Descripcion => 'Aguascalientes'],
            [Model::ID => 2, Model::Clave => 'BNC', Model::Descripcion => 'Baja California'],
            [Model::ID => 3, Model::Clave => 'BNS', Model::Descripcion => 'Baja California Sur'],
            [Model::ID => 4, Model::Clave => 'CAM', Model::Descripcion => 'Campeche'],
            [Model::ID => 5, Model::Clave => 'COA', Model::Descripcion => 'Coahuila de Zaragoza'],
            [Model::ID => 6, Model::Clave => 'COL', Model::Descripcion => 'Colima'],
            [Model::ID => 7, Model::Clave => 'CHP', Model::Descripcion => 'Chiapas'],
            [Model::ID => 8, Model::Clave => 'CHH', Model::Descripcion => 'Chihuahua'],
            [Model::ID => 9, Model::Clave => 'DIF', Model::Descripcion => 'Ciudad de México'],
            [Model::ID => 10, Model::Clave => 'DUR', Model::Descripcion => 'Durango'],
            [Model::ID => 11, Model::Clave => 'GUA', Model::Descripcion => 'Guanajuato'],
            [Model::ID => 12, Model::Clave => 'GRO', Model::Descripcion => 'Guerrero'],
            [Model::ID => 13, Model::Clave => 'HID', Model::Descripcion => 'Hidalgo'],
            [Model::ID => 14, Model::Clave => 'JAL', Model::Descripcion => 'Jalisco'],
            [Model::ID => 15, Model::Clave => 'MEX', Model::Descripcion => 'México'],
            [Model::ID => 16, Model::Clave => 'MIC', Model::Descripcion => 'Michoacán de Ocampo'],
            [Model::ID => 17, Model::Clave => 'MOR', Model::Descripcion => 'Morelos'],
            [Model::ID => 18, Model::Clave => 'NAY', Model::Descripcion => 'Nayarit'],
            [Model::ID => 19, Model::Clave => 'NLE', Model::Descripcion => 'Nuevo León'],
            [Model::ID => 20, Model::Clave => 'OAX', Model::Descripcion => 'Oaxaca'],
            [Model::ID => 21, Model::Clave => 'PUE', Model::Descripcion => 'Puebla'],
            [Model::ID => 22, Model::Clave => 'QUE', Model::Descripcion => 'Querétaro'],
            [Model::ID => 23, Model::Clave => 'ROO', Model::Descripcion => 'Quintana Roo'],
            [Model::ID => 24, Model::Clave => 'SLP', Model::Descripcion => 'San Luis Potosí'],
            [Model::ID => 25, Model::Clave => 'SIN', Model::Descripcion => 'Sinaloa'],
            [Model::ID => 26, Model::Clave => 'SON', Model::Descripcion => 'Sonora'],
            [Model::ID => 27, Model::Clave => 'TAB', Model::Descripcion => 'Tabasco'],
            [Model::ID => 28, Model::Clave => 'TAM', Model::Descripcion => 'Tamaulipas'],
            [Model::ID => 29, Model::Clave => 'TLA', Model::Descripcion => 'Tlaxcala'],
            [Model::ID => 30, Model::Clave => 'VER', Model::Descripcion => 'Veracruz de Ignacio de la Llave'],
            [Model::ID => 31, Model::Clave => 'YUC', Model::Descripcion => 'Yucatán'],
            [Model::ID => 32, Model::Clave => 'ZAC', Model::Descripcion => 'Zacatecas']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
