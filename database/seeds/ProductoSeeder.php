<?php

use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\StockProductosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        $data = [
            [
                ProductosModel::NO_IDENTIFICACION_FACTURA => 'MXO 5W30G4',
                ProductosModel::CLAVE_UNIDAD_FACTURA => 'E48',
                ProductosModel::CLAVE_PROD_SERV_FACTURA => '12181600',
                ProductosModel::DESCRIPCION => 'ACEITE PARA MOTORES',
                ProductosModel::UNIDAD => 'PIEZA',
                ProductosModel::CANTIDAD => '0',
                ProductosModel::VALOR_UNITARIO => '230.37',
                ProductosModel::PRECIO_FACTURA => '18429.60',
                ProductosModel::TALLER_ID => 1,
                ProductosModel::ALMACEN_ID => 1,
                ProductosModel::UBICACION_PRODUCTO_ID => 1
            ],
            [
                ProductosModel::NO_IDENTIFICACION_FACTURA => 'ZX1 5W3G54',
                ProductosModel::CLAVE_UNIDAD_FACTURA => 'E49',
                ProductosModel::CLAVE_PROD_SERV_FACTURA => '12181601',
                ProductosModel::DESCRIPCION => 'ANTICONGELANTE',
                ProductosModel::UNIDAD => 'PIEZA',
                ProductosModel::CANTIDAD => '0',
                ProductosModel::VALOR_UNITARIO => '330.37',
                ProductosModel::PRECIO_FACTURA => '18429.60',
                ProductosModel::TALLER_ID => 1,
                ProductosModel::ALMACEN_ID => 1,
                ProductosModel::UBICACION_PRODUCTO_ID => 1
            ],
            [
                ProductosModel::NO_IDENTIFICACION_FACTURA => 'GN1Z78101A05BA',
                ProductosModel::CLAVE_UNIDAD_FACTURA => 'E44',
                ProductosModel::CLAVE_PROD_SERV_FACTURA => '12181601',
                ProductosModel::DESCRIPCION => 'PANEL - TAP',
                ProductosModel::UNIDAD => 'PIEZA',
                ProductosModel::CANTIDAD => '0',
                ProductosModel::VALOR_UNITARIO => '693.17',
                ProductosModel::PRECIO_FACTURA => '6930.60',
                ProductosModel::TALLER_ID => 1,
                ProductosModel::ALMACEN_ID => 1,
                ProductosModel::UBICACION_PRODUCTO_ID => 1
            ],
            [
                ProductosModel::NO_IDENTIFICACION_FACTURA => 'FL3Z 13466A',
                ProductosModel::CLAVE_UNIDAD_FACTURA => 'E44',
                ProductosModel::CLAVE_PROD_SERV_FACTURA => '39101600',
                ProductosModel::DESCRIPCION => 'BOMBILLAR',
                ProductosModel::UNIDAD => 'PIEZA',
                ProductosModel::CANTIDAD => '0',
                ProductosModel::VALOR_UNITARIO => '270.50',
                ProductosModel::PRECIO_FACTURA => '541.00',
                ProductosModel::TALLER_ID => 1,
                ProductosModel::ALMACEN_ID => 1,
                ProductosModel::UBICACION_PRODUCTO_ID => 1
            ],
            [
                ProductosModel::NO_IDENTIFICACION_FACTURA => 'AL3Z 15200A',
                ProductosModel::CLAVE_UNIDAD_FACTURA => 'E44',
                ProductosModel::CLAVE_PROD_SERV_FACTURA => '25172900',
                ProductosModel::DESCRIPCION => 'FARO ANTINI NEBLINA',
                ProductosModel::UNIDAD => 'PIEZA',
                ProductosModel::CANTIDAD => '0',
                ProductosModel::VALOR_UNITARIO => '582.68',
                ProductosModel::PRECIO_FACTURA => '29134',
                ProductosModel::TALLER_ID => 1,
                ProductosModel::ALMACEN_ID => 1,
                ProductosModel::UBICACION_PRODUCTO_ID => 1
            ]
        ];

        foreach ($data as $key => $item) {
            $producto = ProductosModel::create([
                ProductosModel::NO_IDENTIFICACION_FACTURA => $item[ProductosModel::NO_IDENTIFICACION_FACTURA],
                ProductosModel::CLAVE_UNIDAD_FACTURA => $item[ProductosModel::CLAVE_UNIDAD_FACTURA],
                ProductosModel::CLAVE_PROD_SERV_FACTURA => $item[ProductosModel::CLAVE_PROD_SERV_FACTURA],
                ProductosModel::DESCRIPCION => $item[ProductosModel::DESCRIPCION],
                ProductosModel::UNIDAD => $item[ProductosModel::UNIDAD],
                ProductosModel::CANTIDAD => $item[ProductosModel::CANTIDAD],
                ProductosModel::VALOR_UNITARIO => $item[ProductosModel::VALOR_UNITARIO],
                ProductosModel::PRECIO_FACTURA => $item[ProductosModel::PRECIO_FACTURA],
                ProductosModel::TALLER_ID => $item[ProductosModel::TALLER_ID],
                ProductosModel::ALMACEN_ID => $item[ProductosModel::ALMACEN_ID],
                ProductosModel::UBICACION_PRODUCTO_ID => $item[ProductosModel::UBICACION_PRODUCTO_ID]
            ]);
            
            // DB::table(StockProductosModel::getTableName())->insert([
            //     StockProductosModel::PRODUCTO_ID => $producto->id,
            //     StockProductosModel::CANTIDAD_ACTUAL => $producto[ProductosModel::CANTIDAD],
            //     StockProductosModel::CANTIDAD_ALMACEN_PRIMARIO => $producto[ProductosModel::CANTIDAD],
            // ]);
        }
    }
}
