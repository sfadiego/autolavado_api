<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaTipoJornadaModel as Model;

class CaTipoJornadaSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => 1, Model::Clave=>'01', Model::Descripcion => 'Diurna'],
            [Model::ID => 2, Model::Clave=>'02', Model::Descripcion => 'Nocturna'],
            [Model::ID => 3, Model::Clave=>'03', Model::Descripcion => 'Mixta'],
            [Model::ID => 4, Model::Clave=>'04', Model::Descripcion => 'Por hora'],
            [Model::ID => 5, Model::Clave=>'05', Model::Descripcion => 'Reducida'],
            [Model::ID => 6, Model::Clave=>'06', Model::Descripcion => 'Continuada'],
            [Model::ID => 7, Model::Clave=>'07', Model::Descripcion => 'Partida'],
            [Model::ID => 8, Model::Clave=>'08', Model::Descripcion => 'Por turnos'],
            [Model::ID => 9, Model::Clave=>'99', Model::Descripcion => 'Otra Jornada']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
