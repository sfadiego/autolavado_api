<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaTablasSistemaModel as Model;
use Illuminate\Support\Facades\DB;

class CaTablasSistemaSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => 1,Model::Clave => 1, Model::Descripcion => 'ISR Mes 2018',Model::id_TipoCatalogo=>rand(1,4)],
            [Model::ID => 2,Model::Clave => 2, Model::Descripcion => 'ISR Año 2018',Model::id_TipoCatalogo=>rand(1,4)],
            [Model::ID => 3,Model::Clave => 3, Model::Descripcion => 'Subsidio Empleo',Model::id_TipoCatalogo=>rand(1,4)],
            [Model::ID => 4,Model::Clave => 4, Model::Descripcion => 'Salario Diario',Model::id_TipoCatalogo=>rand(1,4)],
            [Model::ID => 5,Model::Clave => 5, Model::Descripcion => 'Vacaciones',Model::id_TipoCatalogo=>rand(1,4)],
            [Model::ID => 6,Model::Clave => 6, Model::Descripcion => 'Vac. S.R. 1 día',Model::id_TipoCatalogo=>rand(1,4)],
            [Model::ID => 7,Model::Clave => 7, Model::Descripcion => 'Vac. S.R. 2 día',Model::id_TipoCatalogo=>rand(1,4)],
            [Model::ID => 8,Model::Clave => 8, Model::Descripcion => 'Vac. S.R. 3 día',Model::id_TipoCatalogo=>rand(1,4)],
            [Model::ID => 9,Model::Clave => 9, Model::Descripcion => 'Vac. S.R. 4 día',Model::id_TipoCatalogo=>rand(1,4)],
            [Model::ID => 10,Model::Clave => 10, Model::Descripcion => 'Vac. S.R. 5 día',Model::id_TipoCatalogo=>rand(1,4)],
            [Model::ID => 11,Model::Clave => 11, Model::Descripcion => 'Faltas Sem.Red',Model::id_TipoCatalogo=>rand(1,4)]
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
