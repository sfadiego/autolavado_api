<?php

use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoFormaPagoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos_formas_pagos = [
            [
                TipoFormaPagoModel::ID => 1,
                TipoFormaPagoModel::DESCRIPCION => 'Contado',

            ],
            [
                TipoFormaPagoModel::ID => 2,
                TipoFormaPagoModel::DESCRIPCION => 'Credito',
            ],
           
        ];

        foreach ($tipos_formas_pagos as $value) {
            DB::table(TipoFormaPagoModel::getTableName())->insert([
                TipoFormaPagoModel::ID => $value[TipoFormaPagoModel::ID],
                TipoFormaPagoModel::DESCRIPCION => $value[TipoFormaPagoModel::DESCRIPCION],
            ]);
        }
    }
}
