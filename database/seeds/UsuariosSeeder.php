<?php

use App\Models\Usuarios\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarios = [
            [
                User::NOMBRE => 'Diego armando ',
                User::APELLIDO_PATERNO => 'Silva',
                User::APELLIDO_MATERNO => 'Facio',
                User::TELEFONO => '3121166811',
                User::EMAIL => 'diegosilva@gmail.com',
                User::PASSWORD => '12345678',//1-8
                User::ROL_ID => 10,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'diegosilva1234',
                User::RFC => 'SIAASDASDASD',
                User::ACTIVO => true,

            ],
            [
                User::NOMBRE => 'Gregorio ',
                User::APELLIDO_PATERNO => 'Jalomo',
                User::APELLIDO_MATERNO => 'Apellido materno',
                User::TELEFONO => '3121166812',
                User::EMAIL => 'gregorio_jalomo@gmail.com',
                User::PASSWORD => '12345678',//1-8
                User::ROL_ID => 10,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'gregorio1234',
                User::RFC => 'SIAASDASDASD123',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Martin ',
                User::APELLIDO_PATERNO => 'Reyes',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3122439797',
                User::EMAIL => 'lmartinreyes@gmail.com',
                User::PASSWORD => '12345678',//1-8
                User::ROL_ID => 1,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'elreyes',
                User::RFC => 'RESL890315ABX',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Dms ',
                User::APELLIDO_PATERNO => 'Ford',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3121166813',
                User::EMAIL => 'dmssohex@gmail.com',
                User::PASSWORD => 'gL7fePmAf1',//'$2y$10$66gcLUxuoFtr12D44n0Bguqg3EZBiPkaqQmiE2a61GjsNg4/zc1pa',//gL7fePmAf1
                User::ROL_ID => 1,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'admin2020',
                User::RFC => 'gL7fePmAf1',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Horacio ',
                User::APELLIDO_PATERNO => 'dmsford',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3121113335',
                User::EMAIL => 'dmshoracio@gmail.com',
                User::PASSWORD => 'gL7fePmAf1',//gL7fePmAf1
                User::ROL_ID => 10,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'adminhoracio',
                User::RFC => 'gL7fePmAf121',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Asesor prospectos 1',
                User::APELLIDO_PATERNO => '',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3123094368',
                User::EMAIL => 'prospectos1@gmail.com',
                User::PASSWORD => '12345678',//gL7fePmAf1
                User::ROL_ID => 11,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'prospectos1',
                User::RFC => 'prospectos1',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Asesor prospectos 2',
                User::APELLIDO_PATERNO => '',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3123094369',
                User::EMAIL => 'prospectos2@gmail.com',
                User::PASSWORD => '12345678',
                User::ROL_ID => 11,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'prospectos2',
                User::RFC => 'prospectos2',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Asesor prospectos 3',
                User::APELLIDO_PATERNO => '',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3123094366',
                User::EMAIL => 'prospectos3@gmail.com',
                User::PASSWORD => '12345678',
                User::ROL_ID => 11,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'prospectos3',
                User::RFC => 'prospectos3',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Asesor telemarketing 1 ',
                User::APELLIDO_PATERNO => '',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3123094365',
                User::EMAIL => 'telemarketing1@gmail.com',
                User::PASSWORD => '12345678',//gL7fePmAf1
                User::ROL_ID => 12,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'telemarketing1',
                User::RFC => 'telemarketing1',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Asesor telemarketing 2 ',
                User::APELLIDO_PATERNO => '',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3123094361',
                User::EMAIL => 'telemarketing2@gmail.com',
                User::PASSWORD => '12345678',//gL7fePmAf1
                User::ROL_ID => 12,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'telemarketing2',
                User::RFC => 'telemarketing2',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Asesor telemarketing 3 ',
                User::APELLIDO_PATERNO => '',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3123094362',
                User::EMAIL => 'telemarketing3@gmail.com',
                User::PASSWORD => '12345678',//gL7fePmAf1
                User::ROL_ID => 12,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'telemarketing3',
                User::RFC => 'telemarketing3',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Asesor ventas 1 ',
                User::APELLIDO_PATERNO => '',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3123094340',
                User::EMAIL => 'ventas1@gmail.com',
                User::PASSWORD => '12345678',//gL7fePmAf1
                User::ROL_ID => 13,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'ventas1',
                User::RFC => 'ventas1',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Gestor UNO',
                User::APELLIDO_PATERNO => 'Cobranza',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3122439798',
                User::EMAIL => 'gestor@gmail.com',
                User::PASSWORD => '12345678',
                User::ESTATUS_ID => 1,
                User::ROL_ID => 5,
                User::USUARIO => 'gestor1',
                User::RFC => 'XAXX00001',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Gestor DOS',
                User::APELLIDO_PATERNO => 'Cobranza',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3121166870',
                User::EMAIL => 'gestor2@gmail.com',
                User::PASSWORD => '12345678',
                User::ROL_ID => 5,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'gestor2',
                User::RFC => 'XAXX00002',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Asesor ventas 2 ',
                User::APELLIDO_PATERNO => '',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3123094348',
                User::EMAIL => 'ventas2@gmail.com',
                User::PASSWORD => '12345678',//gL7fePmAf1
                User::ROL_ID => 13,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'ventas2',
                User::RFC => 'ventas2',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Asesor ventas 3 ',
                User::APELLIDO_PATERNO => '',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3123094338',
                User::EMAIL => 'ventas3@gmail.com',
                User::PASSWORD => '12345678',//gL7fePmAf1
                User::ROL_ID => 13,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'ventas3',
                User::RFC => 'ventas3',
                User::ACTIVO => true,
            ],
            [
                User::NOMBRE => 'Administrativo',
                User::APELLIDO_PATERNO => '',
                User::APELLIDO_MATERNO => '',
                User::TELEFONO => '3122439710',
                User::EMAIL => 'administrativo@gmail.com',
                User::PASSWORD => '12345678',//gL7fePmAf1
                User::ROL_ID => 4,
                User::ESTATUS_ID => 1,
                User::USUARIO => 'administrativo1',
                User::RFC => 'admini',
                User::ACTIVO => true,
            ],
        ];

        foreach ($usuarios as $key => $value) {
            DB::table(User::getTableName())->insert([
                User::NOMBRE => $value[User::NOMBRE],
                User::APELLIDO_MATERNO => $value[User::APELLIDO_MATERNO],
                User::APELLIDO_PATERNO => $value[User::APELLIDO_PATERNO],
                User::TELEFONO => $value[User::TELEFONO],
                User::EMAIL => $value[User::EMAIL],
                User::PASSWORD => $value[User::PASSWORD],
                User::ROL_ID => $value[User::ROL_ID],
                User::ESTATUS_ID => $value[User::ESTATUS_ID],
                User::USUARIO => $value[User::USUARIO],
                User::RFC => $value[User::RFC],
                User::ACTIVO => $value[User::ACTIVO],
            ]);
        }
    }
}
