<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaTiposContratoModel as Model;
use Illuminate\Support\Facades\DB;

class CaTipoContratoSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID=>1, Model::Clave => '01', Model::Descripcion => 'Contrato de trabajo por tiempo indeterminado'],
            [Model::ID=>2, Model::Clave => '02', Model::Descripcion => 'Contrato de trabajo por obra determinada'],
            [Model::ID=>3, Model::Clave => '03', Model::Descripcion => 'Contrato de trabajo por tiempo determinado'],
            [Model::ID=>4, Model::Clave => '04', Model::Descripcion => 'Contrato de trabajo por temporada'],
            [Model::ID=>5, Model::Clave => '05', Model::Descripcion => 'Contrato de trabajo sujeto a prueba'],
            [Model::ID=>6, Model::Clave => '06', Model::Descripcion => 'Contrato de trabajo con capacitación inicial'],
            [Model::ID=>7, Model::Clave => '07', Model::Descripcion => 'Modalidad de contratación por pago de hora laborada'],
            [Model::ID=>8, Model::Clave => '08', Model::Descripcion => 'Modalidad de trabajo por comisión laboral'],
            [Model::ID=>9, Model::Clave => '09', Model::Descripcion => 'Modalidad de contratación donde no existe relación de trabajo'],
            [Model::ID=>10, Model::Clave => '10', Model::Descripcion => 'Jubilación, pensión, retiro'],
            [Model::ID=>11, Model::Clave => '99', Model::Descripcion => 'Otro contrato']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
